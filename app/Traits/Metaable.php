<?php

namespace App\Traits;

use App\Metadata;

trait Metaable
{
    public function syncManyFromString(String $meta="", String $type = null,String $delimiter = ",")
    {
        $this->metadata()->delete();
        $metas = [];
        foreach (explode($delimiter, $meta) as $m) $metas[] = new Metadata(['value' => trim($m), 'type' => $type]);
        $this->metadata()->saveMany($metas);
    }
}
