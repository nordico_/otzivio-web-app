<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Sites;

class Subscriptions extends Model
{
    // no timestamps please
    public $timestamps = false;

    // public const TRIAL_PERIOD = 30;

    const SUBSCRIPTION_ACTIVE = "Active";
    const SUBSCRIPTION_CANCELLED = "Canceled";


    // belongs to user
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // has one site
    public function site()
    {
        return $this->hasOne(Sites::class, 'id', 'site_id');
    }

    public function isActive()
    {
        return $this->subscription_status === self::SUBSCRIPTION_ACTIVE;
    }

    public function isCancelled()
    {
        return $this->subscription_status === self::SUBSCRIPTION_CANCELLED;
    }

    public function isOnGracePeriod()
    {
        if (!$this->isCancelled()) return false;
        if ($this->subscription_end_date == null) return false;
        return now()->lte(date('Y-m-d', $this->subscription_end_date));
    }

    public static function trialEndsOn($time)
    {
        return strtotime(\Carbon\Carbon::parse($time)->addDays(env('TRIAL_PERIOD')));
    }

    public function isOnTrialPeriod()
    {
        return $this->trial_end_date != null && now()->lte(date('Y-m-d', $this->trial_end_date));
    }
}
