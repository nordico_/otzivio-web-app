<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{

    public const SETTING_KEY_SEND_INVITATION_AFTER = 'send_invitation_after';
    public const SETTING_KEY_SEND_REMINDER_AFTER = 'send_reminder_after';
    public const SETTING_KEY_INVITATION_EMAIL_TEMPLATE = 'invitation_email_template';
    public const SETTING_KEY_REMINDER_EMAIL_TEMPLATE = 'reminder_email_template';
    public const SETTING_KEY_REMINDER_EMAIL_SUBJECT = 'reminder_email_subject';
    public const SETTING_KEY_INVITATION_EMAIL_SUBJECT = 'invitation_email_subject';

    public const SETTING_SUBJECT_SITE_NAME_PLACEHOLDER = "[SiteName]";
    public const DEFAULT_SUBJECT = "⭐⭐⭐⭐⭐ Колко звезди ще дадете на [SiteName]";

    protected $fillable = ['site_id','send_reminder_after','send_invitation_after','reminder_email_subject','invitation_email_subject','reminder_email_template_id','invitation_email_template_id'];
    
    protected $appends = ['reminder_email_template','invitation_email_template'];

    public function getReminderEmailTemplateAttribute(){
        $existingTemplate = $this->reminderEmailTemplate()->first();
        return $existingTemplate ? $existingTemplate->value : EmailTemplate::DEFAULT_EMAIL_TEMPLATE;
    }
    
    public function getInvitationEmailTemplateAttribute(){
        $existingTemplate = $this->invitationEmailTemplate()->first();
        return $existingTemplate ? $existingTemplate->value : EmailTemplate::DEFAULT_EMAIL_TEMPLATE;
    }


    public function reminderEmailTemplate(){
        return $this->belongsTo(EmailTemplate::class,'reminder_email_template_id');
    }

    public function invitationEmailTemplate(){
        return $this->belongsTo(EmailTemplate::class,'invitation_email_template_id');
    }
}
