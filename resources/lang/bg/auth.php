<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Въведените данни за идентификация не съответстват на нашите записи.',
    'social' => 'Опа... изглежда имейлът, който използвате, е вече свързан с :platform. Моля, влезте с :platform акаунта си.',
    'throttle' => 'Твърде много опити за влизане. Моля, опитайте отново след :seconds секунди.',

];
