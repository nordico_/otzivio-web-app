<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use \Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }

    protected function login(Request $request)
    {
        $this->validateLogin($request);
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
        $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            
            return $this->sendLockoutResponse($request);
        }
        
        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }
        
        // check if user has logged in using social accounts
        
        $social = User::where('email',$request->email)->whereNotNull('provider_user_id')->whereNotNull('provider')->first();
        
        if($social){
            return $this->sendTrySocialLoginResponse($social->provider);
        }
        
        
        $this->incrementLoginAttempts($request);
        
        return $this->sendFailedLoginResponse($request);
    }

    



    protected function authenticated(Request $request, $user)
    {
        //  $user->role . '/home'
        $route = $request['return'] != null ? $request['return'] : route('myaccount');
        return redirect()->to($route);
    }


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        $this->performLogout($request);
        $url = LaravelLocalization::localizeUrl(route('home'));
        $url = !str_contains($url,'en') ? $url."bg" : $url;
        return redirect($url);
    }


    protected function sendTrySocialLoginResponse($platform){
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.social',['platform'=>$platform])],
        ]);
    }

}
