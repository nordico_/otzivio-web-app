<?php
return [ 
	'trustScore' => 'TrustScore',
	'notRated' => 'Not Rated',
	'prettyBad' => 'Bad', // scores less than or equal to 1.49
	'bad' => 'Poor', // scores between >= 1.50 and <= 2.49
	'good' => 'Average',// scores between >= 2.50 and lower than or equal to <= 3.49
	'veryGood' => 'Great',// scores between >= 3.50 and lower than or equal to <= 4.49
	'excellent' => 'Excellent' // higher or equal to >= 4.49
];