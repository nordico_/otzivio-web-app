<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('sitemap:generate')->weekly();
        $schedule->command('excel:clear')->weekly();
        // $schedule->command('emails:fetch')->dailyAt('23:56')->timezone('Europe/Sofia');
        // $schedule->command('emails:send')->dailyAt('8:00')->timezone('Europe/Sofia');
        // $schedule->command('reminder:send')->dailyAt('9:00')->timezone('Europe/Sofia');
        $schedule->command('emails:fetch')->everyTwoMinutes();
        $schedule->command('emails:send')->everyThreeMinutes();
        $schedule->command('reminder:send')->everyTwoMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
