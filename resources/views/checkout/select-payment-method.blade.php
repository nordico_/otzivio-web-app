@extends( 'base' )

@section( 'content' )


@if ($plan == 'monthly')
	@php
		$customPlan = 'месец'
	@endphp
@elseif ($plan == '6months')
	@php
		$customPlan = '6 месеца'
	@endphp
@else
	@php
		$customPlan = 'година'
	@endphp
@endif

<div class="inner-page">
	<div class="inner-page__title text-center">
		<h1>{{ __( 'Select Payment Method') }}</h1>
		<div class="inner-page__subtitle">
			<p>Ще бъдете таксувани с {{ $price . Options::get_option( 'currency_symbol' ) . ' на ' . $customPlan }}, докато не анулирате абонамента си. Ако анулирате абонамента си до 14 дни от срока на плащане, ще ви върнем парите напълно.</p>			
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2 text-center">
				
				<h5>{{ __( sprintf( 'Сума за плащане: %sлв на %s', $price, ucfirst($customPlan) ) )}}</h5>
				<br>

				@if( 'Yes' == Options::get_option( 'stripeEnable' ) )
				<a href="/checkout/credit-card" class="btn btn-success btn-lg">{{ __('Credit Card') }}</a>
				@endif
				@if( 'Yes' == Options::get_option( 'paypalEnable' ) )
				<a href="/checkout/paypal" class="btn btn-warning paypalSubmit btn-lg">
					{{ __('PayPal') }}</a>
				@endif

				<div class="payment-methods__cards">
					<img src="/images/visa-card.svg"/>
					<img src="/images/mastercard-card.svg"/>
					<img src="/images/maestro-card.svg"/>
				</div>
									
			</div><!-- /.col-md-6 -->
		</div><!-- /.row -->
	</div>
</div>

@endsection