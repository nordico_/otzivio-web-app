@extends('layouts.business') 

@section( 'content' )

<div class="subscription-plans__page inner-page">
    <div class="inner-page__title text-center">
        <h1>{{ __( 'Pricing' ) }}</h1>
        <div class="inner-page__subtitle">
            {{ __( 'pricing_page_description' ) }}
            @if( request()->has( 'company' ) )
                            
            @else
                {!! __( sprintf('pricing_page_howtoclaim')) !!}
            @endif
            
        </div>
    </div>

    <div class="container subscription-plans">
        <div class="row">
            <div class="col-xs-12 col-md-6 subscription-plans__plan">
                <div class="panel panel-info card">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ __('pro_plan_title') }}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="the-price">{{ Options::get_option( 'monthlyPrice' ) }}<span class="subscript">лв / {{ __('month') }}</span></div>
                        <br>
                        <ul class="subscription-plans__benefits">
                            <li><i class="fas fa-check"></i><span class=>{{ __('pricing_page_plan_benefit_1') }}</span></li>
                            <li><i class="fas fa-check"></i><span class=>{{ __('pricing_page_plan_benefit_2') }}</span></li>
                            <li><i class="fas fa-check"></i><span class=>{{ __('pricing_page_plan_benefit_3') }}</span></li>
                            <li><i class="fas fa-check"></i><span class=>{{ __('pricing_page_plan_benefit_4') }}</span></li>
                            <li><i class="fas fa-check"></i><span class=>{{ __('pricing_page_plan_benefit_5') }}</span></li>
                            <li><i class="fas fa-check"></i><span class=>{{ __('pricing_page_plan_benefit_6') }}</span></li>
                            <li><i class="fas fa-check"></i><span class=>{{ __('pricing_page_plan_benefit_7') }}</span></li>
                        </ul>
                    </div>
                    <div class="panel-footer">
                        @if( request()->has( 'company' ) )
                            <a href="{{ route('companyClaim', [ 'site' => request()->company, 'plan' => 'monthly' ]) }}" class="btn btn-success">{{ __( 'pricing_page_pro_plan_button_text',['days'=>env('TRIAL_PERIOD')]) }}</a>
                        @endif
                        <p class="mt-3">{{ __('pricing_page_free_trial_desc',['days'=>env('TRIAL_PERIOD')]) }}</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 subscription-plans__plan">
                <div class="panel panel-primary card">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ __('free_plan_title') }}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="the-price">{{ __('free_plan_desc') }}<!--{{ Options::get_option( 'monthlyPrice' ) }}<span class="subscript">лв / {{ __('month') }}</span>--></div>
                        <br>
                        <ul class="subscription-plans__benefits">
                            <li><i class="fas fa-check"></i><span class=>{{ __('pricing_page_plan_benefit_1') }}</span></li>
                            <li><i class="fas fa-check"></i><span class=>{{ __('pricing_page_plan_benefit_2') }}</span></li>
                        </ul>
                    </div>
                    <div class="panel-footer">
                        @if( request()->has( 'company' ) )
                            <a href="{{ route('companyClaim', [ 'site' => request()->company, 'plan' => 'free' ]) }}" class="btn btn-dark">{{ __( 'pricing_page_free_plan_button_text' ) }}</a>
                        @endif
                        <p class="mt-3">{{ __('pricing_page_free_plan_desc') }}</p>
                    </div>
                </div>
            </div>
        </div>
        @if( request()->has( 'company' ) )
                            
        @else
            <div class="subscription-plans__choose-company">        
                {{ __('Browse our site and find your company to claim and get any plan') }}
            </div>
        @endif
        <br><br>
        <h2 class="text-center">{{ __('pricing_page_faq_title') }}</h2>
        <br>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h4>{{ __('pricing_page_faq_1_title') }}</h4>
                <p>{{ __('pricing_page_faq_1_body') }}</p>
            </div>
            <div class="col-xs-12 col-md-6">
                <h4>{{ __('pricing_page_faq_2_title') }}</h4>
                <p>{{ __('pricing_page_faq_2_body') }}</p>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h4>{{ __('pricing_page_faq_3_title') }}</h4>
                <p>{{ __('pricing_page_faq_3_body') }}</p>
            </div>
            <div class="col-xs-12 col-md-6">
                <h4>{{ __('pricing_page_faq_4_title') }}</h4>
                <p>{{ __('pricing_page_faq_4_body') }}</p>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h4>{{ __('pricing_page_faq_5_title') }}</h4>
                <p>{{ __('pricing_page_faq_5_body') }}</p>
            </div>
            <div class="col-xs-12 col-md-6">
                <h4>{{ __('pricing_page_faq_6_title') }}</h4>
                <p>{{ __('pricing_page_faq_6_body') }}</p>
            </div>
        </div>
    </div>
</div>

@endsection