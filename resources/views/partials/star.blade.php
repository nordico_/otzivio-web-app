@php ($fullStar = '<svg width="129" height="123" viewBox="0 0 129 123" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M64.5682 98.8726L104.303 123L93.7904 77.4965L128.864 46.8954L82.6288 42.9011L64.5682 0L46.5076 42.9011L0.272705 46.8954L35.3459 77.4965L24.8336 123L64.5682 98.8726Z" fill="#18806F"/></svg>')
@php ($halfStar = '<svg width="64" height="123" viewBox="0 0 64 123" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M64 99.0519V0.701721L46.2349 42.9011L0 46.8954L35.0732 77.4965L24.561 123L64 99.0519Z" fill="#18806F"/></svg>')
@php ($emptyStar = '<svg width="129" height="123" viewBox="0 0 129 123" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M64.5682 98.8726L104.303 123L93.7904 77.4965L128.864 46.8954L82.6288 42.9011L64.5682 0L46.5076 42.9011L0.272705 46.8954L35.3459 77.4965L24.8336 123L64.5682 98.8726Z" fill="#E8E8E8"/></svg>')

<div class="rating-stars__wrapper">
    <div class="rating-stars__no-rating">
        {!! str_repeat('<div class="rating-star rating-star__no-rating">'.$emptyStar.'</div>', 5) !!}
    </div>
    <div class="rating-stars__has-rating">
        @if(number_format($r,2) <= 1.29) 
            {!! str_repeat('<div class="rating-star rating-star__poor">'.$fullStar.'</div>', $r) !!}
        @elseif(number_format($r,2) >= 1.30 && number_format($r,2) <= 1.79) 
            {!! '<div class="rating-star rating-star__poor">'.$fullStar.'</div><div class="rating-star rating-star__poor">'.$halfStar.'</div>' !!}
        @elseif(number_format($r,2) >= 1.80 && number_format($r,2) <= 2.29) 
            {!! str_repeat('<div class="rating-star rating-star__average">'.$fullStar.'</div>', $r) !!}
        @elseif(number_format($r,2) >= 2.30 && number_format($r,2) <= 2.79) 
            {!! str_repeat('<div class="rating-star rating-star__average">'.$fullStar.'</div>', $r) !!}  <div class="rating-star rating-star__average">{!! $halfStar !!}</div>
        @elseif(number_format($r,2) >= 2.80 && number_format($r,2)
        <= 3.29) {!! str_repeat('<div class="rating-star rating-star__good">'.$fullStar.'</div>', $r) !!}
        @elseif(number_format($r,2) >= 3.30 && number_format($r,2)
        <= 3.79) {!! str_repeat('<div class="rating-star rating-star__good">'.$fullStar.'</div>', $r) !!}  <div class="rating-star rating-star__good">{!! $halfStar !!}</div>
        @elseif(number_format($r,2) >= 3.80 && number_format($r,2)
        <= 4.29) {!! str_repeat('<div class="rating-star rating-star__very-good">'.$fullStar.'</div>', $r) !!}
        @elseif(number_format($r,2) >= 4.30 && number_format($r,2)
        <= 4.79) {!! str_repeat('<div class="rating-star rating-star__excellent">'.$fullStar.'</div>', $r) !!}<div class="rating-star rating-star__excellent">{!! $halfStar !!}</div>
        @elseif(number_format($r,2) >= 4.80) {!! str_repeat('<div class="rating-star rating-star__excellent">'.$fullStar.'</div>', $r) !!}
        @endif
    </div>
</div>
