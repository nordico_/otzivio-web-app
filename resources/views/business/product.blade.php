@extends('layouts.business') 
@section('content')
<div class="for-businesses-page">
	<div class="gray-background pt-3 pb-5">
		<div class="container">
			<div class="inner-page__title text-center">
				<h1>{{ __('Listen and learn. Win trust. Increase your sales.') }}</h1>
			</div>
			<div class="text-center">
				<img src="/images/business/for-business-image.svg" class="mt-5 mt-md-0" style="max-width: 460px; width: 100%;" />
			</div>
		</div>
	</div>
	<div class="container pb-md-5 mt-md-5 mb-5">
		<div class="row d-flex">
			<div class="col-12 text-center text-lg-left col-lg-6 pr-md-5 align-self-center justify-content-sm-center order-2 order-lg-1">
				<h3 class="mt-4">{{ __('Collect reviews effortlessly') }}</h3>
				<p>{{ __("You'll get a constant influx of new and verified reviews from your customers; with Otzivio, those reviews are displayed even if you're no longer a customer. And you don't have to lift a finger, all it takes is one email, sent by us. Valuable insight, positive feedback and increased sales - we make it simple.") }}</p>
				<a href="{{ route('businessCollect') }}" class="arrow__link"><span>{{ __('Learn how to collect reviews') }}</span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M17.707 9.293l-5-5a1 1 0 00-1.414 1.414L14.586 9H3a1 1 0 100 2h11.586l-3.293 3.293a1 1 0 001.414 1.414l5-5a1 1 0 000-1.414z"></path></svg></a>
			</div>
			<div class="col-12 text-center text-lg-left col-lg-6 mt-5 align-self-center order-1 order-lg-2">
				<img src="/images/business/collect-reviews.svg" style="max-width: 500px; width: 100%;"  />
			</div>
		</div>
	</div>
	<div class="gray-background pt-5 pb-5 mt-5">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center text-lg-left col-lg-6 align-self-center">
					<img src="/images/business/engage-with-customers.svg" style="max-width: 500px; width: 100%;" />
				</div>
				<div class="col-12 text-center text-lg-left col-lg-6 pl-md-5 align-self-center">
					<h3 class="mt-4">{{ __('Engage in real conversations with your customers') }}</h3>
					<p>{{ __("Talking to your customers is the best way to build and promote your brand. Engage and delight them with timely and informative responses. It's easy to do with our centralized portal.") }}</p>
					<a href="{{ route('businessEngage') }}" class="arrow__link"><span>{{ __('Learn how to engage with your customers') }}</span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M17.707 9.293l-5-5a1 1 0 00-1.414 1.414L14.586 9H3a1 1 0 100 2h11.586l-3.293 3.293a1 1 0 001.414 1.414l5-5a1 1 0 000-1.414z"></path></svg></a>
				</div>
			</div>
		</div>
	</div>
	<div class="container pb-5 mb-5 mt-5">
		<div class="row d-flex">
			<div class="col-12 text-center text-lg-left col-lg-6 pr-md-5 align-self-center order-2 order-lg-1">
				<h3 class="mt-4">{{ __('Share: Reviews increase trust') }}</h3>
				<p>{{ __("Reviews prove to customers whether or not you put their needs first. If customer service is in your DNA, they'll make the case. With Otzivio, it's incredibly easy to share your good service on websites, Facebook, and on search engines. Learn more about why reviews matter.") }}</p>
				<a href="{{ route('businessShare') }}" class="arrow__link"><span>{{ __('Share your trustworthiness') }}</span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M17.707 9.293l-5-5a1 1 0 00-1.414 1.414L14.586 9H3a1 1 0 100 2h11.586l-3.293 3.293a1 1 0 001.414 1.414l5-5a1 1 0 000-1.414z"></path></svg></a>
			</div>
			<div class="col-12 text-center text-lg-left col-lg-6 align-self-center order-1 order-lg-2">
				<img src="/images/business/share.svg"  style="max-width: 500px; width: 100%;"/>
			</div>
		</div>
	</div>
	<div class="gray-background pt-5 pb-5 mt-5">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center text-lg-left col-lg-6 align-self-center">
					<img src="/images/business/help.svg" style="max-width: 500px; width: 100%;" />
				</div>
				<div class="col-12 text-center text-lg-left col-lg-6 pl-md-5 align-self-center">
					<h3 class="mt-4">{{ __('Need support?') }}</h3>
					<p>{{ __("We're happy to help. We work hard to make sure you get all the support you need from the get-go.") }}</p>
					<a href="{{ route('businessEngage') }}" class="arrow__link"><span>{{ __('See how to get in touch with us') }}</span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M17.707 9.293l-5-5a1 1 0 00-1.414 1.414L14.586 9H3a1 1 0 100 2h11.586l-3.293 3.293a1 1 0 001.414 1.414l5-5a1 1 0 000-1.414z"></path></svg></a>
				</div>
			</div>
		</div>
	</div>
	@component('components/subscribe-to-otzivio-panel') @endcomponent
</div>
@endsection