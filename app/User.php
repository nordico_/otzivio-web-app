<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    const DEFAULT_PASSWORD = "password";
    const FACEBOOK_AUTH_PROVIDER = "facebook";
    const GOOGLE_AUTH_PROVIDER = "google";


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'provider_user_id', 'profilePic', 'provider', 'email_verified_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // user reviews
    public function reviews()
    {
        return $this->hasMany(Reviews::class);
    }

    // user company
    public function company()
    {
        return $this->hasOne(Sites::class, 'claimedBy', 'id');
    }
    public function invites()
    {
        return $this->hasMany(Invite::class);
    }

    public function pendingInvite()
    {
        return $this->hasOne(Invite::class)->where('status', Invite::PENDING);
    }

    // subscriptions
    public function subscriptions()
    {
        return $this->hasMany(Subscriptions::class);
    }

    // get profile pic attribute
    public function getProfileThumbAttribute()
    {

        if (is_null($this->profilePic))
            return asset('public/storage/no-img.png');
        else
            return asset('public/storage/' . $this->profilePic);
    }

    // resend verificatio email
    public function resend()
    {

        $user = auth()->user();

        $verificationUrl = \URL::temporarySignedRoute(
            'verification.verify',
            \Carbon\Carbon::now()->addMinutes(config('auth.verification.expire', 60)),
            [
                'id' => $user->getKey(),
                'hash' => sha1($user->getEmailForVerification()),
            ]
        );

        $user->sendEmailVerificationNotification();

        return back()->with('resent', true);
    }
    public function isOnPaidSubscription()
    {
        $company = auth()->user()->company;
        if(!$company) return false;
        if(!$company->isOnPaidSubscription() && !$company->isOnGracePeriod()) return false;
        return true;
    }
    public function isOnGracePeriod()
    {
        $company = auth()->user()->company;
        if(!$company) return false;
        return $company->isOnGracePeriod();
    }
}
