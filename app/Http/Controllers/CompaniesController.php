<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use App\Sites;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use App\Mail\EmailNotification;
use App\Services\CompanyReviewService;
use App\SiteEmail;
use App\Verify;

use Mail;
use Hash;
use Options;

class CompaniesController extends Controller
{
	private CompanyReviewService $reviewService;

	public function __construct(CompanyReviewService $service)
	{
		$this->reviewService = $service;
	}
	// plans
	public function plans()
	{

		$seo_title = __('business_pricing_plans');
		SEOMeta::setTitle(trans('meta_title_for_business_pricing_page'));
		SEOMeta::setDescription(trans('meta_desc_for_business_pricing_page'));
		OpenGraph::setTitle(trans('meta_title_for_business_pricing_page'));
		OpenGraph::setDescription(trans('meta_desc_for_business_pricing_page'));

		return view('business/pricing-plans', compact('seo_title'));
	}

	// claim
	public function claim(Sites $company, String $plan)
	{

		if (!is_null($company->claimedBy)) {
			alert()->error(__('This company is already claimed'), __('OOPS'));
			return redirect()->route('reviewsForSite',['site'=>$company->url]);
		}
		if (!is_null(auth()->user()->company)) {
			alert()->error(__('You cannot claim multiple companies'), __('OOPS'));
			return redirect()->route('reviewsForSite',['site'=>$company->url]);
		}

		// verify ownership
		return view('verify-ownership', compact('company', 'plan'));
	}

	// verify ownership
	public function verifyOwnershipForm(Sites $c, Request $r)
	{

		// validate    	
		$this->validate($r, ['username' => 'required', 'plan' => 'in:free,6months,yearly,monthly']);


		// create hash
		$code = Hash::make(md5(rand(1, 99999999)));

		// create entry hash
		$verify = new Verify;
		$verify->plan = $r->plan;
		$verify->site_id = $c->id;
		$verify->hash = $code;
		$verify->save();



		// compute email destination
		$sendToEmail = $r->username . '@' . str_ireplace('www.', '', $c->url);

		session(['ownershipEmail' => $sendToEmail]);

		// send verification email
		$data['message'] = sprintf(__('Здравейте,%s 
                                      Някой поиска проверка на собствеността на Вашата компания %s в Отзивио.
                                      Ако това сте Вие, моля, кликнете на линка по-долу, за да потвърдите собствеността'), '<br>', '<strong>' . $c->business_name . '</strong>');

		$data['intromessage'] = __('Проверка на собствеността на компанията');
		$data['url'] = env('APP_URL') . '/ownership-verify?code=' . $code;

		$data['buttonText'] = __('Потвърдете собствеността');

		Mail::to($sendToEmail)->send(new EmailNotification($data));

		return view('verify-ownership-message', compact('sendToEmail'));
	}

	// ownership verification
	public function ownershipVerify(Request $r)
	{

		// check site by code
		if (!$r->has('code'))
			return redirect('/');

		// check hash exists
		$verify = Verify::where('hash', $r->code)->first();

		if (!$verify) {
			alert()->warning(__('Невалиден код за потвърждение.'), __('Опа!'));
			return redirect('/');
		}

		// all good now
		$verify->verified = 'yes';
		$verify->save();

		// redirect to payment plan page
		session(['ownershipVerified' => $r->code]);

		// set plan 
		session(['ownershipPlan' => $verify->plan]);

		// set site
		session(['ownershipSite' => $verify->site]);

		// alert
		alert()->success(
			__(sprintf('Успешно потвърдихте собствеността на %s', $verify->site->url)),
			__('Готово!')
		);

		if ($verify->plan === 'free') {
			$verify->site->update([
				'claimedBy' => auth()->user()->id,
			]);
			return redirect(route('checkout.success'));
		}
		return redirect(route('select-payment-method'));
	}

	public function showEvaluatePage(Request $request, Sites $site)
	{
		$initialRating = $request['rating'] ?? 0;
		if (auth()->user() && $site->reviews()->where('user_id', auth()->user()->id)->first())
			return redirect()->route('review-acknowledge', $site);

		return view('evaluate', compact('site', 'initialRating'));
	}
	public function evaluate(Sites $site, Request $request)
	{
		try {
			$this->reviewService->create($site, $request['rating'], $request['review_title'], $request['review_content'], auth()->user()->id);
			return redirect()->route('review-acknowledge', $site);
		} catch (\Exception $e) {
			return back()->with('message', $e->getMessage());
		}
	}
	public function acknowledge(Sites $site)
	{
		$review = $site->reviews()->where('user_id', auth()->user()->id)->first();

		return view('review-acknowledge', compact('site', 'review'));
	}
	// new site email verification
	public function verifySiteEmail(Request $request, $id, $hash)
	{
		$siteEmail = SiteEmail::find($id);

		if (!sha1($siteEmail) == $hash) {
			abort(401);
		}

		$siteEmail->markEmailAsVerified();

		return view('email-verified-message');
	}
}
