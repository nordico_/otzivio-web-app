@extends( 'base' )

@section( 'content' )
<div>
    @if( !empty( Options::get_option( 'homeAd' ) ) )
    <div class="row">
        <div class="col-xs-12">
            {!! Options::get_option( 'homeAd' ) !!}
            <br><br>
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->
    @endif    

    <div class="latest-reviews">
        <h2 class="text-center">{{__('Recently added reviews')}}</h2>
        <div class="latest-reviews__slider">
            @forelse($reviews as $r)

            <div class="latest-reviews__slide">
                <div class="card">

                    <div class="image-with-rating">
                        @if ($r->user->profilePic!=null)
                            <img src="{{ $r->user->profileThumb }}" alt="profile pic" style="height: 35px"
                            class="img-fluid rounded-circle image-with-rating__image">
                        @else
                            <img avatar="{{$r->user->name}}" alt="profile pic"
                            class="img-fluid rounded-circle image-with-rating__image">
                        @endif
                        <div class="image-with-rating__rating">
                            @include('partials/star', ['r' => $r->rating])
                        </div>
                    </div>
                    <div class="reviewee">
                        <strong>{{ $r->reviewer }}</strong> {{ __('reviewed') }} <a href="{{ $r->site->slug }}">{{ $r->site->url }}</a>
                    </div><!-- /.col-xs-6 col-md-11 -->
                    <!--<p class="text-bold">"{{ $r->review_title }}"</p>-->
                    <p>{{  str_limit($r->review_content, $limit = 96, $end = '...')}}</p>
                    <p class="justify-content-between">
                        <!--<span class="text-muted float-left">
                                    {{ $r->timeAgo  }}
                                </span>-->
                        <a href="{{ $r->site->slug }}" class="arrow__link float-left">{{ __('Read Review') }}<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M17.707 9.293l-5-5a1 1 0 00-1.414 1.414L14.586 9H3a1 1 0 100 2h11.586l-3.293 3.293a1 1 0 001.414 1.414l5-5a1 1 0 000-1.414z"></path></svg></a>
                    </p>
                    <!-- /.btn btn-xs btn-success -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-md-3 -->

            <!-- /.container -->
            @empty
            <h1 class="text-center">
                <span class="badge badge-warning">
                    {{ __('No reviews yet :(') }}
                </span>
                <!-- /.badge badge-warning -->
            </h1>
            @endforelse
        </div>
    </div>

    <div class="how-it-works text-center">
        <h2 class="">{{ __('How does it work?') }}</h2>
        <div class="row">
            <div class="col-12 col-lg-4 col-sm-12">
                <img src="/images/find_company.svg" />
                <h3><span class="how-it-works__step">1</span> <span class="how-it-works__title">{{__('Browse Companies')}}</span></h3>
                <p>{{__("Use the 'Browse companies' box above to search for a company or click 'Browse companies' from the menu")}}</p>
            </div>
            <div class="col-12 col-lg-4 col-sm-12">
                <img src="/images/add_company.svg" />
                <h3><span class="how-it-works__step">2</span> <span class="how-it-works__title">{{__('Submit Company')}}</span></h3>
                <p>{{__("If you cannot find the company you are looking for, click on 'Add company' from the menu. It takes a few hours to verify and approve the company you want to add")}}</p>
            </div>
            <div class="col-12 col-lg-4 col-sm-12">
                <img src="/images/add_review.svg" />
                <h3><span class="how-it-works__step">3</span><span class="how-it-works__title">{{__('Write a review')}}</span></h3>
                <p>{{__('Once you have found or added the company, write your review. Please follow the guidelines for writing reviews. It takes a few hours to approve the review')}}</p>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>

<div class="colored-box text-center">
    <h2>{{__('What is Otzivio?')}}</h2>
    <p class="subline">{{__('Otzivio home page description')}}</p>
    <div class="row colored-box__columns">
        <div class="row">
            <div class="col-md-12 col-lg-4">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#008060" class="bi bi-megaphone"
                    viewBox="0 0 16 16">
                    <path
                        d="M13 2.5a1.5 1.5 0 0 1 3 0v11a1.5 1.5 0 0 1-3 0v-.214c-2.162-1.241-4.49-1.843-6.912-2.083l.405 2.712A1 1 0 0 1 5.51 15.1h-.548a1 1 0 0 1-.916-.599l-1.85-3.49a68.14 68.14 0 0 0-.202-.003A2.014 2.014 0 0 1 0 9V7a2.02 2.02 0 0 1 1.992-2.013 74.663 74.663 0 0 0 2.483-.075c3.043-.154 6.148-.849 8.525-2.199V2.5zm1 0v11a.5.5 0 0 0 1 0v-11a.5.5 0 0 0-1 0zm-1 1.35c-2.344 1.205-5.209 1.842-8 2.033v4.233c.18.01.359.022.537.036 2.568.189 5.093.744 7.463 1.993V3.85zm-9 6.215v-4.13a95.09 95.09 0 0 1-1.992.052A1.02 1.02 0 0 0 1 7v2c0 .55.448 1.002 1.006 1.009A60.49 60.49 0 0 1 4 10.065zm-.657.975l1.609 3.037.01.024h.548l-.002-.014-.443-2.966a68.019 68.019 0 0 0-1.722-.082z" />
                </svg>
                <h4>{{__('Transparency is good for everyone')}}</h4>
                <p>{{__('What better way to find out how a company is doing than to hear it directly from other users? All reviews, both good and bad, open up opportunities to build trust, popularity and reputation')}}</p>
            </div>
            <div class="col-md-12 col-lg-4">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#008060" class="bi bi-trophy"
                    viewBox="0 0 16 16">
                    <path
                        d="M2.5.5A.5.5 0 0 1 3 0h10a.5.5 0 0 1 .5.5c0 .538-.012 1.05-.034 1.536a3 3 0 1 1-1.133 5.89c-.79 1.865-1.878 2.777-2.833 3.011v2.173l1.425.356c.194.048.377.135.537.255L13.3 15.1a.5.5 0 0 1-.3.9H3a.5.5 0 0 1-.3-.9l1.838-1.379c.16-.12.343-.207.537-.255L6.5 13.11v-2.173c-.955-.234-2.043-1.146-2.833-3.012a3 3 0 1 1-1.132-5.89A33.076 33.076 0 0 1 2.5.5zm.099 2.54a2 2 0 0 0 .72 3.935c-.333-1.05-.588-2.346-.72-3.935zm10.083 3.935a2 2 0 0 0 .72-3.935c-.133 1.59-.388 2.885-.72 3.935zM3.504 1c.007.517.026 1.006.056 1.469.13 2.028.457 3.546.87 4.667C5.294 9.48 6.484 10 7 10a.5.5 0 0 1 .5.5v2.61a1 1 0 0 1-.757.97l-1.426.356a.5.5 0 0 0-.179.085L4.5 15h7l-.638-.479a.501.501 0 0 0-.18-.085l-1.425-.356a1 1 0 0 1-.757-.97V10.5A.5.5 0 0 1 9 10c.516 0 1.706-.52 2.57-2.864.413-1.12.74-2.64.87-4.667.03-.463.049-.952.056-1.469H3.504z" />
                </svg>
                <h4>{{__('Our goal')}}</h4>
                <p>{{__('Customer feedback is the basis of what we do at Otzivio. We strive to provide authentic feedback that enables consumers to enter into a dialogue with the companies from which they buy')}}</p>
            </div>
            <div class="col-md-12 col-lg-4">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#008060" class="bi bi-heart"
                    viewBox="0 0 16 16">
                    <path
                        d="M8 2.748l-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z" />
                </svg>
                <h4>{{__('Otzivio is for you')}}</h4>
                <p>{{__('This platform is made by you and for you. By sharing your experience online, you contribute to creating better shopping for everyone. In the end, sharing something useful can only help')}}</p>
            </div>
        </div>
        <div class="row">
            <a href="{{url('p-za-otzivite')}}" class="colored-box__read-more">{{__('Read more')}} <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M17.707 9.293l-5-5a1 1 0 00-1.414 1.414L14.586 9H3a1 1 0 100 2h11.586l-3.293 3.293a1 1 0 001.414 1.414l5-5a1 1 0 000-1.414z"></path></svg></a>
        </div>
    </div>
    
    </div>

    @endsection

    @section('extraJS')

    <script src="/public/js/app.js"></script>

    <script type="application/ld+json">
        {
            "@context":"http://schema.org",
            "@type":"WebSite",
            "name":"Otzivio",
            "url":"https://otziv.io",
            "sameAs":[
                "https://www.facebook.com/otzivio",
                "https://www.linkedin.com/company/otzivio"
            ],
            "potentialAction":{
               "@type":"SearchAction",
               "target":"https://otziv.io/search?searchQuery={search_term}",
               "query-input":"required name=search_term"
            }
         }
    </script>
    
    <script type="application/ld+json">
    {
        "@context":"https://schema.org",
        "@type":"Organization",
        "name":"Otzivio",
        "legalName":"HASHTAG SHOPBUDDY LTD",
        "url":"https://www.otziv.io",
        "logo":"https://res.cloudinary.com/hashtag-shopbuddy-ltd/image/upload/v1621598485/cdn_dont_delete/mcjsc8fdnj0ulzaqx2ya.png",
        "foundingDate":"2020",
        "founders":[
           {
              "@type":"Person",
              "name":"Mladen Gorchev"
           }
        ],
        "address":{
           "@type":"PostalAddress",
           "streetAddress":"Kemp House 152-160 City Road",
           "addressLocality":"London",
           "addressRegion":"England",
           "postalCode":"33444",
           "addressCountry":"GB"
        },
        "contactPoint":{
           "@type":"ContactPoint",
           "contactType":"customer support",
           "telephone":"[+44-7491-974472]",
           "email":"help@otziv.io"
        },
        "sameAs":[
           "https://www.facebook.com/otzivio",
           "https://www.linkedin.com/company/otzivio"
        ]
     }
    </script>
    
    @endsection