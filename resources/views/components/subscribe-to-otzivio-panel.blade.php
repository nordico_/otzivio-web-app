<div class="subscribe-to-otzivio-panel">
    <div class="content text-center">
        <h3>{{__('subscribe_to_otzivio_panel_text_1')}}</h3>
        <a href="{{ route('businessPlans') }}" class="btn btn-lg btn-success mt-4">{{ __('Start for free!') }}</a>
    </div>
</div>

