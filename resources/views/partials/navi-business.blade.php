<nav class="navbar navbar-expand-xl navbar-dark fixed-top navbar-business">
  <a class="navbar-brand site-logo" href="{{ route('home') }}">

    @if($logo = Options::get_option( 'site.logo' ))
    <img src="/public/{{ $logo }}" height="30" alt="Otzivio logo" />
    @else
    <img src="/images/logo.svg" alt="Otzivio logo" />
    <span class="site-logo__extra-text">{{App::getLocale() == 'bg' ? '/ Бизнес' : '/ Business'}}</span>
    @endif
  </a>

  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
    data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false"
    aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse justify-content-end" id="navbarsExampleDefault">
    <ul class="navbar-nav">
      <li class="nav-item @if(isset($activeNav) && ($activeNav == 'overview')) active @endif">
        <a class="nav-link"
          href="{{ route('businessProduct') }}">{{ __('Product') }}</a>
      </li>
      <li class="nav-item @if(isset($activeNav) && ($activeNav == 'collect')) active @endif">
        <a class="nav-link"
          href="{{ route('businessCollect') }}">{{ __('Collect') }}</a>
      </li>
      <li class="nav-item @if(isset($activeNav) && ($activeNav == 'engage')) active @endif">
        <a class="nav-link" href="{{ route('businessEngage') }}">{{ __('Engage') }}</a>
      </li>
      <li class="nav-item @if(isset($activeNav) && ($activeNav == 'share')) active @endif">
        <a class="nav-link"
          href="{{ route('businessShare') }}">{{ __('Share') }}</a>
      </li>
      <li class="nav-item @if(isset($activeNav) && ($activeNav == 'pricing-plans')) active @endif">
        <a class="nav-link"
          href="{{ route('businessPlans') }}">{{ __('Pricing') }}</a>
      </li>
    </ul>
    <div class="nav-for-businesses pl-xl-2 mt-2 mt-xl-0">
      <a href="{{ route('home') }}">Към Отзивио</a>
    </div>
  </div>
</nav>