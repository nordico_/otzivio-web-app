<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="author" content="crivion">

  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="/images/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/images/favicons//favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/images/favicons//favicon-16x16.png">
  <link rel="manifest" href="/images/favicons//site.webmanifest">
  <link rel="mask-icon" href="/images/favicons//safari-pinned-tab.svg" color="#18806f">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="theme-color" content="#ffffff">


  @if (strpos(env("APP_URL"), 'staging') == true)
  <meta name="robots" content="noindex">
  @endif



  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

  <title>@if(isset($seo_title)) {{ $seo_title }} @else
    {{ Options::get_option( 'seo_title', 'Отзивио | Автентични отзиви от реални хора' ) }} @endif</title>

  @if( $d = Options::get_option( 'seo_desc' ) )
  <meta name="description" content="{{ $d }}" />
  @endif

  @if( $k = Options::get_option( 'seo_keys' ) )
  <meta name="keywords" content="{{ $k }}" />
  @endif

  <!-- Google Tag Manager -->
  <script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-K8VVB4G');
  </script>
  <!-- End Google Tag Manager -->

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
  <link rel="stylesheet" href="https://use.typekit.net/jyl5rvz.css">

  <!-- Custom styles for this template -->
  <link href="{{ asset('css/cookieconsent.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet">


  <!-- Bootstrap Select CDN -->
  <link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">

  <!-- extra CSS loaded by other views -->
  @yield( 'extraCSS' )

  @if( Options::get_option( 'extra_js' ) )
  {!! Options::get_option( 'extra_js' ) !!}
  @endif

  <!-- Google Ads -->
  <script data-ad-client="ca-pub-1873991222873561" async
    src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

</head>

<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K8VVB4G" height="0" width="0"
      style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
    <a class="navbar-brand site-logo" href="{{ route('home') }}">

      @if($logo = Options::get_option( 'site.logo' ))
      <img src="/public/{{ $logo }}" height="30" alt="Otzivio logo" />
      @else
      <img src="/images/logo.svg" alt="Otzivio logo" />
      @endif
      <span class="logo-beta">БЕТА</span>
    </a>
  </nav>
  <div class="star-source">

    <svg>
      <symbol id="star" viewBox="153 89 106 108">
        <polygon id="star-shape" stroke="url(#grad)" stroke-width="5" fill="currentColor"
          points="206 162.5 176.610737 185.45085 189.356511 150.407797 158.447174 129.54915 195.713758 130.842203 206 95 216.286242 130.842203 253.552826 129.54915 222.643489 150.407797 235.389263 185.45085">
        </polygon>
      </symbol>
    </svg>
  </div><!-- ./star-source -->

  <main role="main">

    <div class="main-wrapper" style="padding: 0">
      <div class="shadow p-3 mb-3 bg-white rounded">
        <div class="row">
          <div class="col-6 m-auto d-flex justify-content-center">
            <div>
              <h4 class="m-0 p-0">
                {{$site->business_name}}
              </h4>
              <p class="m-0 p-0">
                {{$site->url}}
              </p>
              <p class="m-0 p-0">
                {{$site->location}}
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12 col-lg-4 m-auto">
          <div class="shadow p-3 mb-5 bg-white rounded">
            <form method="post" action="{{ route('evaluate', [ 'site' => $site ]) }}">
              @csrf

              <input type="hidden" name="rating" value="{{$initialRating}}">
              <div class="star-container">
                <input type="radio" name="star" id="five">
                <label for="five">
                  <svg class="star" data-no="five" data-rating="5" data-toggle="modal" data-target="#review-modal">
                    <use xlink:href="#star" />
                  </svg>
                  <div class="text-star text-star-five">
                    {{ __('Great') }}
                  </div>
                </label>
                <input type="radio" name="star" id="four">
                <label for="four">
                  <svg class="star" data-toggle="modal" data-target="#review-modal" data-rating="4" data-no="four">
                    <use xlink:href="#star" />
                  </svg>
                  <div class="text-star text-star-four">
                    {{ __('Good') }}
                  </div>
                </label>
                <input type="radio" name="star" id="three">
                <label for="three">
                  <svg class="star" data-toggle="modal" data-target="#form-modal" data-no="three" data-rating="3">
                    <use xlink:href="#star" />
                  </svg>
                  <div class="text-star text-star-three">
                    {{ __('Okay') }}
                  </div>
                </label>
                <input type="radio" name="star" id="two">
                <label for="two">
                  <svg class="star" data-toggle="modal" data-target="#form-modal" data-no="two" data-rating="2">
                    <use xlink:href="#star" />
                  </svg>
                  <div class="text-star text-star-two">
                    {{ __('Subpar') }}
                  </div>
                </label>
                <input type="radio" name="star" id="one">
                <label for="one">
                  <svg class="star" data-toggle="modal" data-target="#form-modal" data-no="one" data-rating="1">
                    <use xlink:href="#star" />
                  </svg>
                  <div class="text-star text-star-one">
                    {{ __('Bad') }}
                  </div>
                </label>
              </div><!-- ./star-container -->

              <div class="form-group">
                <label>{{ __('Review Title') }}</label>
                <input class="form-control" type="text" placeholder="{{ __('ie. Very good company') }}"
                  name="review_title" value="{{ old('review_title') }}" required="required">
              </div>
              <div class="form-group">
                <label>{{ __( 'Description' ) }}</label>
                <textarea class="form-control" style="height: 180px;" name="review_content"
                  placeholder="{{ __('Let others know about your experience with this company') }}"
                  required="required">{{ old( 'review_content' ) }}</textarea>
              </div>

              @auth
              <button type="submit" name="sbReview" class="btn btn-primary btn-block">{{ __('Post Review') }}</button>
              @else
              <a class="btn btn-primary btn-block text-white"
                href="{{route('login').'?return='.getReturnUrl()}}">{{ __('Login') }}</a>
              <!--<a class="btn btn-primary btn-block text-white"
                href="{{route('register').'?return='.getReturnUrl()}}">{{ __('Register') }}</a>-->
              @endauth

            </form>
          </div>
        </div>
      </div>

    </div>

  </main>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
    jQuery(document).ready(function($) {


              // show initial rating if url has ?rating=1
              let initialRating = "{{$initialRating}}";
              
              let numberToString = {
                1:'one',
                2:'two',
                3:'three',
                4:'four',
                5:'five',
              }
              for (let index = 1; index <= initialRating; index++) {
                let inp = $(`#${numberToString[index]}`).prop("checked", true);
              } 
  
              // handle upvoting
              $( '.upvote' ).click( function() {
  
                  var review = parseInt($( this ).data( 'review' ));
  
                  $.getJSON( '{{ route('vote', ['review' => '/']) }}/' + review, function( r ) {
  
                      if (r.hasOwnProperty('error')) {
                          var oopsMsg = '{{__("Oops...")}}';
                          sweetAlert(oopsMsg, r.error, "error");
                      }else{
                          $( 'span.upvotes[data-review="' + review +'"]' ).html( r.upvotes );
                      }
  
                  });
                  
              });
      
              // handle text when hovering stars!
              $( '.star' ).hover( function() {
  
                  // define which star was clicked
                  var dataNo = $( this ).data( 'no' );
  
                  // hide all other texts
                  $( '.text-star' ).hide();
  
                  // reveal text under hovered star
                  $( '.text-star-' + dataNo ).show();
  
              }, 
              function() {
  
              });
  
              // add rating value into the form input
              $( '.star' ).click( function() {
  
                  var rating = $( this ).data( 'rating' );
                  
                  $( "input[name=rating]" ).val( rating );
  
                  console.log( 'Rating Chosen: ' + rating );
  
              });
  
              $( '.btn-toggle-review-form' ).click( function() {
  
                  $( '.review-form-toggle' ).toggle();
  
              });
  
              $( '.btn-reply' ).click( function() {
                  var replyID = $( this ).data( 'id' );
                  $(this).hide();
                  
                  var replyForm = $("form[name=replyAsCompany" + replyID + "]");
                  replyForm.show();
  
  
  
              });
  
          });
  </script>

</body>

</html>