<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use TCG\Voyager\Facades\Voyager;
use App\FormFields\BSDateTimeFormField;
use Illuminate\Support\Facades\View;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        \Schema::defaultStringLength(191);
        View::share('ip', request()->ip());
        View::share('en_url', LaravelLocalization::getLocalizedURL('en'));
        View::share('bg_url', LaravelLocalization::getLocalizedURL('bg'));
        View::share('locale', LaravelLocalization::getCurrentLocale());
        View::share('activeNav',basename(request()->url()));
        \Carbon\Carbon::setLocale(LaravelLocalization::getCurrentLocale());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
