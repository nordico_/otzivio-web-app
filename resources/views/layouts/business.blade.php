<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include( 'partials/head' )

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K8VVB4G" height="0" width="0"
        style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="app">
        @include( 'partials/navi-business' )

        <main>
            @yield('content')
        </main>    
    </div>

    @include( 'partials/footer' )
    @include( 'partials/body-scripts' )

    <!-- Chat Widget -->
    <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="d8e98833-3af8-4ab3-9efe-1d32d763a6fc";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
</body>
</html>
