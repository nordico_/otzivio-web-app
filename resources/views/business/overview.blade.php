@extends('layouts.business') 
@section('content')
<div class="for-businesses-page">
	<div class="inner-page__title text-center">
		<h1>{{ __('business_overview_page_title') }}</h1>
		<p class="inner-page__subtitle">{{ __('business_overview_page_subtitle') }}</p>
	</div>
	<div class="container">
		<div class="row for-businesses__main-points">
			<div class="col col-12 col-md-6 text-center">
				<img src="/images/business/for-business-image.svg" style="max-width: 460px; width: 100%;" />
			</div>
			<div class="col col-12 col-md-6 text-md-left text-center pt-4 pt-md-0">
				<ul>
					<li><i class="fas fa-check"></i> {{ __('business_overview_hero_point_1') }}</li>
					<li><i class="fas fa-check"></i> {{ __('business_overview_hero_point_2') }}</li>
					<li><i class="fas fa-check"></i> {{ __('business_overview_hero_point_3') }}</li>
				</ul>
				<a href="{{route('businessPlans')}}" class="btn btn-lg btn-success mt-4">{{__('business_overview_page_get_started')}}</a>
			</div>
		</div>
		<h2 class="text-center mt-5">{{ __('How Otzivio will help your business?') }}</h2>
		<div class="text-center">
			<a href="/business/product" class="arrow__link"><span>{{ __('Learn more about the product') }}</span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M17.707 9.293l-5-5a1 1 0 00-1.414 1.414L14.586 9H3a1 1 0 100 2h11.586l-3.293 3.293a1 1 0 001.414 1.414l5-5a1 1 0 000-1.414z"></path></svg></a>
		</div>
		<div class="row">
			<div class="col col-12 text-center text-md-left col-md-4 mt-5">
				<img src="/images/business/better_conversion.svg" />
				<h3 class="mt-4">{{ __('Increased conversion') }}</h3>
				<p>{{ __("It's a simple equation. Reviews increase trust. In fact, you can improve your conversion rate up to 40%.") }}</p>
			</div>
			<div class="col col-12 text-center text-md-left col-md-4 mt-5">
				<img src="/images/business/reputation_management.svg" />
				<h3 class="mt-4">{{ __('business_overview_page_reputation_management') }}</h3>
				<p>{{ __('business_overview_page_reputation_management_body') }}</p>
			</div>
			<div class="col col-12 text-center text-md-left col-md-4 mt-5">
				<img src="/images/business/verified_reviews.svg" />
				<h3 class="mt-4">{{ __('Verified Reviews') }}</h3>
				<p>{{ __('Rest assured, your reviews come from your actual customers. A unique link backed by reference ID (e.g. order number) identifies each customer as the real deal.') }}</p>
			</div>
		</div>
	</div>
	<div class="for-businesses__how-it-works pb-5">
		<div class="container">
			<h2 class="text-center">{{ __('See Otzivio in action') }}</h2>
			<div class="text-center mb-md-5">
				<a href="/business/product" class="arrow__link"><span>{{ __('Learn more about the product') }}</span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M17.707 9.293l-5-5a1 1 0 00-1.414 1.414L14.586 9H3a1 1 0 100 2h11.586l-3.293 3.293a1 1 0 001.414 1.414l5-5a1 1 0 000-1.414z"></path></svg></a>
			</div>
			<div class="row mt-3">
				<div class="col col-12 col-md-6 pr-md-5 mb-4 mb-md-0">
					<div class="video-container">
						<div class="video-container__video-wrapper">
							<iframe src="https://www.youtube.com/embed/sI9RY6uIlEo?showinfo=0&rel=0&color=white&controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
				<div class="col col-12 text-center text-md-left col-md-6 pl-md-5">
					<h3>{{ __('We collect reviews for you.') }}</h3>
					<p>{{ __('We just need to send one email.') }}</p>
					<h3>{{ __('You engage with your customers.') }}</h3>
					<p>{{ __('Respond to reviews and start a conversation.') }}</p>
					<h3>{{ __('You share your trustworthiness.') }}</h3>
					<p>{{ __('Spread trust and amplify your customer service.') }}</p>
				</div>
			</div>
		</div>
	</div>
	@component('components/subscribe-to-otzivio-panel') @endcomponent
</div>
@endsection