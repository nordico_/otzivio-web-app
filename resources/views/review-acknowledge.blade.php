@extends( 'base' )

@section( 'content' )
<div class="company-page">
    <div class="container-fluid card inner-site-header">
        <div class="container">
            <div class="row">
                <h4 class="text-center m-auto">
                    {{__("Thank you for reviewing company $site->business_name.")}}
                    @if ($review->is_verified == "No")
                    {{__("\nWe will need to first approve you review before you can see it live. This could take
                                    from few minutes to few hours.")}}
                    @endif
                </h4>
            </div>

            <div class="row mt-4">
                <a href="{{route('reviewsForSite',$site)}}" class="btn btn-primary btn-rounded m-auto">
                    {{__("Go back to $site->business_name profile")}}
                </a>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div><!-- /.container -->
    <!-- /.container -->
</div>

@endsection