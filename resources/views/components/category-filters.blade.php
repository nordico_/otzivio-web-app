<form method="GET">
    <hr>

    <strong>{{ __( 'Category' ) }}</strong><br>
    
    <a href="{{ route('browse-category', [ 'slug' => 'top-companies' ]) }}">
        {{ __('Top Companies') }}
    </a>
    <br>

    @foreach( $all_categories as $c ) 
    <a href="{{ route('browse-category', [ 'slug' => $c->slug ]) }}">
        {{ $c->name }}
    </a><br>
    @endforeach
    <br>

    @if( is_null( $location ) )
    <strong>{{ __( 'Местоположение' ) }}</strong>
    <input type="text" name="location" class="form-control" id="city_region">
    <input type="hidden" name="lati" id="lati">
    <input type="hidden" name="longi" id="longi">
    @else
    <span class="tag tag-primary"><i class="fa fa-globe"></i> {{ $location }} <a href="{{ $resetURL }}" class="text-primary">[{{ __('Reset Location') }}]</a></span>
    <br>
    @endif
    <br>
    
    <strong>{{ __( 'Сортирай' ) }}</strong>
    <ul class="list-unstyled">
    <li><input type="radio" name="sortBy" value="default" @if(!request()->has('sortBy') OR request( 'sortBy' ) == 'default') checked="" @endif> {{ __('Default') }}</li>
    <li><input type="radio" name="sortBy" value="best" @if(request('sortBy') == 'best') checked @endif> {{ __('Best Rated') }}</li>
    <li><input type="radio" name="sortBy" value="worst" @if(request('sortBy') == 'worst') checked @endif> {{ __('Worst Rated') }}</li>
    <li><input type="radio" name="sortBy" value="most-reviews" @if(request('sortBy') == 'most-reviews') checked @endif> {{ __('Most Reviewed') }}</li>
    <li><input type="radio" name="sortBy" value="least-reviews" @if(request('sortBy') == 'least-reviews') checked @endif> {{ __('Least Reviewed') }}</li>
    </ul><!-- /.list-unstyled -->

    <strong>{{ __( 'Брой отзиви' ) }}</strong>
    <ul class="list-unstyled">
    <li><input type="radio" name="reviewsCount" value="0" @if(!request()->has('reviewsCount') OR request('reviewsCount') == '0') checked @endif> {{ __('All') }}</li>
    <li><input type="radio" name="reviewsCount" value="25" @if(request('reviewsCount') == 25) checked @endif> 25+</li>
    <li><input type="radio" name="reviewsCount" value="50" @if(request('reviewsCount') == 50) checked @endif> 50+</li>
    <li><input type="radio" name="reviewsCount" value="100" @if(request('reviewsCount') == 100) checked @endif> 100+</li>
    <li><input type="radio" name="reviewsCount" value="250" @if(request('reviewsCount') == 250) checked @endif> 250+</li>
    <li><input type="radio" name="reviewsCount" value="500" @if(request('reviewsCount') == 500) checked @endif> 500+</li>
    <li><input type="radio" name="reviewsCount" value="1000" @if(request('reviewsCount') == 1000) checked @endif> 1k+</li>
    </ul><!-- /.list-unstyled -->

    <strong>{{ __( 'Статус на компанията' ) }}</strong>
    <ul class="list-unstyled">
    <li><input type="radio" name="companyStatus" value="all" @if(!request()->has( 'companyStatus' ) OR request('companyStatus') == 'all') checked @endif> {{ __('All') }}</li>
    <li><input type="radio" name="companyStatus" value="claimed" @if(request('companyStatus') == 'claimed') checked @endif> {{ __('Claimed') }}</li>
    <li><input type="radio" name="companyStatus" value="unclaimed" @if(request('companyStatus') == 'unclaimed') checked @endif> {{ __('Unclaimed') }}</li>
    </ul><!-- /.list-unstyled -->

    <hr>
    <input type="submit" name="sbFilter" value="{{ __('Apply Filters') }}" class="btn btn-primary">
</form>