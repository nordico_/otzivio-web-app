<?php

namespace App\Services;

use App\User;
use Carbon\Carbon;
use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialGoogleAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $user = User::whereProvider(USER::GOOGLE_AUTH_PROVIDER)
            ->whereProviderUserId($providerUser->id)
            ->first();

        if (!$user) {
            $user = User::whereEmail($providerUser->email)->first();
            if (!$user) {
                $user = User::create([
                    'provider_user_id' => $providerUser->id,
                    'provider' => USER::GOOGLE_AUTH_PROVIDER,
                    'email' => $providerUser->email,
                    'name' => $providerUser->name,
                    'profilePic' => $providerUser->avatar_original,
                    'email_verified_at' => Carbon::now(),
                ]);
                $user->save();
            }
        }

        return $user;
    }
}
