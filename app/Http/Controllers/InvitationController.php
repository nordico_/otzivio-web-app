<?php

namespace App\Http\Controllers;

use App\Exports\CustomerImportSampleExcel;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class InvitationController extends Controller
{

    public function __construct()
    {
        $this->middleware('paidSubscriptionUsersOnly')->except('index');
    }

    public function index()
    {
        $site = auth()->user()->company;
        $customers = $site->invitedCustomers()->paginate(50);
        return view('invitation-history', compact('customers'));
    }

    public function downloadSample()
    {
        return Excel::download(new CustomerImportSampleExcel(), 'sample.csv');
    }

    public function downloadErrorFile($filename)
    {
        $path = 'public/storage/excel/errors/' . $filename . '.xlsx';
        return response()->download($path, $filename . '.xlsx', [
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'inline; filename="' . $filename . '.xlsx' . '"'
        ]);
    }

    public function showInvitePage()
    {
        $site= auth()->user()->company;
        $pendingInvitation = auth()->user()->pendingInvite()->with('invitedCustomers')->first();
        $siteEmails = $site->getEmailList();
        if (!in_array(auth()->user()->email, $siteEmails)) $siteEmails[] = auth()->user()->email;
        $template = $site->getInvitationEmailTemplate();
        $subject = $site->getInvitationEmailSubject();


        return view('invite-customers', compact('pendingInvitation', 'siteEmails', 'template','subject'));
    }

    public function previewInvitation()
    {
        $markdown = new \Illuminate\Mail\Markdown(view(), config('mail.markdown'));
        $data['html'] =  session('preview-template');
        return $markdown->render("emails.invitation", compact('data'));
    }

    public function showInvitationSettingsPage(){
        $settings = auth()->user()->company->getSettings();
        return view('invitation-settings', compact('settings'));
    }
    
    public function showEmailTemplatePage(Request $request)
	{
        $settings = auth()->user()->company->getSettings();
        $siteSettings = auth()->user()->company->siteSettings;
		return view('email-template',compact('settings','siteSettings'));
	}
}
