<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
    <meta name="author" content="crivion">
  
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicons//favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicons//favicon-16x16.png">
    <link rel="manifest" href="/images/favicons//site.webmanifest">
    <link rel="mask-icon" href="/images/favicons//safari-pinned-tab.svg" color="#18806f">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
  
    <meta property="og:image" content="{{url('/images/logo-social.jpg')}}"/>
  
    @if (strpos(env("APP_URL"), 'staging') == true)
    <meta name="robots" content="noindex">
    @endif

    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    {!! Twitter::generate() !!}
  
    <!-- Google Tag Manager -->
    <script>
      (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-K8VVB4G');
    </script>
    <!-- End Google Tag Manager -->
  
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
      integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://use.typekit.net/jyl5rvz.css">
  
    <!-- Custom styles for this template -->
    <link href="{{ asset('css/cookieconsent.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet">
  
    <!-- SLick Slider -->
    <link href="{{ asset('css/slick.css') }}" rel="stylesheet" type="text/css" />
  
    <!-- Bootstrap Select CDN -->
    <link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">
  
    <!-- extra CSS loaded by other views -->
    @yield( 'extraCSS' )
  
    @if( Options::get_option( 'extra_js' ) )
    {!! Options::get_option( 'extra_js' ) !!}
    @endif
  
    <!-- Google Ads -->
    <script data-ad-client="ca-pub-1873991222873561" async
      src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
  
  </head>