<?php

namespace App\Listeners;

use App\SiteEmail;

class AttachEmailToSite
{

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $company = $event->user->company;
        if (!$company) return;

        SiteEmail::updateOrCreate([
            'site_id' => $company->id,
            'email' => $event->user->email,
        ], [
            'email_verified_at' => $event->user->email_verified_at,
        ]);

        if (session()->has('ownershipEmail')) {
            $email = session('ownershipEmail');
            SiteEmail::updateOrCreate([
                'site_id' => $company->id,
                'email' => $email,
            ], [
                'email_verified_at' => now(),
            ]);
        }

        session()->forget('ownershipEmail');
    }
}
