<?php
namespace App\Services;

use App\InvitedCustomer;
use App\Sites;
use Carbon\Carbon;
use GuzzleHttp\Client;

class AutomaticFeedbackSystemService{

    private $client;
    private $namespace;
    private $apiKey;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->namespace = env('TESTMAIL_NAMESPACE');
        $this->apiKey=env('TESTMAIL_API_KEY');
    }

    public function initiate()
    {
       $data = $this->fetch();
       if($data && $data->count>0) $this->evaluate($data->emails);
    }
    public function fetch()
    {
        $today =(string)strtotime('Europe/Sofia today') . "000";
        $url = 'https://api.testmail.app/api/json?apikey='.$this->apiKey.'&namespace='.$this->namespace.'&pretty=true&limit=100&timestamp_from='.$today;
        $response =$this->client->get($url);

        return json_decode($response->getBody()->getContents());
    }

    public function evaluate($emails){
        $namespace = env('TESTMAIL_NAMESPACE').".";
        $provider = "@inbox.testmail.app";
        for($i=0;$i<count($emails);$i++){
            $mail = $emails[$i];
            
            $to = $mail->to_parsed[0];

            $receiverName = null;
            $receiverEmail = null;
            $referenceId = null;

            if(str_contains($to->address,$this->namespace)){
                // this email contains Structured data in the body
                $structuredData = $this->evaluateStructuredData($mail->html);
                if($structuredData == null) continue;

                $receiverEmail = $structuredData->recipientEmail;
                $receiverName = $structuredData->recipientName;
                $referenceId = $structuredData->referenceId;

            }else{
                $receiverEmail = $to->address;
                $receiverName = $to->name;
            }
            
            $companyUrl = str_replace([$namespace,$provider],['',''],$mail->envelope_to);

            if($receiverEmail){
                $this->processEmail($companyUrl,$receiverEmail,$receiverName,$referenceId);
            }
        }
    }


    // sample for structured data
    // <!-- <script>
    // {
    //     "recipientEmail":"pahadiashok2@gmail.com",
    //     "recipientName":"Ashok Pahadi",
    //     "referenceId":"121212"
    // }
    // </script> -->

    private function evaluateStructuredData($body){
        $body = trim(preg_replace('/\s+/', '', $body));
        $structuredData = $this->getStringBetween($body,'<script>','</script>');
        $structuredData = '{'.$this->getStringBetween($body,'{','}') . '}';
        $structuredData = str_replace(['"""'],[''],$structuredData);
        $structuredData = json_decode($structuredData);

        if (json_last_error() === JSON_ERROR_NONE) {
            // the data is valid and contains json
            return $structuredData;
        }
        return null;
    }

    private function getStringBetween($str,$from,$to) {
        $string = substr($str, strpos($str, $from) + strlen($from));
        if (strstr ($string,$to,TRUE) != FALSE) {
            $string = strstr ($string,$to,TRUE);
        }
        return $string;
    }

    public function processEmail($url,$email,$name,$referenceId){
        $company = Sites::where('url',$url)->first();
        if(!$company  || !$company->claimer) return;
        
        if($name == null) $name = explode('@',$email)[0];

        if(InvitedCustomer::where('email',$email)
            ->where('name',$name)
            ->where('site_id',$company->id)
            ->where('is_afs',true)
            ->whereDate('created_at', Carbon::today())
            ->first()!=null) return;
        
        InvitedCustomer::create([
            'email'=>$email,
            'name'=>$name,
            'status'=> InvitedCustomer::NOT_STARTED,
            'site_id'=>$company->id,
            'reference_number'=>$referenceId,
            'is_afs'=>true,
        ]);
    }
}