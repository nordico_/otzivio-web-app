<?php

namespace App\Http\Controllers\Api;

use App\Events\SiteEmailCreated;
use App\Http\Controllers\Controller;
use App\Mail\VerifySiteEmail;
use App\SiteEmail;
use App\Sites;
use Illuminate\Support\Facades\Validator;
use Mail;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{
    public function addEmail(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'status' => 'invalid_email']);
        }

        $site = auth()->user()->company;

        $siteEmail = SiteEmail::where('email', $request['email'])->where('site_id', $site->id)->first();

        if ($siteEmail) return response()->json(['success' => false, 'data' => [], 'status' => 'email_exists', 'message' => 'This email added!']);

        $siteEmail = SiteEmail::create([
            'email' => $request['email'],
            'site_id' => $site->id,
        ]);

        $siteEmail->sendEmailVerificationNotification();


        return response()->json(['success' => true, 'data' => ['email' => $request['email']], 'status' => 'email_delivered', 'message' => 'Email delivered successfully!']);
    }
}
