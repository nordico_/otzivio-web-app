@extends( 'base' )

@section( 'content' )

<div class="container" id="app">

    <div class="row">
		<div class="col-12 mx-auto">
			<div class="card">
				<a class="mb-4" href="{{route('mycompany')}}"><i class="fa fa-chevron-left mr-2" aria-hidden="true"></i>{{ trans('Go back to My Company') }}</a>
				<h2>{{ __('Invitation Settings') }}</h2>
				<hr>
                <invitation-settings :settings="{{\json_encode($settings)}}"></invitation-settings>
			</div>
		</div>
	</div>
</div>

@endsection

@section('extraJS')
<script src="/public/js/app.js"></script>
@endsection