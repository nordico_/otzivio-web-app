@extends('admin.base')

@section('styles')
<style>
  .select2{
    width: 100% !important;
    display: block;
  }
  .select2-selection__choice{
    color: black !important;
  }
</style>
@endsection

@section('section_title')
	<strong>Review Questionnaire</strong>
@endsection

@section('section_body')

<div class="row">
	<div class="col-xs-12 col-md-12">
    <div class="row">
		@if( empty( $category ) )
		<form method="POST" action="/admin/add_category" enctype="multipart/form-data">
		@else
		<form method="POST" action="/admin/update_category" enctype="multipart/form-data">
		<input type="hidden" name="catID" value="{{ $catID }}">
		@endif
			{!! csrf_field() !!}
      <div class="col-md-6">
        <div class="form-group">
          <label for="file">Name EN</label>
          <input required type="text" name="catname_en" value="{{ empty($category) ? "": \json_decode($category->getRawOriginal('name'))->en ?? '' }}" class="form-control">
        </div>
      </div>
	  <div class="col-md-6">
        <div class="form-group">
          <label for="file">Question EN</label>
          <input required type="text" name="question_en" value="{{ empty($category) ? "": \json_decode($category->getRawOriginal('question'))->en ?? '' }}" class="form-control">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="file">Name BG</label>
          <input required type="text" name="catname_bg" value="{{ empty($category) ? "": \json_decode($category->getRawOriginal('name'))->bg ?? '' }}" class="form-control">
        </div>
      </div>
	  <div class="col-md-6">
        <div class="form-group">
          <label for="file">Question BG</label>
          <input required type="text" name="question_bg" value="{{ empty($category) ? "": \json_decode($category->getRawOriginal('question'))->bg ?? '' }}" class="form-control">
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="companies">Companies</label>
          <select required multiple id="select-2" name="companies[]">
            @foreach ($companies as $company)
              <option data-image="{{$company->featured_image}}" {{!empty( $category) && in_array($company->id,$selectedCompanyIds) ? 'selected':''}} value="{{$company->id}}">{{$company->business_name}}</option>
            @endforeach
          </select>
        </div>
      </div>


	  <div class="col-md-6" >
		 <h5> <strong>Selected Companies</strong></h5>
			<ul style="list-style: none;padding:0" id="selected-companies">
			</ul>
		</div>
      </div>


		<input type="hidden" name="type" value="{{ $type }}" class="form-control">
		<input type="hidden" name="return" value="homepage" class="form-control">
		<br/>
		<div style="display:flex;justify-content: flex-end" class="col-md-12 d-flex align-items-end">
        	<input type="submit" name="sb" value="Save Category" class="btn btn-primary">
      	</div>

	  
	  
	</form>
  </div>
	</div><!-- /.col-xs-12 col-md-6 -->
</div><!-- /.row -->

<br/>
<hr/>

@if($categories)
	<table class="table table-striped table-bordered table-responsive">
	<thead>
	<tr>
		<th>ID</th>
		<th>Category</th>
		<th>Companies</th>
		<th>Actions</th>
	</tr>
	</thead>
	<tbody>
		@foreach( $categories as $c )
		<tr>
			<td>
				{!! $c->id !!}
			</td>
			<td>
				{{ $c->name }}
			</td>
			<td>
				@foreach ($c->sites as $company)
					<a  href="{{route('reviewsForSite',['site'=>$company])}}" target="_" > <button class="btn btn-sm btn-secondary">{{$company->business_name}}</button></a>
				@endforeach
			</td>
			</td>
			<td>
				 <div class="btn-group">
				 	<a class="btn btn-primary btn-xs" href="/admin/homepage?update={!! $c->id !!}">
				 		<i class="glyphicon glyphicon-pencil"></i>
				 	</a>
    				<a href="/admin/homepage?remove={!! $c->id !!}" onclick="return confirm('IMPORTANT! Any descendant that category has will also be deleted!');" class="btn btn-danger btn-xs">
						<i class="glyphicon glyphicon-remove"></i>
					</a>
				</div>
			</td>
		</tr>
		@endforeach
	</tbody>
	</table>
@else
	No categories in database.
@endif

@endsection
@section('scripts')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
	let isUpdating = false;
  $(document).ready(function() {
	$('#select-2').select2();
	$('#select-2').on('select2:select', function (e) {
		element = e.params.data;
		console.log($(element.element).data('image'));
		$("#selected-companies").append(buildCompanyItem(element.text,element.id,$(element.element).data('image')))
	});
	$('#select-2').on('select2:unselect', function (e) {
		element = e.params.data;
		removeCompanyItem(element.id)
	});
	$('#selected-companies').on('click', function (e) {
		if(e.target.className.includes('remove')){
			unSelectCompany($(e.target).data('id').toString())
		}
	});
	
	let selectedCompanies = <?php echo json_encode($selectedCompanies); ?>;
	isUpdating = selectedCompanies.length!=0;
	selectedCompanies.forEach((element,index) => {
		$("#selected-companies").append(buildCompanyItem(element.business_name,element.id,element.featured_image))
	});

  });


  function buildCompanyItem(name,id,image){
	  let el = $("<li style='margin-bottom:8px'/>")
	  		.attr("id","company-"+id)
	  		.addClass("d-flex align-items-center")
			.html(`<h5 style="display:inline-block;min-width:200px"><span style="margin-left: 10px">${id}. ${name}</span></h5><input style="display:inline-block;max-width:400px;margin-left:10px;" type="file" ${image ? '' : 'required'} name="thumbnails[${id}]" class="form-control" accept="image/*"><a style="margin-left: 4px" data-id="${id}" class="btn btn-danger btn-xs remove-item"><i  data-id="${id}" class="glyphicon glyphicon-remove"></i></a>`);
		if(image) el.append(`<img src="/${image}" style="display:block;margin-left:8px;max-height:100px" \>`)
		return el;
  }
  function removeCompanyItem(id){
	$("#company-"+id).remove();
  }
  function unSelectCompany(id){
	var $select = $('#select-2');
	var values = $select.val();
	console.log(id);
	if (values) {
		var i = values.indexOf(id);
		if (i >= 0) {
			values.splice(i, 1);
			$select.val(values).change();
			removeCompanyItem(id)
		}
	}

  }
  
</script>
@endsection