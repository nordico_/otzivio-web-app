<?php

namespace App\Events;

use App\SiteEmail;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SiteEmailCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $siteEmail;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(SiteEmail $siteEmail)
    {
        $this->$siteEmail = $siteEmail;
    }
}
