@extends( 'base' )

@section( 'content' )

<div class="container card">
	<h4>{{ __( 'Потвърдете собствеността върху' ) }} {{ $company->business_name }}</h4>
	
	{{ __( 'За да потвърдим собствеността на тази компания, ще ви изпратим имейл до избрания от Вас имейл') }}
	{{ '****@' . str_ireplace('www', '', $company->url) }}

	<div class="row">
		<div class="col-md-6">
		<form method="POST" action="{{ route('verifyOwnershipForm', [ 'site' => $company->url ]) }}">
			@csrf
			<br>
			{{ __( 'Въведете имейл за този домейн: ' )}}
			<br>

			<input type="hidden" name="plan" value="{{ $plan }}">

			<div class="input-group mb-3">
			  <input type="text" class="form-control" placeholder="{{ __("Потребителско име на получателя") }}" aria-label="Потребителско име на получателя" aria-describedby="basic-addon2" required="required" name="username">
			  <div class="input-group-append">
			    <span class="input-group-text" id="basic-addon2">{{ '@'. $company->url }}</span>
			  </div>
			</div>

			<input type="submit" name="sb" value="{{ __('Send Verification Email') }}" class="btn btn-primary">
		</form>
	</div><!-- /.col-md-6 -->
	</div><!-- /.row -->
</div><!-- /.container -->

@endsection