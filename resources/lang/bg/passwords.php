<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Паролите трябва да са поне шест знака и да съответстват на потвърждението.',
    'reset' => 'Паролата Ви беше успешно променена!',
    'sent' => 'Изпратихме по имейл връзката за промяна на паролата.',
    'token' => 'Този маркер за промяна на паролата е невалиден.',
    'user' => "Не можем да намерим потребител с този имейл адрес.",

];
