<?php

namespace App\Listeners;

use App\Events\SiteEmailCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendSiteEmailVerification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function handle(SiteEmailCreated $event)
    {
        if (!$event->siteEmail->hasVerifiedEmail()) {
            $event->siteEmail->sendEmailVerificationNotification();
        }
    }
}
