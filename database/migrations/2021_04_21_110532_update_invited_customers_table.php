<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateInvitedCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invited_customers', function (Blueprint $table) {
            
            $table->string('reference_number')->nullable()->change();

            $table->boolean('is_afs')->default(false);
            
            $table->unsignedInteger('site_id')->nullable();

            $table->foreign('site_id')->references('id')->on('sites')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invited_customers', function (Blueprint $table) {
            $table->string('reference_number')->change();
            $table->dropColumn('is_afs');
            $table->dropColumn('site_id');
        });
    }
}
