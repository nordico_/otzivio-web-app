<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Rinvex\Categories\Models\Category as ModelsCategory;

class Category extends ModelsCategory
{
    public const CATEGORY_TYPE_INDUSTRY = 'industry';
    public const CATEGORY_TYPE_PURPOSE = 'purpose';

    protected $tableName = 'categories';

    public $translatable = [
        'name',
        'description',
        'question'
    ];


    public function scopeIndustryType($query){
        return $query->whereType(Category::CATEGORY_TYPE_INDUSTRY);
    }
    public function scopePurposeType($query){
        return $query->whereType(Category::CATEGORY_TYPE_PURPOSE);
    }

    protected $fillable = ['name','type'];

    public function scopeHavingName($query,String $search){
        $formattedSearch = str_slug(strtolower($search));
        if($formattedSearch=="") $formattedSearch = $search;
        return $query->where('slug','like', '%' . $formattedSearch . '%');
    }
    
    protected $appends = ['title','redirect_url'];

    public function getTitleAttribute(){
        return ucwords(str_replace("-"," ",$this->slug));
    }
    public function getRedirectUrlAttribute(){
        return LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(), route('browse-category',[
            'slug'=>$this->slug
        ]));
    }

    public function categorizableSites(){
        return $this->hasMany(Categorizable::class,'category_id')->where('categorizable_type',Sites::class);
    }

    public function sites()
    {
        return $this->morphedByMany(Sites::class, 'categorizable', config('rinvex.categories.tables.categorizables'), 'category_id', 'categorizable_id', 'id', 'id');
    }
}
