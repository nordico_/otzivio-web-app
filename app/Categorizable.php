<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\MorphPivot;

class Categorizable extends MorphPivot
{
    protected $table= "categorizables";

    protected $fillable = ['category_id','categorizable_id','categorizable_type'];
}
