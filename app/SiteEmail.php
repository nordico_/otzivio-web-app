<?php

namespace App;

use App\Notifications\VerifySiteEmailNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class SiteEmail extends Model
{
    use Notifiable;
    protected $fillable = ['site_id', 'user_id', 'email', 'email_verified_at'];

    /**
     * Determine if the user has verified their email address.
     *
     * @return bool
     */
    public function hasVerifiedEmail()
    {
        return $this->email_verified_at != null;
    }

    /**
     * Mark the given user's email as verified.
     *
     * @return bool
     */
    public function markEmailAsVerified()
    {
        $this->update([
            'email_verified_at' => now(),
        ]);
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifySiteEmailNotification);
    }

    /**
     * Get the email address that should be used for verification.
     *
     * @return string
     */
    public function getEmailForVerification()
    {
        return $this->email;
    }

    public function scopeVerified($query)
    {
        return $query->whereNotNull('email_verified_at');
    }
    public function isVerified()
    {
        return $this->email_verified_at != null;
    }
}
