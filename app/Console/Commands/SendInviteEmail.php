<?php

namespace App\Console\Commands;

use App\InvitedCustomer;
use App\Jobs\SendInvitationEmail;
use App\Services\InviteCustomerEmailService;
use App\Sites;
use App\SiteSetting;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendInviteEmail extends Command
{


    protected $signature = 'emails:send';

    public function handle()
    {
        $emailsToBeSent = InvitedCustomer::with('site')->AFS()->notStarted()->get();
        
        foreach($emailsToBeSent as $customer){
            $site = $customer->site;

            if($customer->created_at->lt(Carbon::now()->subDays($site->getSettings()[SiteSetting::SETTING_KEY_SEND_INVITATION_AFTER]))){
                var_dump('sending email to '.$customer->email);
                $owner = $site->claimer;
                $template = $site->getInvitationEmailTemplate();
    
                $job = new SendInvitationEmail(
                    $customer,
                    InviteCustomerEmailService::process($template,$owner->email , $customer, $site),
                    null,
                    false,
                    $site,
                );
                dispatch($job);
            }

        }
    }
}
