<?php

namespace App\Services;

use App\Reviews;
use App\Sites;
use Mail;
use App\Mail\EmailNotification;
use Options;

class CompanyReviewService
{
    public function create(Sites $site, $rating = 0, $reviewTitle, $reviewContent, $userId)
    {
        // insert review
        $review = new Reviews(['rating' => $rating, 'review_title' => $reviewTitle, 'review_content' => $reviewContent]);

        $review->user_id = $userId;

        // save review
        $id = $site->reviews()->save($review);

        // set sweet alert message
        alert()->success(__('Your review was sent to review and will soon be published if it abides by our TOS'), __('Thank you'));

        // notify admin by email
        $data['message'] = sprintf(
            __('New review to %s
                                  Reviewer: %s
                                  Title: %s
                                  Rating: %s
                                  Review Content: %s'),

            '<strong>' . $site->url . '</strong><br>',
            $review->reviewer_name . '<br>',
            $review->review_title . '<br>',
            $review->rating . '<br>',
            $review->review_content
        );

        $data['intromessage'] = __('New Review awaiting approval');
        $data['url'] = route('reviewsForSite', ['site' => $site->url]);
        $data['buttonText'] = __('See Review');

        Mail::to(Options::get_option('adminEmail'))->send(new EmailNotification($data));
    }
}
