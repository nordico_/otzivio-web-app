<?php

namespace App\Providers;

use App\Events\CompanyClaimed;
use App\Events\SiteEmailCreated;
use App\Listeners\AttachEmailToSite;
use App\Listeners\SendSiteEmailVerification;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Events\Verified;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        CompanyClaimed::class => [
            AttachEmailToSite::class,
        ],
        SiteEmailCreated::class => [
            SendSiteEmailVerification::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
