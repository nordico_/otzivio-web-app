<?php
return [ 
	'trustScore' => 'Резултат на доверие',
	'notRated' => 'Не е оценена',
	'prettyBad' => 'Слаба', // scores less than or equal to 1.49
	'bad' => 'Средна', // scores between >= 1.50 and <= 2.49
	'good' => 'Добра',// scores between >= 2.50 and lower than or equal to <= 3.49
	'veryGood' => 'Много добра',// scores between >= 3.50 and lower than or equal to <= 4.49
	'excellent' => 'Отлична' // higher or equal to >= 4.49
];