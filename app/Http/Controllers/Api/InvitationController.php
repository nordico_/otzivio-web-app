<?php

namespace App\Http\Controllers\Api;

use App\EmailTemplate;
use App\Exports\CustomersErrorExcel;
use App\Imports\CustomersImport;
use App\Invite;
use App\Jobs\SendInvitationEmail;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Mail;
use App\Http\Controllers\Controller;
use App\InvitedCustomer;
use App\Mail\InviteCustomer;
use App\Services\InviteCustomerEmailService;
use App\Sites;
use App\SiteSetting;
use Illuminate\Support\Facades\Validator;

class InvitationController extends Controller
{
    public function import(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'excel' => 'required|file|mimes:csv,txt,xls,xlsx',
        ]);
        if ($validator->fails())
            return response()->json(['success' => false, 'data' => [$validator->errors()], 'status' => 'invalid_file_type', 'message' => 'The given data is not valid!']);

        $path = $request->file('excel');

        $customers = Excel::toCollection(new CustomersImport, $path)[0];

        if (!$customers || count($customers) < 1)
            return response()->json(['success' => false, 'data' => [], 'status' => 'empty_file', 'message' => 'The file does not contain any data!']);

        $hasErrors = false;

        // check for any errors on the data
        foreach ($customers as $customer) {
            $errorMessage = $this->__validateRow($customer);
            if ($errorMessage != "") $hasErrors = true;
            $customer[3] = $errorMessage;
        }

        if ($hasErrors) {
            $filename = str_random() . '.' . $request->file('excel')->getClientOriginalExtension();
            $path = 'excel/errors/' . $filename;
            Excel::store(new CustomersErrorExcel($customers), $path, 'public');
            return response()->json(['success' => false, 'data' => ['file' => $path], 'status' => 'invalid_data', 'message' => 'There were some errors in the excel file!']);
        }

        $invite = null;
        if ($request->has('invite_id')) {
            $invite = Invite::find($request['invite_id']);
        } else {
            $invite = Invite::create([
                'user_id' => auth()->user()->id,
                'site_id' => auth()->user()->company->id,
                'file_name' => $request['file_name'],
                'status' => Invite::PENDING,
            ]);
        }

        // delete previous entries
        $invite->invitedCustomers()->delete();
        // create customers
        foreach ($customers as $index => $customer) {

            $customer = InvitedCustomer::create([
                'email' => $customer[0],
                'name' => $customer[1],
                'reference_number' => $customer[2],
                'invite_id' => $invite->id,
                'status' => InvitedCustomer::NOT_STARTED,
                'site_id' => auth()->user()->company->id,
            ]);

            $customers[$index] = $customer;
        }

        return response()->json(['success' => true, 'data' => ['customers' => $customers, 'invitation' => $invite], 'status' => 'success', 'message' => 'Customers Validated Successfully!']);
    }


    private function __validateRow($row)
    {
        $errorMessage = "";

        if (!$row[0] || $row[0] == "") $errorMessage = $errorMessage . " Email cannot be empty";
        else if (!filter_var($row[0], FILTER_VALIDATE_EMAIL)) $errorMessage = $errorMessage . " Invalid Email";

        if (!$row[1] || $row[1] == "") $errorMessage = $errorMessage . " Name cannot be empty";
        if (strlen($row[1]) < 3) $errorMessage = $errorMessage . " Name is too small";

        if (!$row[2] || $row[2] == "") $errorMessage = $errorMessage . " Reference cannot be empty";

        return $errorMessage;
    }


    public function downloadErrorFile($filename)
    {
        $path = 'public/storage/excel/errors/' . $filename;
        return response()->download($path, $filename, [
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'inline; filename="' . $filename . '"'
        ]);
    }

    public function showInvitePage()
    {
        return view('invite-customers');
    }

    public function cacheTemplate(Request $request)
    {
        session(['preview-template'=>$request->template]);
        return response()->json(['success'=>true],200);
    }

    public function sendInvitations(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sender_email' => 'required|email',
            'sender_name' => 'required|min:3',
            'email_template' => 'required',
            'invite_id' => 'required|exists:invites,id',
            'subject'=>'required',
        ]);
        if ($validator->fails())
            return response()->json(['success' => false, 'data' => [$validator->errors()], 'status' => 'validation_failed', 'message' => 'The given data is not valid!']);

        $invite = Invite::find($request['invite_id']);
        $customers = $invite->invitedCustomers;
        $site = auth()->user()->company()->first();

        $invite->update(['status' => Invite::IN_PROGRESS]);

        foreach ($customers as $index => $customer) {
            $isLast = $index == count($customers) - 1;
            $job = new SendInvitationEmail(
                $customer,
                InviteCustomerEmailService::process($request['email_template'], $request['sender_email'], $customer, $site),
                $invite,
                $isLast,
                $site,
            );
            dispatch($job);
        }

        return response()->json(['success' => true, 'data' => [], 'status' => 'sending_emails', 'message' => 'The emails have been queued for delivery']);
    }

    public function testInvitation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sender_email' => 'required|email',
            'sender_name' => 'required|min:3',
            'email_template' => 'required',
            'subject' => 'required',
        ]);
        if ($validator->fails())
            return response()->json(['success' => false, 'data' => [$validator->errors()], 'status' => 'validation_failed', 'message' => 'The given data is not valid!']);

        $customer = InvitedCustomer::make([
            'email' => $request['sender_email'],
            'name' => $request['sender_name'],
        ]);

        $site = auth()->user()->company()->first();

        Mail::to($customer->email)->send(new InviteCustomer(
            InviteCustomerEmailService::process($request['email_template'], $request['sender_email'], $customer, $site),
            $site,
            $request['subject'],
        ));

        return response()->json(['success' => true, 'data' => [], 'status' => 'email_sent', 'message' => 'The emails have been delivered']);
    }


    public function updateInvitationSettings(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'send_invitation_after' => 'required',
            'send_reminder_after' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'status' => 'invalid_data']);
        }

        $site = auth()->user()->company;
        $settings = $site->siteSettings;


        if($settings){
            $site->siteSettings()->update([
                SiteSetting::SETTING_KEY_SEND_INVITATION_AFTER => $request[SiteSetting::SETTING_KEY_SEND_INVITATION_AFTER],
                SiteSetting::SETTING_KEY_SEND_REMINDER_AFTER => $request[SiteSetting::SETTING_KEY_SEND_REMINDER_AFTER],
            ]);
        }else{
            $site->siteSettings()->create([
                SiteSetting::SETTING_KEY_SEND_INVITATION_AFTER => $request[SiteSetting::SETTING_KEY_SEND_INVITATION_AFTER],
                SiteSetting::SETTING_KEY_SEND_REMINDER_AFTER => $request[SiteSetting::SETTING_KEY_SEND_REMINDER_AFTER],
            ]);
        }

        return response()->json(['success' => true, 'data' => [], 'status' => 'settings_updated', 'message' => 'Site Settings updated successfully!']);
    }
    public function updateEmailTemplate(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'reminder_email_subject' => 'required|min:10',
            'reminder_email_template' => 'required|min:10',
            'invitation_email_subject' => 'required|min:10',
            'invitation_email_template' => 'required|min:10',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'status' => 'invalid_data','messages'=>$validator->errors()],422);
        }

        $site = auth()->user()->company;
        $settings = $site->siteSettings;


        $invitationTemplateId = $request['invitation_email_template_id'];
        if(!$invitationTemplateId){
            $invitationTemplateId = EmailTemplate::updateOrCreate(
                [
                    'created_by'=>$site->id,
                    'type'=>EmailTemplate::EMAIL_TEMPLATE_TYPE_INVITATION,
                ],
                [
                    'value' => $request[SiteSetting::SETTING_KEY_INVITATION_EMAIL_TEMPLATE],
                    'name' => $site->business_name .'-'.EmailTemplate::EMAIL_TEMPLATE_TYPE_INVITATION.'-template',
                ]
            )->id;
        }

        $reminderTemplateId = $request['reminder_email_template_id'];
        if(!$reminderTemplateId){
            $reminderTemplateId = EmailTemplate::updateOrCreate(
                [
                    'created_by'=>$site->id,
                    'type'=>EmailTemplate::EMAIL_TEMPLATE_TYPE_REMINDER,
                ],
                [
                    'value' => $request[SiteSetting::SETTING_KEY_REMINDER_EMAIL_TEMPLATE],
                    'name' => $site->business_name .'-'.EmailTemplate::EMAIL_TEMPLATE_TYPE_REMINDER.'-template',
                ]
            )->id;
        }


        if($settings){
            $site->siteSettings()->update([
                'reminder_email_subject' => $request['reminder_email_subject'],
                'invitation_email_template_id' => $invitationTemplateId,
                'invitation_email_subject' => $request['invitation_email_subject'],
                'reminder_email_template_id' => $reminderTemplateId,
            ]);
        }else{
            $site->siteSettings()->create([
                'reminder_email_subject' => $request['reminder_email_subject'],
                'invitation_email_template_id' => $invitationTemplateId,
                'invitation_email_subject' => $request['invitation_email_subject'],
                'reminder_email_template_id' => $reminderTemplateId,
            ]);
        }

        return response()->json(['success' => true, 'data' => [], 'status' => 'template_updated', 'message' => 'Email template updated successfully!']);
    }
}
