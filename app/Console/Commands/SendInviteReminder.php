<?php

namespace App\Console\Commands;

use App\InvitedCustomer;
use App\Jobs\SendInvitationEmail;
use App\Services\InviteCustomerEmailService;
use App\Sites;
use App\SiteSetting;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendInviteReminder extends Command
{


    protected $signature = 'reminder:send';

    public function handle()
    {
        
        $emailsToBeSent = InvitedCustomer::with('site')->AFS()->deliveredSuccessfully()
                        ->leftJoin('users','invited_customers.email','=','users.email')
                        ->leftJoin('reviews',function($join){
                            $join->on('reviews.user_id', '=', 'users.id')
                                ->on('reviews.review_item_id','=','invited_customers.site_id');
                        })
                        ->whereNull('reviews.user_id')
                        ->get(['invited_customers.id as id','invited_customers.email as email','delivered_at','site_id','invited_customers.created_at as created_at','invited_customers.name as name']);
        
        foreach($emailsToBeSent as $customer){
            $site = $customer->site;

            if($customer->delivered_at && $customer->delivered_at->lt(Carbon::now()->subDays($site->getSettings()[SiteSetting::SETTING_KEY_SEND_REMINDER_AFTER]))){
                $owner = $site->claimer;
                $template = $site->getReminderEmailTemplate();

                $job = new SendInvitationEmail(
                    InvitedCustomer::find($customer->id),
                    InviteCustomerEmailService::process($template,$owner->email , $customer, $site),
                    null,
                    false,
                    $site,
                    true
                );
                dispatch($job);
            }
        }
    }
}
