<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvitedCustomer extends Model
{
    protected $fillable = [
        'email',
        'name',
        'reference_number',
        'invite_id',
        'status',
        'delivered_at',
        'is_afs',
        'site_id',
    ];

    const NOT_STARTED = 'not_started';

    const QUEUED = 'queued_for_delivery';

    const DELIVERED = 'delivered_successfully';

    const FAILED = 'failed_to_deliver';

    const REMINDER_SENT = 'reminder_sent';

    protected $casts=[
        'delivered_at'=>'datetime',
    ];

    public function invite()
    {
        $this->belongsTo(Invite::class);
    }
    public function scopeDeliveredSuccessfully($query)
    {
        return $query->where('status', self::DELIVERED);
    }
    public function scopeNotStarted($query)
    {
        return $query->where('status', self::NOT_STARTED);
    }
    public function scopeAFS($query)
    {
        return $query->where('is_afs', true);
    }
    public function site()
    {
        return $this->belongsTo(Sites::class,'site_id');
    }
}
