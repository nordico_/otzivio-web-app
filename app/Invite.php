<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    protected $fillable = [
        'user_id',
        'site_id',
        'status',
        'file_name',
    ];

    const PENDING = 'pending';

    const IN_PROGRESS = 'in_progress';

    const COMPLETED = 'completed';

    const FAILED = 'falied';

    public function invitedCustomers()
    {
        return $this->hasMany(InvitedCustomer::class);
    }
}
