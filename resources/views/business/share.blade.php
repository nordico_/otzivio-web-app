@extends('layouts.business') 
@section('content')
<div class="for-businesses-page">
	
	<div class="gray-background pt-3 pb-5">
		<div class="container">
			<div class="inner-page__title text-center">
				<h1>{{ __('Sharing shows how you care.') }}</h1>
			</div>
			<div class="text-center">
				<img src="/images/business/share.svg" class="mt-5 mt-md-0" style="max-width: 500px; width: 100%;" />
			</div>
		</div>
	</div>
	<div class="container text-center text-lg-left mt-5 pb-md-3">
		<div class="row">
			<div class="col-12">
				<h3>{{ __("On your websites") }}</h3>
				<p class="h3_subtitle">{{ __("Show off your Credibility") }}</p>
				<p>{{ __("Displaying your reviews in a central position on your website, for instance during checkout, can increase both trust and sales conversions. It's a proven fact.") }}</p>
			</div>
		</div>
	</div>
	<div class="container pb-md-5 mb-5">
		<div class="row d-flex">
			<div class="col-12 text-center text-lg-left col-lg-6 pr-md-5 align-self-center justify-content-sm-center order-2 order-lg-1">
				<h3 class="mt-4">{{ __('Share your trust with a rating box') }}</h3>
				<p>{{ __("Share your best reviews and your best side with the tailored-for-you rating box. Let your customers speak for you by proudly placing your rating box on your website. After all, customers trust other customers: According to a recent Nielsen study, 72% of consumers polled trust online reviews as much as personal recommendations. Consumer reviews say it best.") }}</p>
			</div>
			<div class="col-12 text-center col-lg-6 mt-5 align-self-center order-1 order-lg-2">
				<img src="/images/business/rating_box.png" style="max-width: 550px; width: 100%;"  />
			</div>
		</div>
	</div>
	<div class="gray-background pt-5 pb-5 mt-5">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center col-lg-6 align-self-center">
					<img src="/images/business/reviews_box.svg" style="max-width: 400px; width: 100%;" />
				</div>
				<div class="col-12 text-center text-lg-left col-lg-6 pl-md-5 align-self-center">
					<h3 class="mt-4">{{ __('... or show your latest review with a review box') }}</h3>
					<p>{{ __("Except the rating box, which alows you to show your avarage rating from all reviews, the reviews box gives you the possibility to show the latest reviews to your customers. In this way, your customers can read the reviews directly on your website and in develop more trust in your products or services.") }}</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container text-center text-lg-left mt-5">
		<div class="row">
			<div class="col-12">
				<h3>{{ __("Search Engines") }}</h3>
				<p class="h3_subtitle">{{ __("Find your reviews where it matters, on the search engines") }}</p>
				<p>{{ __("According to GroupM, 93% of all buyers use Search when shopping online. Getting your reviews in the search engines means you generate more traffic and increase your click through rate. Customer reviews are user-generated, keyword-rich content and that's great for improving your presence on search engines.") }}</p>
			</div>
		</div>
	</div>
	<div class="pt-5 pb-5">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center col-lg-6 align-self-center">
					<img src="/images/business/search_engines.svg" style="max-width: 400px; width: 100%;" />
				</div>
				<div class="col-12 text-center text-lg-left col-lg-6 pl-md-5 align-self-center">
					<h3 class="mt-4">{{ __('Look better on Search Engines') }}</h3>
					<p>{{ __("Using Otzivio actively in your search strategy helps you increase the chance that your customer reviews are shown to your future customers.") }}</p>
				</div>
			</div>
		</div>
	</div>
	@component('components/subscribe-to-otzivio-panel') @endcomponent
</div>
@endsection