<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reviews;
use App\Sites;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use App\Services\CompanyReviewService;

class ReviewController extends Controller
{
    private CompanyReviewService $reviewService;

    public function __construct(CompanyReviewService $service)
    {
        $this->reviewService = $service;
    }

    // single item
    public function single(Sites $review)
    {

        // check if business is under review and is not admin
        if (!session()->has('admin') and $review->publish == 'No')
            return view('company-under-review');


        // get reviews for this site
        $reviews = $review->reviews()
            ->with('votes')
            ->withCount('votes')
            ->wherePublish('Yes')
            ->orderByDesc('votes_count')
            ->orderByDesc('id')
            ->paginate(10);

        // get average rating
        $averageRating = @number_format($reviews->avg('rating'), 1) ?? 0.0;

        // set seo title 
        $seo_title =  $review->business_name . ' - ' . $review->url . ' ' .  __('Reviews ');

        SEOMeta::setTitle($review->business_name . ' | ' . trans('meta_title_company_review') . ' ' . $review->url);
        SEOMeta::setDescription(trans('meta_desc_company_review') . ' ' . $review->business_name . '. ' . trans('meta_desc_company_review_1'));
        OpenGraph::setTitle($review->business_name . ' | ' . trans('meta_title_company_review') . ' ' . $review->url);
        OpenGraph::setDescription(trans('meta_desc_company_review') . ' ' . $review->business_name . '. ' . trans('meta_desc_company_review_1'));
        if($review->hasLogo()) OpenGraph::addImage($review->screenshot);

        // get current user auth
        $alreadyReviewed = false;

        if (!auth()->guest() && auth()->user()->id) {

            $alreadyReviewed = Reviews::where('user_id', auth()->user()->id)
                ->where('review_item_id', $review->id)
                ->exists();
        }

        return view('review-single', compact(
            'reviews',
            'review',
            'averageRating',
            'seo_title',
            'alreadyReviewed'
        ));
    }

    // take review
    public function takeReview(Sites $r, Request $request)
    {

        $this->middleware('auth');

        // validate
        $this->validate($request, [
            'rating' => 'required|integer|between:1,5',
            'review_title' => 'required|min:2',
            'review_content' => 'required|min:5'
        ]);


        try {
            $this->reviewService->create($r, $request['rating'], $request['review_title'], $request['review_content'], auth()->user()->id);
            return back();
        } catch (\Exception $e) {
            return back()->with('message', $e->getMessage());
        }
    }

    // take reply as company
    public function replyAsCompany(Reviews $r, Request $req)
    {

        if (!$req->has('replyTo_' . $r->id)) {
            return back()->with('message', __('Please enter the reply content.'));
        }

        // get review item 
        $reviewItem = $r->site->claimedBy;

        if (auth()->user()->id != $reviewItem) {
            return back()->with('message', __('You do not seem to own this review item id.'));
        }

        // save reply
        $r->company_reply = $req->{"replyTo_" . $r->id};
        $r->save();

        return back()->with('message', __('Your reply was saved.'));
    }
}
