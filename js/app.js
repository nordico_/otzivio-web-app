$( function() {


	$('form').on('submit',function(){
		$('button[name="sbReview"]').prop('disabled',true)
	})
	
	// stripe form
	var $form = $('#payment-form');

	$form.submit(function(event) {
	    // Disable the submit button to prevent repeated clicks:
	    $form.find('.submit').prop('disabled', true);

	    // Request a token from Stripe:
	    Stripe.createToken({
	        number: $('.card-number').val(),
	        cvc: $('.card-cvc').val(),
	        exp_month: $('.card-expiry-month').val(),
	        exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);

	    // Prevent the form from being submitted..:
	    return false;
	});

	function stripeResponseHandler(status, response) {
	  var $form = $('#payment-form');

	  if (response.error) {
	    // Show the errors on the form
	    //$form.find('.payment-errors').text(response.error.message);
	    $form.find('.submit').prop('disabled', false);
		
	    sweetAlert("Oops...", response.error.message, "error");

	  } else {

	    // response contains id and card, which contains additional card details
	    var token = response.id;

	    // append values we need!
	    $form.append($('<input type="hidden" name="stripeToken" />').val(token));
	    $form.append($('<input type="hidden" name="last4" />').val(response.card.last4));
	    $form.append($('<input type="hidden" name="expDate" />').val(response.card.exp_month + '/' + response.card.exp_year));

	    // and submit
	    $form.get(0).submit();
	  }
	}

	// Slick Slider
	$('.latest-reviews__slider').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		autoplay: true,
  		autoplaySpeed: 4000,
  		arrows: false,
  		responsive: [
			{
				breakpoint: 1920,
				settings: {
				  slidesToShow: 4
				}
			  },
		    {
		      breakpoint: 1300,
		      settings: {
		        slidesToShow: 3
		      }
		    },
		    {
		      breakpoint: 960,
		      settings: {
		        slidesToShow: 2
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1
		      }
		    }
		  ]
	});
	
});