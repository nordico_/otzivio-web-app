<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    protected $fillable = ['type','name','created_by','value'];

    const EMAIL_TEMPLATE_TYPE_REMINDER = 'reminder';
    const EMAIL_TEMPLATE_TYPE_INVITATION = 'invitation';

    public const DEFAULT_EMAIL_TEMPLATE = "
        <div style='color: #505050; font-size: 14px;padding:20px; line-height: 150%; text-align: left;'>
            <strong>Здравейте [Name]! </strong><br><br>Благодарим Ви, че избрахте [CompanyIdentifier].
            <div class='stars' style='color: #505050;font-size: 14px; line-height: 150%; text-align: left; width: 100%; margin-top: 2em;'>
                <h3 style='color: #202020; display: block; font-family: Arial; font-size: 26px; font-weight: bold; line-height: 100%; margin-top: 0; margin-right: 0; margin-bottom: 10px; margin-left: 0; text-align: left;'><a class='tp-link' data-cke-saved-href='[Link]' href='[Link]' style='color: #0c59f2; font-weight: normal; line-height: 1em; text-decoration: underline; font-size: 18px;'>Как се справихме? </a></h3>
                [Stars]
            </div>
            <br>Вашето преживяване е важно за нас и Вашият отзив (добър, слаб или друг) ще бъде директно публикуван в <a href=\"https://otziv.io\" target=\"_blank\">Otziv.io</a>, за да помогне на други хора да направят по информирани решения преди да закупят нещо.<br><br><strong>Благодарим Ви за отделеното време,<br>[CompanyIdentifier]</strong><br><br><strong>Моля имайте в предвид</strong>, че този имейл за отзив е изпратен автоматично, което означава, че може да сте го получили преди да сте получили продуктите или услугите, които сте закупили. В този случай, моля изчакайте да получите продуктите или услугите, преди да напишете отзив. &nbsp;<br><br><div id='legal-notice'>[LegalNotice]</div>
        </div>
        ";

    public function scopeReminderType($query){
        return $query->where('type',self::EMAIL_TEMPLATE_TYPE_REMINDER);
    }
    
    public function scopeInvitationType($query){
        return $query->where('type',self::EMAIL_TEMPLATE_TYPE_INVITATION);
    }

    public function scopeByAdmin($query){
        return $query->whereNull('created_by');
    }
}
