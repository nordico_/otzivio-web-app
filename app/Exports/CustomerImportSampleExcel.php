<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromArray;

class CustomerImportSampleExcel implements FromArray
{
    public function array(): array
    {
        $faker = \Faker\Factory::create();

        $data = [];
        for ($i = 0; $i < 10; $i++) {
            $name = $faker->firstName . ' ' . $faker->lastName;
            $data[] = [
                strtolower(str_replace(' ', '', $name)) . random_int(0, 100) . '@gmail.com',
                $name,
                random_int(0, 100000),
            ];
        }
        return $data;
    }
}
