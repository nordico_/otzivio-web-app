<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>Embedded Reviews for {{ $c->url }}</title>
	
	 <link href="https://fonts.googleapis.com/css?family=Lato|Patua+One&display=swap" rel="stylesheet">
	  <link rel='stylesheet' href='https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css'>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
  	<script src='https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js'></script>
	<script>
	$(document).ready(function () {
		$('.testiSlide').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			infinite: true,
			autoplaySpeed: 1500,
			responsive: [
				{
					breakpoint: 850,
					settings: {
						slidesToShow: 2
					}
				},
				{
					breakpoint: 650,
					settings: {
						slidesToShow: 1
					}
				}
			]
		});
	});
	</script>

	<style>
	html {
		font-family: 'Helvetica', 'Arial', sans-serif;
		font-size: 16px;
		font-weight: 400;
		line-height: 1.5;
		-webkit-text-size-adjust: 100%;
		-ms-text-size-adjust: 100%;
		background: {{ Options::get_option('generalBG_' . $c->id , '#FFFFFF' ) }};
		color: {{ Options::get_option('generalFC_' . $c->id , '#FFFFFF' ) }};
	}
	body {
		margin:  0;
		background: {{ Options::get_option('generalBG_' . $c->id , '#FFFFFF' ) }};
		height: 100vh;
	}
	h3 {
		font-weight: 800;
		font-size: 13px;
		line-height: 1.4em;
		margin-top: 10px;
		color: {{ Options::get_option('generalFC_' . $c->id , '#AAAAAA' ) }};
	}
	.container {
		box-sizing: content-box;
		margin-left: auto;
		margin-right: auto;
		padding: 40px;
	}
	.indentity {
		margin: 0!important
	}
	.testiSlide {
		width: 100%;
		display: inline-block;
	}
	figure.testimonial {
		position: relative;
		float: left;
		overflow: hidden;
		margin: 10px 0;
		padding: 0 15px;
		text-align: left;
		box-shadow: none !important;
		color: {{ Options::get_option('testiFG_' . $c->id , '#FFFFFF' ) }};
	}
	figure.testimonial * {
		-webkit-box-sizing: border-box;
		box-sizing: border-box;
		-webkit-transition: all 0.35s cubic-bezier(0.25, 0.5, 0.5, 0.9);
		transition: all 0.35s cubic-bezier(0.25, 0.5, 0.5, 0.9);
	}
	figure.testimonial img {
		max-width: 100%;
		vertical-align: middle;
		height: 90px;
		width: 90px;
		border-radius: 50%;
		margin: 40px 0 0 10px;
	}
	figure.testimonial .rating-stars {
		margin-bottom: 10px;
		display: block;
		width: 125px;
	}
	figure.testimonial .rating-stars .rating-star {
		width: 25px;
		height: 25px;
		height: auto;
		margin: 0px;
		display: inline-block;
		border-radius: 0px;
	}
	figure.testimonial .rating-stars .rating-star svg {
		width: auto;
		height: 25px;
		vertical-align: top;
	}
	.rating-stars__wrapper {
	position: relative;
	}

	.rating-stars__no-rating {
	position: absolute;
	}

	.rating-stars__has-rating {
	position: relative;
	z-index: 2;
	}

	.rating-star svg {
	width: auto;
	height: 100%;
	vertical-align: top;
	}

	@if(Options::get_option('urlFC_' . $c->id) == 'rgba(255,255,255,0)' || Options::get_option('urlFC_' . $c->id) == '' || Options::get_option('urlFC_' . $c->id) == null )
		.rating-star__poor svg path {
		fill: #BF2E1D;
		}

		.rating-star__average svg path {
		fill: #FF8622;
		}

		.rating-star__good svg path {
		fill: #FFCE00;
		}

		.rating-star__very-good svg path {
		fill: #73CF12;
		}

		.rating-star__excellent svg path {
		fill: #18806F;
		}
	@else
		.rating-stars__has-rating .rating-star svg path {
			fill: {{ Options::get_option('urlFC_' . $c->id , 'rgba(255,255,255,0)' ) }};
		}
	@endif

	figure.testimonial blockquote {
		background-color: {{ Options::get_option('testiGB_' . $c->id , '#ffffff' ) }};
		display: block;
		font-size: 14px;
		font-weight: 400;
		line-height: 1.5em;
		margin: 0;
		position: relative;
		color: {{ Options::get_option('testiFC_' . $c->id , '#333' ) }};
	}
	/*figure.testimonial blockquote:before, figure.testimonial blockquote:after {
		content: "\201C";
		position: absolute;
		color: {{ Options::get_option('testiFC_' . $c->id , '#333' ) }};
		font-size: 50px;
		font-style: normal;
	}*/
	figure.testimonial blockquote:before {
		top: 25px;
		left: 20px;
	}
	figure.testimonial blockquote:after {
		right: 20px;
		bottom: 0;
	}
	figure.testimonial .btn {
		top: 100%;
		width: 0;
		height: 0;
		border-left: 0 solid transparent;
		border-right: 25px solid transparent;
		border-top: 25px solid #fff;
		margin: 0;
		position: absolute;
	}
	figure.testimonial .peopl {
		margin: 0;
		color: {{ Options::get_option('generalFC_' . $c->id , '#333' ) }};
	}

	.slick-slider {
		position: relative;
		display: block;
		box-sizing: border-box;
		user-select: none;
		-webkit-touch-callout: none;
		-khtml-user-select: none;
		-ms-touch-action: pan-y;
		touch-action: pan-y;
		-webkit-tap-highlight-color: transparent;
	}
	.slick-list {
		position: relative;
		display: block;
		overflow: hidden;
		margin: 0;
		padding: 0;
	}
	.slick-list:focus {
		outline: none;
	}
	.slick-list.dragging {
		cursor: pointer;
		cursor: hand;
	}
	.slick-slider .slick-track, .slick-slider .slick-list {
		transform: translate3d(0, 0, 0);
	}
	.slick-track {
		position: relative;
		top: 0;
		left: 0;
		display: block;
	}
	.slick-track:before, .slick-track:after {
		display: table;
		content: '';
	}
	.slick-track:after {
		clear: both;
	}
	.slick-loading .slick-track {
		visibility: hidden;
	}
	.slick-slide {
		display: none;
		float: left;
		height: 100%;
		min-height: 1px;
	}
	.slick-slide img {
		display: block;
	}
	.slick-slide.slick-loading img {
		display: none;
	}
	.slick-slide.dragging img {
		pointer-events: none;
	}
	.slick-initialized .slick-slide {
		display: block;
	}
	.slick-loading .slick-slide {
		visibility: hidden;
	}
	.slick-vertical .slick-slide {
		display: block;
		height: auto;
		border: 1px solid transparent;
	}
	.slick-btn.slick-hidden {
		display: none;
	}

	.slick-prev, .slick-next {
		font-size: 0;
		line-height: 0;
		position: absolute;
		top: 50%;
		display: block;
		width: 20px;
		height: 20px;
		padding: 0;
		transform: translate(0, -50%);
		cursor: pointer;
		color: {{ Options::get_option('generalFC_' . $c->id , '#FFFFFF' ) }};
		border: none;
		outline: none;
		background: transparent;
	}
	.slick-prev:hover, .slick-prev:focus, .slick-next:hover, .slick-next:focus {
		color: transparent;
		outline: none;
		background: transparent;
	}
	.slick-prev:hover:before, .slick-prev:focus:before, .slick-next:hover:before, .slick-next:focus:before {
		opacity: 1;
	}
	.slick-prev:before, .slick-next:before {
		font-family: "FontAwesome";
		font-size: 16px;
		line-height: 1;
		opacity: .75;
		color: white;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
	}
	.slick-prev {
		left: -25px;
	}
	.slick-prev:before {
		content: "\f053";
	}
	.slick-next {
		right: -25px;
	}
	.slick-next:before {
		content: "\f054";
	}
	.full-review-link {
		color: {{ Options::get_option('urlFC_' . $c->id , '#FFFFFF' ) }};
	}
	.slick-prev:before,
	.slick-next:before {
	  color: {{ Options::get_option('generalFC_' . $c->id , '#AAAAAA' ) }};
	}
	.stars {
		color: #ffc107;
	}
	.our-reviews-info {
		color: #888;
		padding: 0 15px;
		margin: 10px 0;
		color: {{ Options::get_option('generalFC_' . $c->id , '#AAAAAA' ) }};
	}
	@media (min-width: 561px){
		.our-reviews-info {
			margin: 20px 0;
		}
	}
	.our-reviews-info a {
		color: {{ Options::get_option('generalFC_' . $c->id , '#AAAAAA' ) }};
		line-height: 1.3em;
		text-decoration: none;

		display: -webkit-box;
		display: -webkit-flex;
		display: -moz-box;
		display: -ms-flexbox;
		display: flex;

		-webkit-box-pack: justify;
		-webkit-justify-content: space-between;
		-moz-box-pack: justify;
		-ms-flex-pack: justify;
		justify-content: space-between;

		-webkit-box-orient: vertical;
		-webkit-box-direction: normal;
		-webkit-flex-direction: column;
		-moz-box-orient: vertical;
		-moz-box-direction: normal;
		-ms-flex-direction: column;
		flex-direction: column;
	}
	@media (min-width: 561px){
		.our-reviews-info a {
			-webkit-box-align: center;
			-webkit-align-items: center;
			-moz-box-align: center;
			-ms-flex-align: center;
			align-items: center;
			
			-webkit-box-orient: horizontal;
			-webkit-box-direction: normal;
			-webkit-flex-direction: row;
			-moz-box-orient: horizontal;
			-moz-box-direction: normal;
			-ms-flex-direction: row;
			flex-direction: row;
		}
	}
	.our-reviews-info * {
		margin: 0px;
		font-size: 13px;
	}
	.our-reviews-info h5 {
		font-size: 15px;
		position: relative;
		top: -1px;
	}
	.our-reviews-info svg {
		width: 100px;
		height: 24px;
	}
	.our-reviews-info svg path {
		fill: {{ Options::get_option('generalFC_' . $c->id , '#AAAAAA' ) }};
	}
	.our-reviews-info__rating {
		display: -webkit-box;
		display: -webkit-flex;
		display: -moz-box;
		display: -ms-flexbox;
		display: flex;

		-webkit-box-pack: justify;
		-webkit-justify-content: space-between;
		-moz-box-pack: justify;
		-ms-flex-pack: justify;
		justify-content: space-between;

		-webkit-box-orient: vertical;
		-webkit-box-direction: normal;
		-webkit-flex-direction: column;
		-moz-box-orient: vertical;
		-moz-box-direction: normal;
		-ms-flex-direction: column;
		flex-direction: column;
		margin-bottom: 5px;
	}
	.dot {
		display: none;
	}
	@media (min-width: 561px){
		.dot {
			display: block;
		}
		.our-reviews-info__rating {
			-webkit-box-align: center;
			-webkit-align-items: center;
			-moz-box-align: center;
			-ms-flex-align: center;
			align-items: center;

			-webkit-box-orient: horizontal;
			-webkit-box-direction: normal;
			-webkit-flex-direction: row;
			-moz-box-orient: horizontal;
			-moz-box-direction: normal;
			-ms-flex-direction: row;
			flex-direction: row;

			margin-bottom: 0px;
		}
	}
	</style>
</head>
<body>
	
<div class="container">
<div class="testiSlide">

@php($otzivioLogo = '<svg width="758" height="185" viewBox="0 0 758 185" fill="none" xmlns="http://www.w3.org/2000/svg">
	<path fill-rule="evenodd" clip-rule="evenodd" d="M112.803 5.65605C102.423 -1.88535 88.3675 -1.88535 77.9876 5.65605L12.2099 53.4463C1.83011 60.9877 -2.51324 74.3552 1.4515 86.5574L26.5763 163.884C30.5411 176.086 41.9121 184.348 54.7423 184.348H136.048C148.878 184.348 160.249 176.086 164.214 163.884L189.339 86.5574C193.304 74.3552 188.96 60.9877 178.58 53.4464L112.803 5.65605ZM99.4187 56.1618C98.1522 52.2639 92.6378 52.2639 91.3713 56.1618L83.9966 78.8588C83.4302 80.602 81.8057 81.7822 79.9729 81.7822H56.1078C52.0093 81.7822 50.3053 87.0268 53.621 89.4358L72.9283 103.463C74.4111 104.541 75.0316 106.45 74.4652 108.194L67.0905 130.891C65.824 134.788 70.2852 138.03 73.601 135.621L92.9082 121.593C94.3911 120.516 96.399 120.516 97.8818 121.593L117.189 135.621C120.505 138.03 124.966 134.788 123.7 130.891L116.325 108.193C115.758 106.45 116.379 104.541 117.862 103.463L137.169 89.4358C140.485 87.0268 138.781 81.7822 134.682 81.7822H110.817C108.984 81.7822 107.36 80.602 106.793 78.8588L99.4187 56.1618Z" fill="black"/>
	<path d="M285.466 129.917C302.347 129.917 317.07 117.478 317.07 98.6936C317.07 79.9089 302.347 67.5974 285.466 67.5974C268.585 67.5974 253.989 79.9089 253.989 98.6936C253.989 117.478 268.585 129.917 285.466 129.917ZM285.466 116.209C276.328 116.209 268.839 109.101 268.839 98.6936C268.839 88.5397 276.328 81.3051 285.466 81.3051C294.605 81.3051 302.22 88.5397 302.22 98.6936C302.22 109.101 294.605 116.209 285.466 116.209Z" fill="black"/>
	<path d="M330.035 69.1205V82.8282H348.947V128.394H363.543V82.8282H382.455V69.1205H330.035Z" fill="black"/>
	<path d="M433.101 97.4244C437.543 94.759 440.336 89.809 440.336 84.859C440.336 75.2128 432.339 67.5974 417.616 67.5974C410.762 67.5974 400.609 69.3743 394.77 72.0397L398.959 83.7166C403.147 81.8128 409.747 80.6705 414.443 80.6705C420.155 80.6705 425.105 82.1936 425.105 86.7628C425.105 90.4436 421.932 93.6167 414.697 93.6167H404.797V103.263H416.728C424.089 103.263 428.659 105.421 428.659 109.736C428.659 114.813 422.059 117.097 414.697 117.097C409.62 117.097 402.639 115.701 397.689 113.417L394.135 125.601C398.959 128.14 408.859 129.917 416.601 129.917C432.974 129.917 443.889 123.571 443.889 111.767C443.889 104.786 439.193 99.5821 433.101 97.4244Z" fill="black"/>
	<path d="M477.265 106.055V69.1205H462.542V128.394H474.346L505.569 90.9513V128.394H520.165V69.1205H507.981L477.265 106.055Z" fill="black"/>
	<path d="M587.663 96.7897C594.517 94.759 595.659 88.159 595.659 84.2243C595.659 75.8474 588.932 69.1205 577.255 69.1205H542.732V128.394H578.524C593.628 128.394 598.959 119.382 598.959 111.259C598.959 104.405 594.771 98.1859 587.663 96.7897ZM581.317 87.3974C581.317 90.5705 579.286 92.7282 575.732 92.7282H557.455V81.9397H575.605C579.413 81.9397 581.317 84.2243 581.317 87.3974ZM557.455 115.321V103.136H576.874C581.951 103.136 584.363 106.055 584.363 109.355C584.363 112.655 581.951 115.321 576.874 115.321H557.455Z" fill="black"/>
	<path d="M632.687 106.055V69.1205H617.964V128.394H629.768L660.991 90.9513V128.394H675.587V69.1205H663.402L632.687 106.055Z" fill="black"/>
	<path d="M725.95 129.917C742.831 129.917 757.554 117.478 757.554 98.6936C757.554 79.9089 742.831 67.5974 725.95 67.5974C709.069 67.5974 694.473 79.9089 694.473 98.6936C694.473 117.478 709.069 129.917 725.95 129.917ZM725.95 116.209C716.811 116.209 709.323 109.101 709.323 98.6936C709.323 88.5397 716.811 81.3051 725.95 81.3051C735.088 81.3051 742.704 88.5397 742.704 98.6936C742.704 109.101 735.088 116.209 725.95 116.209Z" fill="black"/>
	</svg>
	')

@forelse( $reviews as $r )
	@if(number_format($r->rating,2) >= 4)
		<div>
			<figure class="testimonial"> 
			<blockquote>
				<div class="rating-stars">
					@include('partials/star', ['r' => $r->rating])
				</div>
				<!--{{ number_format($r->rating,2)  }}/5.00 <br>-->
				<strong>{{ substr($r->review_title, 0, 50) }}...</strong><br>
				{{ substr( $r->review_content, 0, 115 )}}...
			</blockquote>
			<div class="peopl">
				<h3>{{ $r->user->name }}</h3>
			</div>
			<!--<p>
			<a href="{{ $c->slug }}" target="_blank" class="full-review-link">{{ __('Read full review') }}</a>
			</p>--><!-- /.btn -->
			</figure>
		</div>
	@endif
@empty
	{{ __('No reviews for this company yet') }}
@endforelse

</div><!--./testiSlide -->

@if( $reviews->count() )
	<div class="our-reviews-info">
		<a href="{{ $c->slug }}" target="_blank">
			<div class="our-reviews-info__rating">
				<h5>{{ $c->trustScore }}</h5><!-- --><span class="dot">&nbsp;&nbsp;·&nbsp;&nbsp;</span><!-- --><p><strong>{{ $c->reviews->avg('rating') }}</strong> базирано на <strong>{{ $reviews->count() }}</strong> <span>отзива</span></p>
			</div>
			{!! $otzivioLogo !!}
		</a>
	</div>
@endif

</div><!-- /.container -->
</body>
</html>