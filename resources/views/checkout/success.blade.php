@extends('base') 

@section('content')
<div class="success-page">
	<div class="container card text-center">
		<div class="success-page__wrapper">
			<div class="row">
				<div class="col-12">
					<h1 class="text-center"><i class="far fa-check-circle"></i> {{ __('Thank you') }}!</h1>
					<div class="separator-3"></div>
				</div>
			</div>

			@if (auth()->user()->company->isOnPaidSubscription())
			{{-- message for paid claim --}}
			{{ __('Your company ownership should appear in your account') }} <span class="badge badge-warning">{{ __('maximum 2 hours') }}</span> {{ __('from now') }}.
			@else
			{{-- message for free claim --}}
			{{ __('Your company ownership should appear in your account') }}.
			@endif
			
			<br><br>

			<a href="{{ route('myaccount') }}" class="btn btn-success btn-md">Прегледайте вашият акаунт</a>
		</div>
	</div>
</div>
@endsection