@extends( 'base' )


@section('extraCSS')
<style>
.panel-title>a, .panel-title>a:active{
	text-decoration:none;
}
.panel-heading  a:before {
	font-family: "Font Awesome 5 Free"; font-weight: 900; content: "\f078";
   float: right;
   transition: all 0.5s;
}

.panel-heading.active a:before {
	-webkit-transform: rotate(180deg);
	-moz-transform: rotate(180deg);
	transform: rotate(180deg);
} 

/* show less pagination on small devices */
@media screen and ( max-width: 720px ){
	li.page-item {

		display: none;
	}

	.page-item:first-child,
	.page-item:last-child,
	.page-item.active {

		display: block;
	}
}


.small-device-filters{
	display: none;
}

@media (min-width: 576px) { 
	.small-device-filters{
		display: none;
	}
 }
@media (max-width: 576px) { 
	.large-device-filters{
		display: none;
	}
	.small-device-filters{
		display: block;
	}
}
	



</style>	
@endsection

@section( 'content' )

	<div class="container companies-list-page">
		<div class="row">
		<div class="col-md-4 col-xs-12">
			<div class="card">

				<div class="panel-group small-device-filters" id="accordion" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default">
					  <div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
						  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							<h5>{{ __('Filters') }}</h5>
						  </a>
						</h4>
					  </div>
					  <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
							@include('components.category-filters')
						</div>
					  </div>
					</div>
				</div>

				<div class="large-device-filters">
						<h5>{{ __('Filters') }}</h5>
						@include('components.category-filters')
				</div>
			</div>
			<br>
		</div><!-- /.col-md-4 col-xs-12 -->
		

		<div class="col-md-8 col-xs-12">
		
			 <h1 class="h2">{{ __( 'Компании в категория ' ) . ' ' . $category->name }}</h1>
			 
			 <div class="col-xs-12">
			 	{{ Options::get_option( 'catAd' ) }}
			 </div>

		
		<br>

		@forelse( $sites as $site )
		<div class="card companies-list-page__company-card">
			<a href="{{ route( 'reviewsForSite', [ 'site' => $site ] ) }}">
				<div>
					<h5>{{ $site->business_name }}</h5>
					<div class="row">
						<div class="col d-flex flex-column flex-md-row">
							<div class="companies-list-page__company-rating">
								@include('partials/star', ['r' => !$site->rating ? 0.0 : number_format($site->rating,1)])
							</div>
							<div class="mt-2 mt-md-0">
								<span class="pl-md-3">
									@if($site->reviewsCount == 1)
										{{ $site->reviewsCount }} {{ __('review') }}
									@else
										{{ $site->reviewsCount }} {{ __('reviews') }}
									@endif
								</span>
								<span class="pl-3">
									{{ __('Rating') }}: {{ !$site->rating ? "0.0" : number_format($site->rating,1)  }}
								</span>
							</div>
						</div>
					</div>
					<div class="row mt-3">
						<div class="col">
							<h6 class="companies-list-page__company-location">{{ $site->location }}</h6>
						</div>
					</div>
					<hr>
					<div class="row">
					@forelse( $site->reviews()->take(2)->orderBy('id','DESC')->get() as $r )
						<div class="col-6 companies-list-page__company-review">
							<h6>{{ $r->review_title }}</h6>
							<p>{{ substr( $r->review_content, 0, 70 )}}....</p>
						</div>
					@empty
						<h6 class="text-muted">&nbsp;&nbsp;&nbsp; {{ __( 'Все още няма отзиви' )}}</h6>
					@endforelse	
					</div>
				</div>
			</a>
		</div><!-- ./card -->
		<br>
		@empty
	        <div class="text-center">
		        <p>
		            {{ __('No companies added in this category yet') }}
		        </p>
		        <a href="https://otziv.io/submit-company" class="btn btn-success">{{ __('Add Company') }}</a>
	        </div>
	    @endforelse

		{{ $sites->appends([ 'sortBy' => request('sortBy'), 
		                     'reviewsCount' => request('reviewsCount'), 
		                     'lati' => request('lati'), 
		                     'longi' => request('longi'), 
		                     'location' => request('location'), 
							 'companyStatus' => request( 'companyStatus' ) ])
							 ->links() }}

		</div>		
		</div>
	</div><!-- /.container card -->
   
@endsection

@section( 'extraJS' )


<script>
$('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
  });

  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
  });
</script>

 <script src="https://maps.google.com/maps/api/js?libraries=places&key={{ Options::get_option('mapsApiKey') }}"></script>
  <script>

  // Address autocomplete
    var placeSearch, autocomplete;
    var componentForm = {
      street_number: 'short_name',
      route: 'long_name',
      locality: 'long_name',
      administrative_area_level_1: 'short_name',
      country: 'long_name',
      postal_code: 'short_name'
    };

    function initialize() {
      // Create the autocomplete object, restricting the search
      // to geographical location types.
      autocomplete = new google.maps.places.Autocomplete(
          /** @type {HTMLInputElement} */(document.getElementById('city_region')),
          { types: ['geocode'] });
      // When the user selects an address from the dropdown,
      // populate the address fields in the form.
      google.maps.event.addListener(autocomplete, 'place_changed', function() {
        fillInAddress();
      });
    }

    // [START region_fillform]
    function fillInAddress() {
      // Get the place details from the autocomplete object.
      var place = autocomplete.getPlace();

      console.log( place.address_components );


      // get latitute and longitude
      var lati = place.geometry.location.lat();
      var longi = place.geometry.location.lng();

      document.getElementById('lati').value = lati;
      document.getElementById('longi').value = longi;

      // get city and state
      var ac = place.address_components;
      var city = ac[ 1 ].long_name;
      var state = ac[ 3 ].long_name;

      document.getElementById('city').value = city;
      document.getElementById('state').value = state;

      // console.log( "City: " + city + ", State: " + state );

      for (var component in componentForm) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
      }

      // Get each component of the address from the place details
      // and fill the corresponding field on the form.
      for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
          var val = place.address_components[i][componentForm[addressType]];
          document.getElementById(addressType).value = val;
          console.log( addressType + "=" + val );
        }
      }
    }
    // [END region_fillform]

    // [START region_geolocation]
    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var geolocation = new google.maps.LatLng(
              position.coords.latitude, position.coords.longitude);
          var circle = new google.maps.Circle({
            center: geolocation,
            radius: position.coords.accuracy
          });
          autocomplete.setBounds(circle.getBounds());
        });
      }
    }
    // [END region_geolocation]

    $( document ).ready( function() {
    	initialize();
    });
    </script>

@endsection
