<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitedCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invited_customers', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->string('name');
            $table->string('reference_number');
            $table->unsignedBigInteger('invite_id')->nullable();
            $table->foreign('invite_id')->references('id')->on('invites')->onDelete('cascade');
            $table->string('status')->nullable();
            $table->dateTime('delivered_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invited_customers');
    }
}
