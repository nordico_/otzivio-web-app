<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metadata extends Model
{
    protected $fillable=['metaable_type','metaable_type_id','value','type'];

    const METADATA_TYPE_SEARCH = 'search';
    const METADATA_TYPE_SEO = 'seo';

    public function metaable()
    {
        return $this->morphTo();
    }
}
