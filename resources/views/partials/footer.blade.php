<footer class="site-footer">
  <div class="container">
    <img src="/images/logo.svg" alt="Otzivio logo"/> 
    <div class="site-footer__menu">
      <div class="row">
        <div class="col col-12 col-md-3">
          <h4>{{ __('Otzivio') }}</h4>
          <ul>
            <li><a href="{{ route('contact') }}">{{ __('Get In Touch') }}</a></li>
            <li><a href="{{url('p-tos')}}">{{ __('Terms and Conditions') }}</a></li>
            <li><a href="{{url('p-privacy-policy')}}">{{__('Privacy Policy')}}</a></li>
          </ul>
          <!--@foreach( App\Page::all() as $page )
          <a href="/p-{{ $page->page_slug }}">{{ $page->page_title }}</a> | 
          @endforeach
          <a href="{{ route('contact') }}">{{ __('Get In Touch') }}</a> | 
          <a href="{{ route('sitemap') }}">{{ __('Sitemap') }}</a>-->
        </div>
        <div class="col col-12 col-md-3">
          <h4>{{ __('Community') }}</h4>
          <ul>
            @guest
              <li><a href="{{route('login')}}">{{ __('Login to your profile') }}</a></li>
            @else
              <li><a href="{{route('myaccount')}}">{{ __('My Account') }}</a></li>
            @endguest
            <li><a href="{{url('p-polezni-sveti-predi-da-napisete-otziv')}}">{{ __('Helpful hints before writing a review') }}</a></li>
          </ul>
        </div>
        <div class="col col-12 col-md-3">
          <h4>{{ __('Business') }}</h4>
          <ul>
            <li><a href="{{route('forBusinesses')}}">{{ __('Otzivio for your business') }}</a></li>
            <li><a href="{{route('businessPlans')}}">{{ __('Pricing') }}</a></li>
          </ul>
        </div>
        <div class="col col-12 col-md-3">
          <h4>{{ __('Follow us') }}</h4>
          <ul>
            <li><a href="https://www.facebook.com/otzivio" target="_blank"><i class="fab fa-facebook-square"></i> {{ __('Facebook') }}</a></li>
          </ul>

          <div class="site-footer__select-language">
            <h5>{{__('Select a language')}}</h5>
            <select class="form-select" id="language-switcher">
              <option disabled>{{__('Select a language')}}</option>
              <option value="en" {{App::getLocale() == 'en' ? 'selected' : ''}}>English</option>
              <option value="bg" {{App::getLocale() == 'bg' ? 'selected' : ''}}>Български</option>
            </select>
            <a href="{{$en_url}}" style="display: none"></a>
            <a href="{{str_replace("en","bg",$en_url)}}" style="display: none"></a>
          </div>
        </div>
      </div><!-- /.row -->
      <a style="opacity: 0;height:0" href='http://pagepeeker.com/website-thumbnails-api/' target='_blank'>Pagepeeker</a>
      <div class="row site-footer__copyright">
        <p>© {{ date('Y') }} {{ __('Otzivio') }}. {{ __('All rights reserved') }}</p>
      </div>
    </div>
  </div>
</footer>