@extends( 'base' )

@section( 'content' )

<div class="container card">
	<h4>{{ __( 'Изпратихме ви имейл до: ' ) }}<em>{{ $sendToEmail }}</em></h4>
	{{ __( 'Моля, проверете имейла си за линк с потвърждение на собствеността.' ) }}
	{{ __( 'Please check you junk or spam folder in case you don\'t receive an email') }}
</div><!-- /.container -->

@endsection