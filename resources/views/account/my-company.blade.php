@extends( 'base' )

@section( 'content' )

<div class="container">

	
	<div class="row">
		<div class="col-12 mx-auto">
			<ul class="nav nav-tabs">
				<li class="nav-item">
					<a class="nav-link" href="{{ route( 'myaccount' ) }}">{{ __('My Reviews') }}</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="{{ route( 'myprofile' ) }}">{{ __('My Profile') }}</a>
				</li>

				<li class="nav-item">
					<a class="nav-link active" href="{{ route( 'mycompany' ) }}">{{ __('My Company') }}</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="{{ route( 'mybilling' ) }}">{{ __('My Billing') }}</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="{{ route( 'logout' ) }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">{{ __('Log Out') }}
					</a>
				</li>
			</ul>
		</div><!-- /.col-10 card -->

		
		{{-- if companies claimed --}}
		@if ($company)
		<div class="col-12 mx-auto">
			<div class="card">
				<h2>{{ __('My Company') }} 
					@if(!$company->isOnGracePeriod() && !$company->isOnPaidSubscription())
					<a class="btn btn-sm btn-success" href="{{route('switchToPaidMembership')}}">
						{{__('company_profile_page_upgrade_to_paid_plan')}}
					</a>
					@endif
				</h2>
				<hr>

				<table class="table-responsive-sm table table-borderless">
					<thead>
						<tr>
							<th>{{ __('URL') }}</th>
							<th>{{ __('Name') }}</th>
							<th>{{ __('Location') }}</th>
							<th>{{ __('Manage') }}</th>
							<th>{{ __('Embed Reviews') }}</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<a href="{{ route('reviewsForSite', ['site'=> $company->url]) }}">
									{{ $company->url }}
								</a>
							</td>
							<td><strong>{{ $company->business_name }}</strong></td>
							<td>{{ $company->location }}</td>
							<td>
								<a href="{{ route('manageCompany') }}"
									class="btn btn-sm btn-primary">{{ __('Manage Profile') }}</a><br>
							</td>
							<td>
								<a href="{{ route('myEmbeddedCodes') }}"
									class="btn btn-sm btn-warning">{{ __('Embedded Codes') }}</a>
							</td>
						</tr>
					</tbody>
				</table>

			</div><!-- /.card -->
		</div><!-- /.col-10 -->

		<div class="col-12 mx-auto">
			<div class="card">
				<h3>{{ __('company_profile_page_overview') }}</h3>

				<div class="row mt-3">
					<div class="col-md-6">
						<div class="card shadow">
							<div class="card-body">
								<h3><span class="badge badge-pill badge-primary">{{$successfulInvitations}}</span></h3>
							  	<h5 class="card-title">{{ __('company_profile_page_invitaions_delivered') }}</h5>
							  	
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="card shadow">
							<div class="card-body">
								<h3><span class="badge badge-pill badge-primary">{{$averageRating}}</span> <small>{{ trans('out of') }} 5.0</small></h3>
							  	<h5 class="card-title">{{ __('company_profile_page_company_rating') }}</h5>
							  	
							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="card shadow">
					<div class="card-body">
						<h5 class="mt-2">{{ __('company_profile_page_recently_invited') }}</h5>
						<div class="mt-2">
							@if (count($recentlyInvitedCustomers) > 0)
							<table class="table px-2 mt-2 table-bordered table-responsive-sm">
								<thead>
									<tr>
										<th scope="col">{{ trans('Email') }}</th>
										<th scope="col">{{ trans('Status') }}</th>
										<th scope="col">{{ trans('Name') }}</th>
										<th scope="col">{{ trans('Reference Number') }}</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($recentlyInvitedCustomers as $customer)
									<tr>
										<th style="font-weight: normal">{{ $customer["email"] }}</th>
										<td>{{ trans(enumToString($customer['status'])) }}</td>
										<td>{{ $customer["name"] }}</td>
										<td>{{ $customer["reference_number"] }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
							<a href="{{route('invitation-history')}}">{{ trans('company_profile_page_recently_invited_history') }}</a>
							@else
							<p>{{ trans('company_profile_page_recently_invited_no_customers') }}</p>
							@endif

						</div>
					</div>
				</div>

			</div><!-- /.card -->
		</div><!-- /.col-12 -->
		<div class="col-12 mx-auto">
			<div class="card">
				<h3>{{ __('company_profile_page_invite_customers') }}</h3>

				<div class="row mt-3 d-flex align-items-stretch">
					<div class="col-md-6 align-items-stretch">
						<div class="card shadow h-100">
							<div class="card-body">
							  	<h5 class="card-title">{{ __('company_profile_page_basic_invitation') }}</h5>
							  	<p class="card-text">{{ __('company_profile_page_basic_invitation_desc') }}</p>
								  <div class="form-group mt-2">
									<div class="input-group mb-3">
										<input type="text" class="form-control" id="evaluation-url"
											value="{{$company->getEvaluationUrl()}}" readonly="readonly">
									</div>
									<button data-target="evaluation-url" class="w-100 btn btn-outline-primary copyTextareaBtn
									copyTextareaBtn" type="button">{{__('copy')}}</button>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-6 align-items-stretch">
						{{-- if paid --}}
						@if ($company->isOnPaidSubscription() || $company->isOnGracePeriod())
						<div class="card shadow h-100">
							<div class="card-body">
								<h5 class="card-title">{{ __('company_profile_page_group_invitation') }}</h5>
								<p class="card-text">{{ __('company_profile_page_group_invitation_desc') }}</p>
								<a target="_" class="mt-2 invite-manually-button" href="{{route('invite-customers')}}"><i class="fas fa-file-upload" style="font-size: 30px;padding-right: 20px;"></i>{{__('company_profile_page_group_invitation_invite_text')}}</a>
							</div>
						</div>
						@endif
					</div>
				</div>


				@if ($company->isOnPaidSubscription() || $company->isOnGracePeriod())

				<div class="row mt-3">
					<div class="col d-flex justify-content-end">
						<a target="_" class="btn shadow btn-primary" href="{{route('email-template')}}">{{__('company_profile_page_view_email_template')}}<i class="fa fa-chevron-right ml-2"aria-hidden="true"></i></a>
					</div>
				</div>


				<h3 class="mt-5">{{ __('company_profile_page_arcs') }}</h3>
				<div class="my-3">
					<p>{{__('company_profile_page_arcs_desc_1')}}</p>
					<div class="form-group mt-2">
						<div class="input-group mb-3">
							<input type="text" class="form-control" id="bcc-email" value="{{$company->getBCCEmail()}}"
								readonly="readonly">
							<div class="input-group-append">
								<button data-target="bcc-email" class="btn btn-outline-secondary copyTextareaBtn
                    copyTextareaBtn" type="button">{{__('copy')}}</button>
							</div>
						</div>
					</div>
					<p class="p-0 m-0">
						{{__('company_profile_page_arcs_desc_2')}}
					</p>
					<div class="row mt-4 afs-codes">
						<div class="col-md-6">
							<div class="card shadow h-100">
								<div class="card-body">
									<h5 class="card-title">{{ __('Shopify') }}</h5>
									<p class="p-0 m-0 my-2 p-3 h-100" id="structured-data" style="border-radius:4px;background-color:#e9ecef">
										<?=nl2br(htmlspecialchars(trim('
											<!-- <script>
											{
												"recipientEmail":"{{customer.email}}",
												"recipientName":"{{customer.name}}",
												"referenceId":"{{name}}"
											}
											</script> -->')))
											?>
									</p>
									<input style="opacity:0;height:2px" id="shopify" value="<?=nl2br(htmlspecialchars(trim('<!-- <script>{"recipientEmail":"{{customer.email}}","recipientName":"{{customer.name}}","referenceId":"{{name}}"}</script> -->')))?>">
									<button data-target="shopify" class="w-100 btn btn-outline-primary copyTextareaBtn
									copyTextareaBtn" type="button">{{__('copy')}}</button>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="card shadow h-100">
								<div class="card-body">
									<h5 class="card-title">{{__('Woo-Commerce')}}</h5>
									<p class="p-0 m-0 my-2 p-3 h-100" id="structured-data" style="border-radius:4px;background-color:#e9ecef">
										<?=nl2br(htmlspecialchars(trim('
										<?php
											echo(
											\'<!--<script>{
												"recipientEmail":"\'.$order->get_billing_email().\'",
												"recipientName":"\'.$order->get_formatted_billing_full_name().\'",
												"referenceId":"\'.$order->get_id().\'"
											}</script> -->\'
											);
										?>
										')))
										?>
									</p>
									<input style="opacity:0;height:2px" id="woo-commerce" value="{{trim('<!--<script>{"recipientEmail":"\'.$order->get_billing_email().\'","recipientName":"\'.$order->get_formatted_billing_full_name().\'","referenceId":"\'.$order->get_id().\'"}</script> -->')}}">
									<button data-target="woo-commerce" class="w-100 btn btn-outline-primary copyTextareaBtn
									copyTextareaBtn" type="button">{{__('copy')}}</button>
								</div>
							</div>
						</div>
					</div>

					
				<div class="row mt-3">
					<div class="col d-flex justify-content-end">
						<a target="_" class="btn shadow btn-primary" href="{{route('invitation-settings')}}">{{__('company_profile_page_view_invitation_settings')}}<i class="fa fa-chevron-right ml-2"aria-hidden="true"></i></a>
					</div>
				</div>
					
					<p class="mt-5">
						{{__('company_profile_page_help')}}
					</p>
					
				</div>
				@endif
				
			</div><!-- /.card -->
		</div><!-- /.col-10 -->



		{{-- if no companies claimed --}}
		@else
			<div class="col-12 mx-auto">
				<div class="card">
					<h2>{{ __('My Company') }} </h2>
					<hr>
					<div class="well">
						{{ __( 'Все още не сте заявили компания.' )}}
					</div><!-- /.well -->
				</div><!-- /.card -->
			</div><!-- /.col-10 -->
		@endif
	</div>
</div><!-- /.container -->

@endsection

@section('extraJS')
<script>
	copyTextareaBtns = document.querySelectorAll(".copyTextareaBtn");
  copyTextareaBtns.forEach(copyTextAreaBtn => {
    copyTextAreaBtn.addEventListener('click', function(event) {
      var copyTextarea = document.getElementById(copyTextAreaBtn.getAttribute('data-target'));
      copyTextarea.focus();
      copyTextarea.select();
      
      try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
      } catch (err) {
        console.log('Oops, unable to copy');
      }
    });
  });
</script>

@endsection

@section('extraCSS')
<style>
	.invite-manually-button {
		height: 120px;
		background: transparent;
		border: 2px solid grey;
		border-radius: 4px;
		display: flex;
		justify-content: center;
		align-items: center;
	}

</style>
@endsection

@if ($company && ($company->isOnPaidSubscription() || $company->isOnGracePeriod()))

<!-- Chat Widget -->
<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="d8e98833-3af8-4ab3-9efe-1d32d763a6fc";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
@endif

