@extends( 'base' )

@section( 'content' )

<div class="container" id="app">
    <a href="{{route('mycompany')}}"><i class="fa fa-chevron-left mr-2"
            aria-hidden="true"></i>{{ trans('company_invite_customers_go_back') }}</a>
    <invite-customer :template="{{\json_encode($template)}}" :emails="{{\json_encode($siteEmails)}}"
        :user="{{\json_encode(auth()->user())}}" :site="{{\json_encode(auth()->user()->company)}}"
        :pending-invitation="{{ \json_encode($pendingInvitation)}}"
        :email-subject="{{ \json_encode($subject)}}"
        :is-default-email="{{\json_encode(auth()->user()->company->email_template==null)}}"
        >
    </invite-customer>
</div>

@endsection

@section('extraJS')
<script src="/public/js/app.js"></script>
@endsection