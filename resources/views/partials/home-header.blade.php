@if( Options::get_option( 'homepage_header_image' ) )
<style>
.homepage-header {
    margin-top: -10px;
    background-image: url('/public/{{ Options::get_option( 'homepage_header_image' ) }}');
}
</style>
@endif

<div class="homepage-header">
  @if (count($questionnaireCategories)>0)
   <div id="app" style="background: #fff">
        <div class="review-questionnaire container p-4 p-md-5">
            <review-questionnaire :categories="{{\json_encode($questionnaireCategories)}}"></review-questionnaire>
            <div class="review-questionnaire__search">
              <div class="searchProcessing"></div><!-- /.searchProcessing -->
              <form method="GET" action="{{ route('search') }}" id="searchUser" class="mb-3">
              <div class="hero__search-bar">
                <h4 class="mb-3">{{ __("Can't find the company? Search for it here...") }}</h4>
                <div class="hero__search-bar--field">
                    <input autocomplete="off" type="text" name="searchQuery" id="search-query" class="form-control search-padding" placeholder="{{ __('Name of the company or website') }}" required>
                    <div id="search-results" style="display: none">
                    </div>
                </div><!--
                --><div class="hero__search-bar--button">
                    <input type="submit" name="searchAction" class="btn btn-success btn-block search-btn-padding" value="{{ __('Go') }}">
                </div><!-- /.col-md-1 no-padding -->
                </div><!-- /.row -->
              </form>
            </div>
        </div>
    </div>
  @else
  <div class="homepage-header-container__wrapper">
    <div class="homepage-header-container">
      <div class="homepage-header-container__left">
        <h1>{{ __('Hero Title') }}</h1>
        <p class="homepage-header-container__subline">{{ __('Hero Slogan') }}</p>
        <div class="searchProcessing"></div><!-- /.searchProcessing -->
        <form method="GET" action="{{ route('search') }}" id="searchUser" class="mb-3">
        <div class="hero__search-bar">
          <div class="hero__search-bar--field">
              <input autocomplete="off" type="text" name="searchQuery" id="search-query" class="form-control search-padding" placeholder="{{ __('Name of the company or website') }}" required>
              <div id="search-results" style="display: none">
              </div>
          </div><!--
          --><div class="hero__search-bar--button">
              <input type="submit" name="searchAction" class="btn btn-success btn-block search-btn-padding" value="{{ __('Go') }}">
          </div><!-- /.col-md-1 no-padding -->
          </div><!-- /.row -->
        </form>
        <a href="{{route('forBusinesses')}}" class="arrow__link"><span>{{ __('Got a business? Click here') }}</span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M17.707 9.293l-5-5a1 1 0 00-1.414 1.414L14.586 9H3a1 1 0 100 2h11.586l-3.293 3.293a1 1 0 001.414 1.414l5-5a1 1 0 000-1.414z"></path></svg></a>
      </div>
      <div class="homepage-header-container__right">
        <div class="homepage-header-container__explainer-video">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/sI9RY6uIlEo?showinfo=0&rel=0&color=white&controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </div><!-- /.homepage-header-container -->
    
  </div>
  @endif
</div><!-- ./jumbotron-->