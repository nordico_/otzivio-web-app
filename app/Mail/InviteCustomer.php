<?php

namespace App\Mail;

use App\Sites;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InviteCustomer extends Mailable
{
    use Queueable, SerializesModels;

    public $html;
    public $site;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($html,Sites $site,string $subject)
    {
        $this->html = $html;
        $this->site = $site;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS'))->subject($this->subject)
            ->markdown('emails.invitation', ['html' => $this->html]);
    }
}
