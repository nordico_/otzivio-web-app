<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Lato|Patua+One&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<style>
	html {
		font-family: 'Helvetica', 'Arial', sans-serif;
		font-size: 16px;
		font-weight: 400;
		line-height: 1.5;
		-webkit-text-size-adjust: 100%;
		-ms-text-size-adjust: 100%;
		background: transparent;
	}
	body {
		margin:  0;
		background: transparent;
		text-align:center;
	}
	h3 {
		font-weight: bold;
		font-size: 1.4em;
		line-height: 1em;
		color: #000;
		display: inline;
	}
	h4 {
		display: inline;
	}
	.rating-stars {
		margin: 3px auto 0;
		display: block;
		background-image: url('/images/rating-0-simple.svg');
		background-repeat: repeat-x;
		background-size: 35px;
		width: 175px;
		text-align: left;
	}
	.rating-stars img {
		width: 35px;
		height: auto;
		margin: 0px;
		display: inline-block;
	}
	.based-on {
		font-size: 12px;
		margin: 3px 0 10px;
		letter-spacing: 0.5px;
	}
	.container {
		box-sizing: content-box;
		max-width: 250px;
		padding-left: 15px;
		padding-right: 15px;
		padding-top: 0;
		padding-bottom: 0;
		text-align: center;
	}
	.otzivio-logo {
		width: 100px;
		display: block;
		margin: 20px auto 0;
	}
	.full-review-link {
		margin: 0 0 10px;
		display: block;
		font-weight: 300;
		text-decoration: none;
	}
	body a {
		color: #000;
		font-weight: bold;
	}
	.checked {
  		color: orange;
	}
	</style>
</head>
<body>
	<div class="container">
		
		<h3>{{ $c->trustScore }}</h3>
		<div class="rating-stars">
			@if(number_format($avg,2) > 0 && number_format($avg,2)
			<= 1.49) {!! str_repeat('<img src="/images/rating-1-simple.svg" />', $avg) !!}
			@elseif(number_format($avg,2) >= 1.50 && number_format($avg,2)
			<= 2.49) {!! str_repeat('<img src="/images/rating-2-simple.svg" />', $avg) !!}
			@elseif(number_format($avg,2) >= 2.50 && number_format($avg,2)
			<= 3.49) {!! str_repeat('<img src="/images/rating-3-simple.svg" />', $avg) !!}
			@elseif(number_format($avg,2) >= 3.50 && number_format($avg,2)
			<= 4.49) {!! str_repeat('<img src="/images/rating-4-simple.svg" />', $avg) !!}
			@elseif(number_format($avg,2) >= 4.50)
			{!! str_repeat('<img src="/images/rating-5-simple.svg" />', $avg) !!}
			@endif
		</div>
		@if( number_format($avg,2) > 0 )
			<p class="based-on">{{ __('rating_box_based_on_text') }} <a href="{{ $c->slug }}" target="_blank">{{ count($c->reviews) }} {{ __('rating_box_based_on_text_reviews') }}</a></p>
		@else
			<a href="{{ $c->slug }}" target="_blank" class="full-review-link">
				{{ __( 'Leave a review' )}}
			</a>
		@endif
		<a href="{{ $c->slug }}" target="_blank"><img src="/images/logo-black-text.svg" class="otzivio-logo" /></a>
	</div>
</body>
</html>