@component('mail::message')

{!! $data['html'] !!}

<br>
{{ config('app.name') }}
@endcomponent