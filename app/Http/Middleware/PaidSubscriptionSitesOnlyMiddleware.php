<?php

namespace App\Http\Middleware;

use App\Sites;
use Closure;

class PaidSubscriptionSitesOnlyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $site = $request->route('site');
        if(!$site) return next($request);
        if(!$site->isOnPaidSubscription() && !$site->isOnGracePeriod()) return response()->json([
            'message'=>'You need to be on paid subscription to view this page',
        ]);
        return $next($request);
    }
}
