<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;


class SocialController extends Controller
{
    public function login(Request $request)
    {
        $isANewUser = false;
        $validator =  Validator::make($request->all(), [
            'provider' =>
            [
                'required',
                Rule::in([User::GOOGLE_AUTH_PROVIDER, User::FACEBOOK_AUTH_PROVIDER]),
            ],
            'user_id' => 'required',
            'email' => 'required',
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false], 400);
        }

        $user = User::whereProvider($request['provider'])
            ->whereProviderUserId($request['user_id'])
            ->first();

        if (!$user) {
            $user = User::whereEmail($request['email'])->first();
            if (!$user) {
                $isANewUser = true;
                $user = User::create([
                    'provider_user_id' => $request['user_id'],
                    'provider' => $request->provider,
                    'email' => $request['email'],
                    'name' => $request['name'],
                    'profilePic' => $request['picture'] ? $this->storeImageFromUrl($request['picture']) : null,
                    'email_verified_at' => Carbon::now(),
                ]);

            } else {
                $user->update(
                    [
                        'email_verified_at' => $user->email_verified_at != null ? $user->email_verified_at : Carbon::now(),
                        'profilePic' => !$user->profilePic && $request['picture'] ? $this->storeImageFromUrl($request['picture']) : $user->profilePic,
                        'provider_user_id' => $user->provider_user_id ==null ?  $request['user_id'] : $user->provider_user_id,
                        'provider' => $user->provider ==null ? $request->provider : $user->provider,
                    ]
                );
            }
        }

        auth()->login($user);

        return response()->json(
            [
                'success' => true,
                'data'=>[
                    'user'=>$user,
                    'is_new_user'=>$isANewUser,
                ]
            ]
        );
    }

    private function storeImageFromUrl($url)
    {
        $filename = time() . '.' . Str::random(10) . '.jpg';
        Image::make($url)->save(public_path('storage/' . $filename));
        return $filename;
    }
}
