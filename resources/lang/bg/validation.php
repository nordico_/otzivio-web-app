<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute трябва да бъде одобрен.',
    'active_url'           => ':attribute не е валиден URL.',
    'after'                => ':attribute трябва да е дата след :date.',
    'alpha'                => ':attribute може да съдържа само букви.',
    'alpha_dash'           => ':attribute може да съдържа само букви, цифри и тирета.',
    'alpha_num'            => ':attribute може да съдържа само букви и цифри.',
    'array'                => ':attribute must be an array.',
    'before'               => ':attribute трябва да е дата преди :date.',
    'between'              => [
        'numeric' => ':attribute трябва да е между :min and :max.',
        'file'    => ':attribute трябва да е между :min and :max килобайта.',
        'string'  => ':attribute трябва да е между :min and :max знака.',
        'array'   => ':attribute трябва да е между :min and :max елемента.',
    ],
    'boolean'              => ':attribute полето трябва да е true или false.',
    'confirmed'            => ':attribute потвърждението не съвпада.',
    'date'                 => ':attribute е невалидна дата.',
    'date_format'          => ':attribute не съвпада с формат :format.',
    'different'            => ':attribute и :other трябва да бъдат различни.',
    'digits'               => ':attribute трябва да е :digits цифри.',
    'digits_between'       => ':attribute трябва да е между :min и :max digits.',
    'distinct'             => ':attribute полето има дублирана стойност.',
    'email'                => ':attribute трябва да е валиден имейл адрес.',
    'exists'               => 'Избраният :attribute е невалиден.',
    'filled'               => ':attribute полето е задължително.',
    'image'                => ':attribute трябва да е изображение.',
    'in'                   => 'Избраното :attribute е невалидно.',
    'in_array'             => ':attribute поле не съществува в :other.',
    'integer'              => ':attribute трябва да е цяло число.',
    'ip'                   => ':attribute трябва да е валиден IP адрес.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => ':attribute не може да бъде по-голямо от :max.',
        'file'    => ':attribute не може да бъде по-голямо от :max килобайта.',
        'string'  => ':attribute не може да бъде повече от :max знака.',
        'array'   => ':attribute не може да бъде повече от :max елемента.',
    ],
    'mimes'                => ':attribute трябва да е файл от един от следните видове: :values.',
    'min'                  => [
        'numeric' => ':attribute трябва да бъде поне :min.',
        'file'    => ':attribute трябва да бъде поне :min килобайта.',
        'string'  => ':attribute трябва да бъде поне :min знака.',
        'array'   => ':attribute трябва да бъде поне :min елемента.',
    ],
    'not_in'               => 'Избраното :attribute е невалидно.',
    'numeric'              => ':attribute трябва да бъде число.',
    'present'              => ':attribute полето трябва да бъде налице.',
    'regex'                => ':attribute формат е невалиден.',
    'required'             => ':attribute полето се изисква.',
    'required_if'          => ':attribute полето се изисква, когато :other е :value.',
    'required_unless'      => ':attribute полето се изисква, освеан ако :other е в :values.',
    'required_with'        => ':attribute полето се изисква, когато :values е налице.',
    'required_with_all'    => ':attribute полето се изисква, когато :values са налице.',
    'required_without'     => ':attribute полето се изисква, когато :values не са налице.',
    'required_without_all' => ':attribute полето се изисква, когато никое от :values са налице.',
    'same'                 => ':attribute и :other трябва да съвпадат.',
    'size'                 => [
        'numeric' => ':attribute трябва да бъде :size.',
        'file'    => ':attribute трябва да бъде :size килобайта.',
        'string'  => ':attribute трябва да бъде :size знака.',
        'array'   => ':attribute трябва да съдържа :size елемента.',
    ],
    'string'               => ':attribute must be a string.',
    'timezone'             => ':attribute трябва да е валидна времера зона.',
    'unique'               => ':attribute е вече взето.',
    'url'                  => ':attribute формат е невалиден.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'персонализирано съобщение',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
