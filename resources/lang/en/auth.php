<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'social' => 'Oops... it’s seems that the email you use is already connected to :platform. Please sign in with :platform instead.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
