<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddFreePlanInVerifyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('verify', function (Blueprint $table) {
            DB::statement("ALTER TABLE verify CHANGE COLUMN plan plan ENUM('free','monthly', '6months', 'yearly') NOT NULL");
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('verify', function (Blueprint $table) {
            DB::statement("ALTER TABLE verify CHANGE COLUMN plan plan ENUM('monthly', '6months', 'yearly') NOT NULL");
        });
    }

}
