<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sites;

class EmbeddedController extends Controller
{
    // render embedded iframe
    public function embeddedIframe( Sites $c) {


        if(!request()->has('key')) return response()->json(['message'=>'You seem to be missing the key parameter!'],401);

        $key = request()->get('key');

        $claimedBy = $c->claimer;

        if(base64_encode($claimedBy->email) != $key) return response()->json(['message'=>'Invalid key!'],400);
        
        // get this company reviews
        $reviews = $c->reviews;

        $content = view( 'iframe', compact( 'c', 'reviews' ) );

        return response( $content )
        				->header( 'Access-Control-Allow-Methods', 'GET,OPTIONS' )
        				->header( 'Access-Control-Allow-Origin', '*' );

    }

    // render embedded iframe
    public function embeddedScore( Sites $c ) {

        if(!request()->has('key')) return response()->json(['message'=>'You seem to be missing the key parameter!'],401);

        $key = request()->get('key');

        $claimedBy = $c->claimer;

        if(base64_encode($claimedBy->email) != $key) return response()->json(['message'=>'Invalid key!'],400);
        
        // get this company reviews
        $avg = $c->reviews->avg('rating');

        $content = view( 'iframe-score', compact( 'c', 'avg' ) );

        return response( $content )
        				->header( 'Access-Control-Allow-Methods', 'GET,OPTIONS' )
        				->header( 'Access-Control-Allow-Origin', '*' );

    }


}
