<!doctype html>
<html lang="{{ LaravelLocalization::getCurrentLocale() }}">

@include('partials/head')

<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K8VVB4G" height="0" width="0"
      style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  @include( 'partials/navi' )

  <main role="main">

    @if( 'home' == Route::currentRouteName() )
    @include( 'partials/home-header' )
    @yield( 'content' )
    @else
    @include( 'partials/inner-header' )
    <div class="main-wrapper">
      @yield( 'content' )
    </div>
    @endif
  </main>

  @include( 'partials/footer' )
  @include('partials/body-scripts')


</body>

</html>