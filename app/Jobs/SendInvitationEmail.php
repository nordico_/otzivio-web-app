<?php

namespace App\Jobs;

use App\Invite;
use App\InvitedCustomer;
use App\Mail\InviteCustomer;
use App\Sites;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class SendInvitationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $customer;
    private $mail;
    private $invite;
    private $isLast;
    private $site;
    private $isSendingReminder;
    public $timeout = 7200; // 2 hours

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(InvitedCustomer $customer, $mail,$invite, $isLast,Sites $site,bool $isSendingReminder=false)
    {
        $this->customer = $customer;
        $this->mail = $mail;
        $this->invite = $invite;
        $this->isLast = $isLast;
        $this->site = $site;
        $this->isSendingReminder = $isSendingReminder;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->customer->update(['status' => InvitedCustomer::QUEUED]);

        Mail::to($this->customer->email)->send(new InviteCustomer($this->mail,$this->site, $this->isSendingReminder ? $this->site->getReminderEmailSubject() : $this->site->getInvitationEmailSubject()));

        if (!Mail::failures()) {
            $this->customer->update(['status' => ($this->isSendingReminder ? InvitedCustomer::REMINDER_SENT: InvitedCustomer::DELIVERED),'delivered_at' => now()]);
        } else $this->customer->update(['status' => InvitedCustomer::FAILED]);

        if ($this->isLast && $this->invite!=null) $this->invite->update(['status' => Invite::COMPLETED]);
    }
}
