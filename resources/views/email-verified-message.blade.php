@extends( 'base' )

@section( 'content' )

<div class="container card">
	{{ __( 'Your Email Has Been Verified Successfully!' ) }}
</div><!-- /.container -->

@endsection