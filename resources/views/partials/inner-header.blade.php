<div class="container-fluid inner-search">
  <div class="inner-search__wrapper">
    <div class="searchProcessing"></div><!-- /.searchProcessing -->
    <form method="GET" action="{{ route('search') }}" id="searchUser">
      <div class="inner-search__search-box">
        <input type="text" id="search-query" autocomplete="off" name="searchQuery" class="form-control"
          placeholder="{{ __('Search for a company') }}" required>
        <div id="search-results" style="display: none">
        </div>
      </div>
      <input type="submit" name="searchAction" class="btn btn-success" value="{{ __('Go') }}">      
    </form>
  </div>
</div><!-- /.container-fluid -->