<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

class CustomersErrorExcel implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
    public function collection()
    {
        return $this->data;
    }
}
