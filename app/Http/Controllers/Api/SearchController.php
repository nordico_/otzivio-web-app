<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Sites;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Metadata;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function search(Request $request){
        $searchString = $request['search'];
        
        $sites = DB::table('sites')
        ->where('sites.publish','Yes')
        ->leftJoin('metadata','metadata.metaable_id','sites.id')
        // ->where('metadata.metaable_type',Sites::class)
        // ->where('metadata.type',Metadata::METADATA_TYPE_SEARCH)
        ->where(
            function ($q) use ($searchString) {
                $q->where('sites.url', 'LIKE', '%' . $searchString . '%')
                    ->orWhere('sites.business_name', 'LIKE', '%' . $searchString . '%')
                        ->orWhere('metadata.value','LIKE', '%' . $searchString . '%');
            }
        )->distinct('sites.id');

        // dd($sites->toSql());
        // $sites =Sites::wherePublish('Yes')
        //     ->where(
        //         function ($q) use ($searchString) {
        //             $q->where('url', 'LIKE', '%' . $searchString . '%')
        //                 ->orWhere('business_name', 'LIKE', '%' . $searchString . '%');
        //         }
        //     )->orWhereHas('searchMeta',function($q) use ($searchString){
        //         $q->where('value', 'LIKE', '%' . $searchString . '%');
        //     });

        $hasMoreSites = $sites->count() > 5;
        $sites = $sites->take(5)->get(['sites.business_name','sites.url']);
        $categories = Category::havingName($searchString)->get(['slug']);



        return response()->json([
            'sites'=>$sites,
            'categories'=>$categories,
            'has_more_sites' => $hasMoreSites,
        ]);
    }
}
