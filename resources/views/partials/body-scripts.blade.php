<script src="{{ asset('js/popper.min.js') }}"></script>

  <!-- jQuery Lib -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

  <!-- Twitter Bootstrap 4 Lib -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>

  <!-- BS Select JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>

  <!-- Stripe JS SDK -->
  <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

  <!-- Sick Slider -->
  <script type="text/javascript" src="{{ asset('js/slick.min.js') }}"></script>

  <script type="text/javascript">
    Stripe.setPublishableKey('{{ Options::get_option('STRIPE_PUBLISHABLE_KEY') }}');
  </script>

  <!-- App JS -->
  <script src="{{ asset('js/app.js') }}"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

  @include('sweet::alert')


  {!! Options::get_option( 'siteAnalytics' ) !!}

  <!-- extra JS loaded by other views -->
  @yield( 'extraJS' )

  <script src="{{ asset( 'js/cookieconsent.min.js' ) }}"></script>
  <script>
    if (!localStorage.noFirstVisit) {
        $.get("https://ipinfo.io/{{$ip}}?token={{env('IPINFO_TOKEN')}}", function( data ) {
          let location = window.location.href;
          console.log(data);
          if(data.country == "BG"){
            if(location.includes('en')) window.location.href = "{{str_replace('en','bg',$en_url)}}";
          }else{
            if(!location.includes('en')) window.location.href = "{{$en_url}}";
          }
        });
        localStorage.noFirstVisit = "1";
      }

      $('#language-switcher').change(function() {
        if($(this).val() == "en") window.location.href = "{{$en_url}}";
        else window.location.href = "{{str_replace('en','bg',$en_url)}}";
      });

        window.cookieconsent.initialise({
          "palette": {
            "popup": {
              "background": "#edeff5",
              "text": "#838391"
            },
            "button": {
              "background": "#4b81e8"
            }
          },
          "content": {
            "message": "{{ __("This website uses cookies for a better experience.") }}",
            "dismiss": "{{ __("Ok, I understand") }}",
            "link": "{{ __("Privacy Policy") }}",
            "href": "{{ __("/p-privacy-policy") }}",
          }
        });
  </script>

  {{-- for autocompleting search --}}
  <script>
    const searchUrl = "{{route('api.search')}}";
      const searchInputID = "search-query";
      const showAllResultsText = "{{ __('Show All Results') }}";
  </script>

  <script src="{{ asset('js/search.js') }}"></script>

  <script src="{{ asset('js/letterAvatar.js') }}"></script>