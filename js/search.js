const searchResultDivID = "search-results";
const searchTimer = 200;

let searchResult = {};
let isSearchResultVisible = false;
let searchQuery = "";

let activeLang = document.getElementsByTagName("html")[0].getAttribute("lang");


let translations={
    'No results found':'Няма намерени резултати',
    'companies':'компании',
    'categories':'категории'
}


function trans(word){
    if(activeLang!='en' && translations[word]) return translations[word];
    return word;
}



var timer;

$("#" + searchInputID).on('input', function () {
    searchQuery = $(this).val();
    if (searchQuery.length <= 1){
        $("#" + searchResultDivID).hide();
        isSearchResultVisible = false;
        searchResult = {};
    }else{
        if(timer) clearTimeout(timer);
        timer = setTimeout(()=>{
            searchForCompanies(searchQuery);
        }, searchTimer);
    }
})

// rebuild search rersult on focus
$("#" + searchInputID).on('focus', function () {
    if (!isSearchResultVisible) buildSearchList();
})

// remove search results on removing focus
$("body").on('click', function (event) {
    if (event.target.id != searchInputID) {
        if (isSearchResultVisible) $("#" + searchResultDivID).hide();
        isSearchResultVisible = false;
    }
})


function buildSearchList() {
    // if search result is empty return
    if(!Object.keys(searchResult).length > 0 ) return;

    $("#" + searchResultDivID).html("");
    isSearchResultVisible = true;
    $("#" + searchResultDivID).show();
    
    // if the search has not data show no result found
    if(!doesSearchResultHasData()) buildNotResultComponent();
    else{
        if (searchResult.sites && searchResult.sites.length > 0) buildSiteComponent();
        if (searchResult.categories && searchResult.categories.length > 0) buildCategoryComponent();
        if (searchResult.has_more_sites) buildViewAllResult();
    }
}

function doesSearchResultHasData(){
    return (searchResult.categories.length > 0 || searchResult.sites.length > 0);
}

//    builds the search result for sites
function buildNotResultComponent() {
    let p = $("<p class='no--result'></p>").text(trans("No results found"));
    $("#" + searchResultDivID).append(p);
}
//    builds the search result for sites
function buildSiteComponent() {
    let header = $("<p class='search--header'></p>").text(trans('companies'));
    let ul = $("<ul></ul>")
    searchResult.sites.forEach(datum => {
        let regex = new RegExp(searchQuery, 'i');
        let searchFoundTitle = datum.business_name.match(regex);
        let searchFoundURL = datum.url.match(regex);

        let siteNameHolder = $("<h5></h5>").html(datum.business_name.replace(searchFoundTitle, '<span class="search-match">' + searchFoundTitle + '</span>'));
        let siteUrlHolder = $("<p></p>").html(datum.url.replace(searchFoundURL, '<span class="search-match">' + searchFoundURL + '</span>'));

        let url = activeLang == 'bg' ? '/reviews/'+datum.url : '/en/reviews/'+datum.url;

        let anchor = $("<a target='_' href='" + url+ "'></a>");

        anchor.append(siteNameHolder);
        anchor.append(siteUrlHolder);

        let li = $("<li></li>");
        li.append(anchor);
        ul.append(li);
    });

    $("#" + searchResultDivID).append(header);
    $("#" + searchResultDivID).append(ul);
}

// builds the search results for categories
function buildCategoryComponent() {
    let header = $("<p class='search--header'></p>").text(trans('categories'));
    let ul = $("<ul></ul>")
    searchResult.categories.forEach(datum => {
        let regex = new RegExp(searchQuery, 'i');
        let searchFoundTitle = datum.title.match(regex);

        let siteNameHolder = $("<h5></h5>").html(datum.title.replace(searchFoundTitle, '<span class="search-match">' + searchFoundTitle + '</span>'));
        let anchor = $("<a target='_' href='" + datum.redirect_url + "'></a>");

        anchor.append(siteNameHolder);

        let li = $("<li></li>");
        li.append(anchor);
        ul.append(li);
    });

    $("#" + searchResultDivID).append(header);
    $("#" + searchResultDivID).append(ul);
}

//    builds show all results button
function buildViewAllResult() {
    let btn = $("<input id='all-result' type='submit' class='btn btn-success w-100'></input>").val(showAllResultsText);
    $("#" + searchResultDivID).append(btn);
}

function searchForCompanies(company) {
    $.ajax({
        type: "GET",
        url: searchUrl,
        data: {
            search: company,
        },
        success: function (data) {
            searchResult = data;
            buildSearchList();
        }
    });
}