@extends('layouts.business') 
@section('content')
<div class="for-businesses-page">
	<div class="gray-background pt-3 pb-5">
		<div class="container">
			<div class="inner-page__title text-center">
				<h1>{{ __('Start the conversation. Engage your customers today.') }}</h1>
			</div>
			<div class="text-center">
				<img src="/images/business/engage-with-customers.svg" class="mt-5 mt-md-0" style="max-width: 500px; width: 100%;" />
			</div>
		</div>
	</div>
	<div class="container text-center text-lg-left mt-5 pb-md-3">
		<div class="row">
			<div class="col-12">
				<h3>{{ __("Review management") }}</h3>
				<p class="h3_subtitle">{{ __("Reviews make your reputation. Easily manage both at once.") }}</p>
				<p>{{ __("How you engage with your customers can make or break your reputation. You need to take a proactive stance. Simply asking for reviews from all of your customers is an excellent start. However, responding to your reviews, in the right way, can take your company to the next level. We offer one-stop review management so that you can easily engage and dazzle your consumers, boost confidence and see your sales head towards the rafters.") }}</p>
			</div>
		</div>
	</div>
	<div class="container pb-md-5 mb-5">
		<div class="row d-flex">
			<div class="col-12 text-center text-lg-left col-lg-6 pr-md-5 align-self-center justify-content-sm-center order-2 order-lg-1">
				<h3 class="mt-4">{{ __('Let your customers know you care') }}</h3>
				<p>{{ __("Never turn a deaf ear on customer reviews. Respond and reap the rewards. Answering reviews is actually the best way to reach out to your customers and show your good customer service in action. Whether you're finding a solution to a problem or just saying 'Thanks', responding to your customers makes a difference. You'll win fans and gain a loyal customer base.") }}</p>
			</div>
			<div class="col-12 text-center col-lg-6 mt-5 align-self-center order-1 order-lg-2">
				<img src="/images/business/answer-reviews.svg" style="max-width: 400px; width: 100%;"  />
			</div>
		</div>
	</div>
	<div class="gray-background pt-5 pb-5 mt-5">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center col-lg-6 align-self-center">
					<img src="/images/business/receive-notfications.svg" style="max-width: 500px; width: 100%;" />
				</div>
				<div class="col-12 text-center text-lg-left col-lg-6 pl-md-5 align-self-center">
					<h3 class="mt-4">{{ __('Get instant notification of new reviews') }}</h3>
					<p>{{ __("You'll always know when you've got a new review. You'll find out right away if you've got a bad review, so you can respond in a matter of minutes.") }}</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container text-center text-lg-left mt-5">
		<div class="row">
			<div class="col-12">
				<h3>{{ __("Company Profile") }}</h3>
				<p class="h3_subtitle">{{ __("Show Bulgaria your company story") }}</p>
				<p>{{ __("The company profile is where you can show your customers why your company stands out. Use your company's pictures and logos. Further customize with special offers, promotions and testaments to your excellent customer service.") }}</p>
			</div>
		</div>
	</div>
	<div class="pt-5 pb-5">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center col-lg-6 align-self-center">
					<img src="/images/business/engage-why-your-company.png" style="max-width: 400px; width: 100%;" />
				</div>
				<div class="col-12 text-center text-lg-left col-lg-6 pl-md-5 align-self-center">
					<h3 class="mt-4">{{ __('Tell your customers why they should choose you') }}</h3>
					<p>{{ __("Delve into what defines your company here. Are you the best in your category? Is your customer service a thing of beauty? Fastest delivery times on record? Tell your customers what your best features are and prove it with a guarantee. Customers want to know that you're confident in your service, so tell them. Give them peace of mind and they're sure to return.") }}</p>
				</div>
			</div>
		</div>
	</div>
	@component('components/subscribe-to-otzivio-panel') @endcomponent
</div>
@endsection