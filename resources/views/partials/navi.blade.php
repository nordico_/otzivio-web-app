<nav class="navbar navbar-expand-xl navbar-dark fixed-top">
  <a class="navbar-brand site-logo" href="{{ route('home') }}">
    @if($logo = Options::get_option( 'site.logo' ))
    <img src="/public/{{ $logo }}" height="30" alt="Otzivio logo" />
    @else
    <img src="/images/logo.svg" alt="Otzivio logo" />
    @endif
    <span class="logo-beta">БЕТА</span>
  </a>

  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
    data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false"
    aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse justify-content-end" id="navbarsExampleDefault">
    <ul class="navbar-nav">
      <li class="nav-item @if(isset($activeNav) && ($activeNav == 'home')) active @endif">
        <a class="nav-link" href="{{ route('home') }}">{{ __('Home') }}</a>
      </li>
      <li class="nav-item @if(isset($activeNav) && ($activeNav == 'browse-categories')) active @endif">
        <a class="nav-link"
          href="{{ route('categories') }}">{{ __('Browse Companies') }}</a>
      </li>
      <li class="nav-item @if(isset($activeNav) && ($activeNav == 'submit-company')) active @endif">
        <a class="nav-link" href="{{ route('addCompany') }}">{{ __('Add Company') }}</a>
      </li>
      @if( auth()->guest() )
      <li class="nav-item">
        @if (url()->current() == route('storeCompany') || url()->current() == route('mycompany') ||
        str_contains(request()->path(),'reviews'))
        <a class="nav-link"
          href="{{ route('login').'?return='.getReturnUrl() }}">{{ __('Login') }}</a>
        @else
        <a class="nav-link" href="{{ route('login')}}">{{ __('Login') }}</a>
        @endif
      </li>
      <!--
      <li class="nav-item">
        @if (url()->current() == route('storeCompany') || url()->current() == route('mycompany') ||
        str_contains(request()->path(),'reviews'))
        <a class="nav-link"
          href="{{ route('register').'?return='.getReturnUrl() }}">{{ __('Signup') }}</a>
        @else
        <a class="nav-link" href="{{ route('register')}}">{{ __('Signup') }}</a>
        @endif
      </li>
    -->
      @else
      <li class="nav-item @if(isset($activeNav) && ($activeNav == 'my-account')) active @endif">
        <a class="nav-link"
          href="{{ route('myaccount') }}">{{ __('My Account') }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('logout') }}"
          onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
      </li>
      @endif
    </ul>
    <div class="nav-for-businesses pl-xl-2 mt-2 mt-xl-0">
      <a href="{{ route('forBusinesses') }}">{{ __('Got a business?') }}</a>
    </div>
  </div>
</nav>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>