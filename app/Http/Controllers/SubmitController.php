<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Sites;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use App\Mail\EmailNotification;
use App\Metadata;
use App\User;

use Mail;
use Options;


class SubmitController extends Controller
{

    // construct
    public function __construct() {
        // $this->middleware(['auth', 'verified']);
        
    }


    // submit company form
    public function submitCompanyForm(  ) {
        
        $categories = app('rinvex.categories.category')->all();
        $seo_title = __( 'Submit Company' ) . ' - ' . env( 'APP_NAME' );

        SEOMeta::setTitle(trans('meta_title_submit_company_page'));
        SEOMeta::setDescription(trans('meta_desc_submit_company_page'));
		OpenGraph::setTitle(trans('meta_title_submit_company_page'));
        OpenGraph::setDescription(trans('meta_desc_submit_company_page'));

        return view('submit-company', compact( 'categories', 'seo_title' ));

    }

    private function get_domain($url)
    {
        $pieces = parse_url($url);
        $domain = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];
        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }
        return false;
    }



    // process new entry
    public function submitStore ( Request $r )
    {

        if(!str_contains($r->url,'http://') && !str_contains($r->url,'https://')) $r->merge(['url'=>'http://'.$r->url]);

        // dd($r->url);
        $this->validate( $r, [ 'url' => 'required|url' ]);

        // parse only domain name
        $uri = $this->get_domain($r->url);

        $urls = ['http://',$uri,'https://'.$uri];
        
        $isValid = false;
        foreach ($urls as $url) {
            if(!$isValid){
                $file_headers = @get_headers($url);
                if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') $isValid = false;
                else $isValid=true;
            }
        }

        if(!$isValid){
            alert()->error(__('This URL could not be reached. Please check for errors'), __('URL Error'));
            return back();
        }

        // does this url exist?
        // if( !urlExists( $r->url ) ) {
        //     alert()->error(__('This URL could not be reached. Please check for errors'), __('URL Error'));
        //     return back();
        // }

        // check for duplicates
        if( Sites::whereUrl( $uri )->exists() ) {
            alert()->error(__('We already have this company listed'), __('Already Exists'));
            return back();
        }

        // save this site
        $site = new Sites;
        $site->url = $uri;
        $site->business_name = $this->__getNameFromUrl($uri);
        // $site->lati = $r->lati;
        // $site->longi = $r->longi;
        $site->location = $r->city_region ?? 'България';
        $site->submittedBy = auth()->user() ? auth()->user()->id : null;
        $site->publish="Yes";
        $site->save();


        if($r['search_meta']) $site->syncManyFromString($r['search_meta'], Metadata::METADATA_TYPE_SEARCH);

        // attach category to this site
        if($r->category_id) $this->__updateCategory( $site, $r->category_id );

        // notify admin by email
        $data[ 'message' ] = sprintf(__('New business added on the website called %s
                              Site URL: %s'), 
                                '<strong>'.$r->name.'</strong><br>', 
                                '<a href="'.$r->url.'">' . $uri . '</a>'
                                );

        $data[ 'intromessage' ] = __('New business added');
        $data[ 'url' ] = route( 'reviewsForSite', [ 'site' => $site->url ]);
        $data[ 'buttonText' ] = __('See Listing');

        Mail::to(Options::get_option( 'adminEmail' ))->send( new EmailNotification( $data ) );

        // set success message
        // alert()->success(__('This company has been added and will be reviewed before publishing to our site.'), __('Company Added'));
        alert()->success(__('This company has been added'), __('Company Added'));

        // redirect to the new listing
        return redirect()->route( 'reviewsForSite',['site'=>$site->url] );


    }

    private function __getNameFromUrl(string $url){
        $urlWithoutProtocol = str_replace(['https://','http://'],['',''],$url);
        $name = explode('.',$urlWithoutProtocol)[0];
        $name = str_replace(['-','_'],[' ',' '],$name);
        $name = ucwords(trim($name));
        return $name;
    }

    // set category
    private function __updateCategory( Sites $p, int $categoryId ): object {
        return $p->syncCategories( $categoryId, true);
    }

}
