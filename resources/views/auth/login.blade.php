@extends('base')

@section('extraCSS')
    <style>
        @keyframes spinner {
            from {
                transform: rotate(0deg);
            } to {
                transform: rotate(360deg);
            }
        }
        .loading{
            opacity: 0.7;
        }

        #overlay{
            display: none
        }
        .loading #overlay{
            position: absolute;
            height: 100%;
            width:100%;
            display: flex;
            justify-content: center;
            justify-items: center;
            align-items: center;
        }
        .loading #spinner {
            z-index: 10;
            min-width: 40px;
            min-height: 40px;
            border: 5px solid #3F3D56;
            border-right: 5px solid #18806f;
            border-radius: 50%;
            animation: spinner 1s linear infinite;
        }
        .container{
            position: relative;
        }
                
    </style>
@endsection

@section('content')
<div class="container">
    <div id="overlay">
        <div id="spinner"></div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card authentication">
                <h1 class="authentication__title">{{ __('Login') }}</h1>

                <div class="card-body">
                    <div id="status">
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <button onclick="javascript:login();" class="btn btn-primary mx-auto">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-facebook" viewBox="0 0 16 16">
                                    <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z"/>
                                </svg>
                                {{ __('Login With Facebook') }}
                            </button>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <button id="btn-google" class="btn btn-light mx-auto">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-google" viewBox="0 0 16 16">
                                    <path d="M15.545 6.558a9.42 9.42 0 0 1 .139 1.626c0 2.434-.87 4.492-2.384 5.885h.002C11.978 15.292 10.158 16 8 16A8 8 0 1 1 8 0a7.689 7.689 0 0 1 5.352 2.082l-2.284 2.284A4.347 4.347 0 0 0 8 3.166c-2.087 0-3.86 1.408-4.492 3.304a4.792 4.792 0 0 0 0 3.063h.003c.635 1.893 2.405 3.301 4.492 3.301 1.078 0 2.004-.276 2.722-.764h-.003a3.702 3.702 0 0 0 1.599-2.431H8v-3.08h7.545z"/>
                                </svg>
                                {{ __('Login With Google') }}
                            </button>
                        </div>
                    </div>
                    <div class="authentication__alternative-divider">
                        <span>{{ __('Or') }}</span>
                    </div>
                    <form method="POST" class="login-form" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="email" type="email" placeholder="{{ __('E-Mail Address') }}"
                                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                    value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="password" type="password" placeholder="{{ __('Password') }}"
                                    class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                    name="password" required>

                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                        {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" id="return" name="return">

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgotten password?') }}
                                </a>
                                @endif

                                <hr>
                                {!! __("No registration?") !!}
                                <a href='{{ route('register')}}' class="text-primary">{{ __('Signup') }}</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extraJS')




{{-- disable buttons on login submit --}}
<script>
    $(".login-form").on('submit',()=>{
        $('.btn').prop('disabled', true);
        $('.container').addClass('loading')
    })
</script>



<script type="text/javascript">
    (function(d){
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement('script'); js.id = id; js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));
</script>


<script type="text/javascript">
    window.fbAsyncInit = function() {
            FB.init({
            appId: "{!! env('FACEBOOK_CLIENT_ID') !!}",
            status: true,
            cookie: true,
            xfbml: true,
            version:'v10.0'
        });
    };

    function login() {
        FB.login(function(response) {
            FB.api('/me',{fields: 'id,name,email,picture'}, function (response) {
                submitUser(
                    {
                        "_token": "{{ csrf_token() }}",
                        provider:'facebook',
                        name:response.name,
                        email:response.email,
                        user_id:response.id,
                        picture:response.picture.data.url,
                    }
                )
            });
        }, {scope: 'public_profile,email'});            
    }

</script>

<script src="https://apis.google.com/js/api:client.js"></script>


<script type="text/javascript">
    var googleUser = {};
    var startApp = function() {
        gapi.load('auth2', function(){
            auth2 = gapi.auth2.init({
            client_id: "{!! env('GOOGLE_CLIENT_ID') !!}",
            cookiepolicy: 'single_host_origin',
            });
            attachSignin(document.getElementById('btn-google'));
        });
    };

  function attachSignin(element) {
    auth2.attachClickHandler(element, {},
        function(googleUser) {
           var profile = googleUser.getBasicProfile();
           submitUser(
                {
                    "_token": "{{ csrf_token() }}",
                    provider:'google',
                    name:profile.getName(),
                    email:profile.getEmail(),
                    user_id:profile.getId(),
                    picture:profile.getImageUrl(),
                }
        );
        
        }, function(error) {
          
        });
  }
  startApp();
</script>


<script>

    function param(name) {
        let rn = (location.search.split(name + '=')[1] || '').split('&')[0];
        return rn =="" ? null : rn;
    }

    $("#return").val(param("return"));

    function submitUser(data){
        $('.btn').prop('disabled', true);
        $('.container').addClass('loading')
        $.ajax({
            type: "POST",
            url: "{{route('api.social.login')}}",
            data: data,
            success: function(response){
                // show welcome message to new user
                if(response.data.is_new_user){
                    sweetAlert("{{__('otzivio_new_user_welcome_message')}}", '', "success");
                    setTimeout(()=>{
                        window.location.href = param("return") != null ? (window.location.origin+ param("return")) : '{{route('myaccount')}}'
                    },1200)
                }
                else window.location.href = param("return") != null ? (window.location.origin+ param("return")) : '{{route('myaccount')}}'
            },
            error:function(response){
                $('.btn').prop('disabled', false);
                sweetAlert("Oops...", response.error.message, "error");
                $('.container').removeClass('loading')
            }
        });
    }
</script>
@endsection