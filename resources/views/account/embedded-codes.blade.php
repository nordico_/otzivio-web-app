@extends( 'base' )

@section( 'content' )

<div class="container">
<div class="row">
<div class="col-12 mx-auto">
<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link" href="{{ route( 'myaccount' ) }}">{{ __('My Reviews') }}</a>
  </li>
  
  <li class="nav-item">
    <a class="nav-link" href="{{ route( 'myprofile' ) }}">{{ __('My Profile') }}</a>
  </li>

  <li class="nav-item">
    <a class="nav-link active" href="{{ route( 'mycompany' ) }}">{{ __('My Company') }}</a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="{{ route( 'mybilling' ) }}">{{ __('My Billing') }}</a>
  </li>
  
  <li class="nav-item">
    <a class="nav-link" href="{{ route( 'logout' ) }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">{{ __('Log Out') }}</a>
  </li>
</ul>
</div><!-- /.col-10 card -->

<div class="col mx-auto">
<div class="card">
<h1>{{ __('My Embedded Codes') }}</h1>
<hr>
@if (auth()->user()->isOnPaidSubscription())
	<h2>{{ __('reviews_box') }}</h2>

	<div class="row mt-3">
		<div class="col-md-4">
		<h4>{{ __('Customize appearance') }}</h4>
		<form method="POST" id="iframe-customizer">
			@csrf
			<dl>
				<dt>{{ __('Star Color') }}</dt>
				<dd><input type="text" name="urlFC" class="form-control cp" value="{{ Options::get_option('urlFC_' . $company->id ) }}"></dd>
				<dt>{{ __('General Background Color') }}</dt>
				<dd><input type="text" name="generalBG" class="form-control cp" value="{{ Options::get_option('generalBG_' . $company->id ) }}"></dd>
				<dt>{{ __('Testimonial Background Color') }}</dt>
				<dd><input type="text" name="testiGB" class="form-control cp" value="{{ Options::get_option('testiGB_' . $company->id ) }}"></dd>
				<dt>{{ __('General Font Color') }}</dt>
				<dd><input type="text" name="generalFC" class="form-control cp" value="{{ Options::get_option('generalFC_' . $company->id ) }}"></dd>
				<dt>{{ __('Testimonial Font Color') }}</dt>
				<dd><input type="text" name="testiFC" class="form-control cp" value="{{ Options::get_option('testiFC_' . $company->id ) }}"></dd>
			</dl>
		</form>
		</div><!-- /.col-xs-12 col-lg-6 -->
		<div class="col-md-8">
			<h4>{{ __('Preview') }}</h4>
			{!! '<iframe id="preview" src="'.route('embedded', [ 'site' => $company ,'key' => base64_encode(auth()->user()->email)]).'" frameborder="0" style="width: 100%;" height="250" scrolling="no"></iframe>' !!}
			<div class="alert alert-info">
				{{ __('Copy & Paste this code where you want the reviews to show') }}
			</div><!-- /.well -->
			<textarea class="form-control" rows="3">
			{{ '<iframe src="'.route('embedded', [ 'site' => $company ,'key' => base64_encode(auth()->user()->email)]).'" frameborder="0" width="900" height="250" scrolling="no"></iframe>' }}
			</textarea>
		</div><!-- ./preview -->
	</div><!-- ./row -->

	<br>

	<h2>{{ __('rating_box') }}</h2>
	<hr>
	<iframe class="mt-4" src="{{ route('embeddedScore', ['site' => $company,'key' => base64_encode(auth()->user()->email)]) }}" frameborder="0" width="250" height="150" scrolling="no"></iframe>
	<textarea class="form-control mt-4" rows="3">
		<iframe src="{{ route('embeddedScore', ['site' => $company,'key' => base64_encode(auth()->user()->email)]) }}" frameborder="0" width="250" height="150" scrolling="no"></iframe>
	</textarea>

@endif

@endsection

@section( 'extraCSS' )
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css">
@endsection

@section( 'extraJS' )
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/js/bootstrap-colorpicker.min.js"></script>

<script>
	$(function() {
		$( '.cp' ).colorpicker();

		$('#iframe-customizer').change(function(el) {

			var inpField = el.target.name;
			var inpValue = el.target.value;
			var token = $( 'input[name=_token]' ).val();

			$.post( '/account/setCompanyWidgetColors', {'field': inpField, 'value':inpValue, '_token': token }, function(resp) {
				
				if( typeof resp.success !== undefined ) {
					$( '#preview' ).attr( 'src', function ( i, val ) { return val; });
				}

				if( resp.error !== undefined ) {
					alert( resp.error );
				}

			});

		});

	});

</script>
@endsection