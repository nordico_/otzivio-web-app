<?php

namespace App\Http\Middleware;

use Closure;

class PaidSubscriptionUsersOnlyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $company = auth()->user()->company;
        if(!$company) return redirect()->back();
        if(!$company->isOnPaidSubscription() && !$company->isOnGracePeriod()) return redirect()->back();
        return $next($request);
    }
}
