<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_settings', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('sites_id');
            $table->foreign('sites_id')->references('id')->on('sites');

            $table->unsignedBigInteger('invitation_email_template_id')->nullable();
            $table->foreign('invitation_email_template_id')->references('id')->on('email_templates');

            $table->unsignedBigInteger('reminder_email_template_id')->nullable();
            $table->foreign('reminder_email_template_id')->references('id')->on('email_templates');
            
            $table->longText('invitation_email_subject')->nullable();
            $table->longText('reminder_email_subject')->nullable();
            $table->integer('send_invitation_after')->default(7);
            $table->integer('send_reminder_after')->default(7);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_settings');
    }
}
