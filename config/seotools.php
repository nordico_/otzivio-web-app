<?php
/**
 * @see https://github.com/artesaos/seotools
 */

return [
    'meta' => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults'       => [
            'title'        => 'Отзивио', // set false to total remove
            'titleBefore'  => false, // Put defaults.title before page title, like 'It's Over 9000! - Dashboard'
            'description'  => 'Четете, пишете и споделяйте отзиви с Отзивио. Отзивио Ви помага бързо да намерите и напишете отзиви за компании, с които сте имали преживяване.', // set false to total remove
            'separator'    => ' | ',
            'keywords'     => ['отзив', 'отзиви', 'отзивио', 'мнение', 'репутация'],
            'canonical'    => false, // Set null for using Url::current(), set false to total remove
        ],
        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google'    => null,
            'bing'      => null,
            'alexa'     => null,
            'pinterest' => null,
            'yandex'    => null,
            'norton'    => null,
        ],

        'add_notranslate_class' => false,
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title'       => 'Отзивио', // set false to total remove
            'description' => 'Четете, пишете и споделяйте отзиви с Отзивио. Отзивио Ви помага бързо да намерите и напишете отзиви за компании, с които сте имали преживяване.', // set false to total remove
            'url'         => false, // Set null for using Url::current(), set false to total remove
            'type'        => false,
            'site_name'   => false,
            'images'      => [],
        ],
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
            //'card'        => 'summary',
            //'site'        => '@LuizVinicius73',
        ],
    ]
];
