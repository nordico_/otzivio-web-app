<?php

namespace App;

use App\Traits\Metaable;
use Illuminate\Database\Eloquent\Model;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Options;

class Sites extends Model
{
    use \Rinvex\Categories\Traits\Categorizable;
    use Metaable;

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'url';
    }

    public const SETTING_KEYS = [
        SiteSetting::SETTING_KEY_SEND_INVITATION_AFTER,
        SiteSetting::SETTING_KEY_SEND_REMINDER_AFTER,
        SiteSetting::SETTING_KEY_INVITATION_EMAIL_SUBJECT,
        SiteSetting::SETTING_KEY_REMINDER_EMAIL_SUBJECT,
        SiteSetting::SETTING_KEY_REMINDER_EMAIL_TEMPLATE,
        SiteSetting::SETTING_KEY_INVITATION_EMAIL_TEMPLATE,
    ];

    public $defaultSettings = [];

    public function __construct()
    {
        $this->defaultSettings=[
            SiteSetting::SETTING_KEY_SEND_INVITATION_AFTER=>7,
            SiteSetting::SETTING_KEY_SEND_REMINDER_AFTER=>5,
            SiteSetting::SETTING_KEY_INVITATION_EMAIL_SUBJECT=>SiteSetting::DEFAULT_SUBJECT,
            SiteSetting::SETTING_KEY_REMINDER_EMAIL_SUBJECT=>SiteSetting::DEFAULT_SUBJECT,
            SiteSetting::SETTING_KEY_REMINDER_EMAIL_TEMPLATE=>EmailTemplate::DEFAULT_EMAIL_TEMPLATE,
            SiteSetting::SETTING_KEY_INVITATION_EMAIL_TEMPLATE=>EmailTemplate::DEFAULT_EMAIL_TEMPLATE,
            
        ];
    }

    protected $appends = ['redirect_url', 'search_meta','evaluation_url'];

    protected $fillable = ['claimedBy','featured_image'];

    // cast to array
    public $casts = ['metadata' => 'array'];

    public function getRedirectUrlAttribute()
    {
        return LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(), route('reviewsForSite', [
            'site' => $this->url
        ]));
    }
    public function getEvaluationUrlAttribute()
    {
        return $this->getEvaluationUrl();   
    }

    public function SiteSettings(){
        return $this->hasOne(SiteSetting::class);
    }

    public function getSettings(){

        $settings = $this->SiteSettings;

        if(!$settings) return $this->defaultSettings;
        
        foreach (self::SETTING_KEYS as $key) {
            if($settings[$key] === null) $settings[$key] = $this->defaultSettings[$key];
        }

        return $settings;
    }

    public function getInvitationEmailSubject(){
        return str_replace(SiteSetting::SETTING_SUBJECT_SITE_NAME_PLACEHOLDER,$this->business_name,$this->getSettings()['invitation_email_subject']);
    }

    public function getReminderEmailSubject(){
        return str_replace(SiteSetting::SETTING_SUBJECT_SITE_NAME_PLACEHOLDER,$this->business_name,$this->getSettings()['reminder_email_subject']);
    }

    public function getInvitationEmailTemplate(){
        return $this->getSettings()['invitation_email_template'];
    }
    public function getReminderEmailTemplate(){
        return $this->getSettings()['reminder_email_template'];
    }

    public function emails()
    {
        return $this->hasMany(SiteEmail::class, 'site_id');
    }
    public function verifiedEmails()
    {
        return $this->hasMany(SiteEmail::class, 'site_id')->verified();
    }
    public function getEmailList()
    {
        return $this->verifiedEmails()->pluck('email')->toArray();
    }
    public function getSearchMetaAttribute()
    {
        return implode(",", $this->searchMeta()->get()->pluck('value')->toArray());
    }

    // relationship to reviews
    public function reviews()
    {
        return $this->hasMany(Reviews::class, 'review_item_id', 'id');
    }

    // relationship to claimer
    public function claimer()
    {
        return $this->belongsTo(User::class, 'claimedBy', 'id');
    }

    // relationship to submitter
    public function submitter()
    {
        return $this->belongsTo(User::class, 'submittedBy', 'id');
    }

    public function category()
    {
        return $this->morphToMany(Category::class, 'categorizable')->where('categories.type',Category::CATEGORY_TYPE_INDUSTRY);
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscriptions::class, 'site_id');
    }
    public function lastSubscription()
    {
        return $this->hasMany(Subscriptions::class, 'site_id')->latest('subscription_date')->first();
    }

    public function isOnPaidSubscription()
    {
        return $this->subscriptions()->where('subscription_status', Subscriptions::SUBSCRIPTION_ACTIVE)->latest('subscription_date')->first() != null;
    }

    public function isOnTrialPeriod()
    {
        if ($this->subscriptions->count() != 1) return false;
        $subscription = $this->lastSubscription();
        if ($subscription->isCancelled()) return false;

        return $this->subscriptions->first()->isOnTrialPeriod();
    }

    // will be on grace period if the subscription period is not over
    public function isOnGracePeriod()
    {
        if ($this->isOnPaidSubscription()) return false;
        $lastCancelledSubscription = $this->subscriptions()->where('subscription_status', Subscriptions::SUBSCRIPTION_CANCELLED)->latest('subscription_date')->first();
        if (!$lastCancelledSubscription) return false;
        return $lastCancelledSubscription->isOnGracePeriod();
    }

    // compute slug
    public function getSlugAttribute()
    {
        return route('reviewsForSite', ['site' => $this]);
    }

    public function getEvaluationUrl()
    {
        return route('evaluate', ['site' => $this]);
    }
    public function getBCCEmail()
    {
        $namespace = env('TESTMAIL_NAMESPACE') . ".";
        $provider = "@inbox.testmail.app";

        return $namespace . $this->url . $provider;
    }

    // get screenshot
    public function getScreenshotAttribute()
    {

        // get licence
        $license = Options::get_option('license_key', 'None');

        if ($this->hasLogo())
            return asset('domain-logos/' . $this->metadata['logo']);
        else
            return 'http://api.pagepeeker.com/v2/thumbs.php?size=x&url='.(str_contains($this->url,'www.') ? $this->url:'www.'.$this->url);
    }

    public function hasLogo(){
        return isset($this->metadata) && isset($this->metadata['logo']);
    }

    public function metadata()
    {
        return $this->morphMany(Metadata::class, 'metaable');
    }
    public function searchMeta()
    {
        return $this->metadata()->where('type', Metadata::METADATA_TYPE_SEARCH);
    }
    public function seoMeta()
    {
        return $this->metadata()->where('type', Metadata::METADATA_TYPE_SEO);
    }

    // get trustscore attribute 
    /*
    'prettyBad' < 2
    'bad' >= 2 and <= 3
    'good' > 3 and < 4
    'excellent' >= 4
    */
    public function getTrustScoreAttribute()
    {

        $rating = $this->reviews->avg('rating');

        if (!$rating)
            return __('trustscore.notRated');

        switch ($rating) {

            case $rating <= 1.49:
                return __('trustscore.prettyBad');
                break;

            case $rating >= 1.50 && $rating <= 2.49:
                return __('trustscore.bad');
                break;

            case $rating >= 2.50 && $rating <= 3.49:
                return __('trustscore.good');
                break;

            case $rating >= 3.50 && $rating <= 4.49:
                return __('trustscore.veryGood');
                break;

            case $rating >= 4.50:
                return __('trustscore.excellent');
                break;

            default:
                return __('trustscore.notRated');
                break;
        }
    }

    public function scopeHavingNameOrUrl($query, String $search)
    {
        return $query->whereRaw("lower('business_name') like '%" . strtolower($search) . "%'")->orWhere('url', 'like', "%" . $search . "%");
    }

    public function invites()
    {
        return $this->hasMany(Invite::class, 'site_id');
    }
    public function invitedCustomers()
    {
        return $this->hasMany(InvitedCustomer::class, 'site_id');
    }

    public function averageRating()
    {
        return number_format((float) $this->reviews()->published()->avg('rating'), 1, '.', '');
    }
}
