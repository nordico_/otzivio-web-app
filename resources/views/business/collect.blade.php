@extends('layouts.business') 
@section('content')
<div class="for-businesses-page">
	<div class="gray-background pt-3 pb-5">
		<div class="container">
			<div class="inner-page__title text-center">
				<h1>{{ __('Collect reviews and real insight with ease') }}</h1>
			</div>
			<div class="text-center">
				<img src="/images/business/collect-reviews.svg" class="mt-5 mt-md-0" style="max-width: 500px; width: 100%;" />
			</div>
		</div>
	</div>
	<div class="container text-center text-lg-left mt-5 pb-md-3">
		<div class="row">
			<div class="col-12">
				<h3>{{ __("Three Ways to Collect Reviews") }}</h3>
				<p>{{ __("We offer three complementary options when it comes to collecting reviews: Group Invitation, Automatic Review Collecting Service, and Custom links. Otzivio invites all your customers, brand new and past, to review your company, via fully customisable emails. No additional customer registration, or personal details are required. It's simple to implement, secure, and yes, it's effective: On average, more than 7% of the invites results in a review.") }}</p>
			</div>
		</div>
	</div>
	<div class="container pb-md-5 mb-5 mb-lg-3 mt-5">
		<div class="row d-flex">
			<div class="col-12 text-center text-lg-left col-lg-6 pr-md-5 align-self-center justify-content-sm-center order-2 order-lg-1">
				<h3 class="mt-4">{{ __('Group Invitation') }}</h3>
				<p class="h3_subtitle">{{ __("The fastest way to get reviews from past customers") }}</p>
				<p>{{ __("We need a list with three things to kickstart your reviews: customer name, email, and order ID. With just those three things, you'll see reviews come pouring in, in no time at all.") }}</p>
			</div>
			<div class="col-12 text-center col-lg-6 align-self-center order-1 order-lg-2">
				<img src="/images/business/group-invitation.svg" style="max-width: 500px; width: 100%;"  />
			</div>
		</div>
	</div>
	<div class="gray-background pt-5 pb-5">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center col-lg-6 align-self-center">
					<img src="/images/business/arcs.svg" style="max-width: 500px; width: 100%;" />
				</div>
				<div class="col-12 text-center text-lg-left col-lg-6 pl-md-5 align-self-center">
					<h3 class="mt-4">{{ __('Automatic Review Collecting Service') }}</h3>
					<p class="h3_subtitle">{{ __("You get new reviews all the time, on autopilot") }}</p>
					<p>{{ __("Keep the reviews flowing with ARCS. We ask every new customer to review your company. Within days of purchase (delay is customisable), we'll invite your brand new customers to leave a fresh review on our site. In a matter of seconds, these reviews, backed by reference ID (e.g. order number), are live on the web. With ARCS, you'll always have new reviews, hot off the press. Rest assured, all private information will always remain private.") }}</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container text-center text-lg-left mt-5">
		<div class="row">
			<div class="col-12">
				<h3>{{ __("Custom links") }}</h3>
				<p class="h3_subtitle">{{ __("Have it your way with fully customizable features") }}</p>
				<p>{{ __("For enterprise customers, unique links give you total control and complete flexibility over your emails. Align your business needs with our service using your own tools.") }}</p>
			</div>
		</div>
	</div>
	<div class="container text-center text-lg-left pb-5 mt-5">
		<div class="row">
			<div class="col-12">
				<h3>{{ __("Works with the platform of your choice") }}</h3>
				<p>{{ __("We work how you work. Whether you use a third party solution or your own bespoke platform, Otzivio integrates without a hitch. Simply BCC your private Otzivio email address in customer order confirmations. If you need additional assistance for setup with your software, we're here to help.") }}</p>
			</div>
		</div>
		<div class="row">
			<div class="col-12 d-lg-flex justify-content-lg-between">
				<img src="/images/business/shopify.svg" style="max-width: 156px; width: 100%;" class="mt-3" />
				<img src="/images/business/woo.svg" style="max-width: 200px; width: 100%;" class="ml-3 mr-3 mt-3" />
				<img src="/images/business/magento.svg" style="max-width: 168px; width: 100%;" class="ml-3 mr-3 mt-3" />
				<img src="/images/business/presta-shop.svg" style="max-width: 96px; width: 100%;" class="ml-3 mr-3 mt-3" />
				<img src="/images/business/volusion.svg" style="max-width: 184px; width: 100%;" class="mt-3" />
			</div>
		</div>
	</div>
	@component('components/subscribe-to-otzivio-panel') @endcomponent
</div>
@endsection