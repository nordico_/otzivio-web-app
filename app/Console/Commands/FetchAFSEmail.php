<?php

namespace App\Console\Commands;

use App\Services\AutomaticFeedbackSystemService;
use Illuminate\Console\Command;

class FetchAFSEmail extends Command
{
    protected $signature = 'emails:fetch';

    private $mailService;

    public function __construct(AutomaticFeedbackSystemService $service)
    {
        $this->mailService = $service;
        parent::__construct();
    }

    public function handle()
    {
        $this->mailService->initiate();        
    }
}
