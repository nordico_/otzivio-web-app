<?php

namespace App\Http\Controllers;

use App\Category;
use App\Reviews;
use App\User;
use App\Http\Controllers\Controller;
use App\Promoted;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Carbon\Carbon;
use Cache;
use DB;

class HomeController extends Controller
{
    // homepage
    public function __invoke()
    {

    	// do we have a return to parameter after login/signup?
    	if( session()->has( 'return' ) ) {
    		$ret = session( 'return' );
			session()->forget( 'return' );
			
    		return redirect( $ret );
    	}

        $reviews = Reviews::with('site')
                        ->wherePublish( 'Yes' )
                        ->latest()
                        ->take(12)
                        ->get();

        SEOMeta::setTitle(trans('site_meta_title'));
        SEOMeta::setDescription(trans('site_meta_desc'));
        OpenGraph::setTitle(trans('site_meta_desc'));
        OpenGraph::setDescription(trans('site_meta_title'));



        $questionnaireCategories = Category::with('sites')->purposeType()->get();
        
                
        return view('home', [ 'activeNav' => 'home', 'reviews' => $reviews,'questionnaireCategories'=>$questionnaireCategories ]);
        
    }
}