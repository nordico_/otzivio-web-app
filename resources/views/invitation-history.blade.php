@extends( 'base' )

@section( 'content' )

<div class="container">
	<div class="row">
		<div class="col-12 mx-auto">
			<div class="card">
				<a class="mb-4" href="{{route('mycompany')}}"><i class="fa fa-chevron-left mr-2" aria-hidden="true"></i>
					{{ trans('Go back to My Company') }}</a>
				<h2>{{ __('Invitation History') }}</h2>
				<hr>

				<table class="table px-2 mt-2 table-bordered">
					<thead>
						<tr>
							<th scope="col">{{ trans('Email') }}</th>
							<th scope="col">{{ trans('Status') }}</th>
							<th scope="col">{{ trans('Name') }}</th>
							<th scope="col">{{ trans('Reference Number') }}</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($customers as $customer)
						<tr>
							<th style="font-weight: normal">{{ $customer["email"] }}</th>
							<td>{{ trans(enumToString($customer['status'])) }}</td>
							<td>{{ $customer["name"] }}</td>
							<td>{{ $customer["reference_number"] }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>

				{{ $customers->links() }}

			</div><!-- /.card -->
		</div><!-- /.col-10 -->
	</div>
</div><!-- /.container -->

@endsection