@extends('base') 

@section('content')

@if ($plan == 'monthly')
	@php
		$customPlan = 'месец'
	@endphp
@elseif ($plan == '6months')
	@php
		$customPlan = '6 месеца'
	@endphp
@else
	@php
		$customPlan = 'година'
	@endphp
@endif
<div class="inner-page">
	<div class="inner-page__title text-center">
        <h1>{{ __('Credit Card Payment') }}</h1>
        <div class="inner-page__subtitle">
			<p>Ще бъдете таксувани с <strong>{{ $price . Options::get_option( 'currency_symbol' ) . ' на ' . $customPlan }}</strong>, докато не анулирате абонамента си. Ако анулирате абонамента си до 14 дни от срока на плащане, ще ви върнем парите напълно.</p>
        </div>
    </div>
	<div class="container">
		<div class="col-6 mx-auto">
			<form action="" method="POST" id="payment-form"> <span class="payment-errors"></span>
				@csrf
				<div class="row">
				<div class='col-12 form-group required'>
					<label class='control-label'>{{ __('Name on Card') }}</label>
					<input class='form-control name-on-card' size='4' type='text' required="required" value="{{ old('customer') }}">
				</div>
				<div class='col-12 form-group required'>
					<label class='control-label'>{{ __('Card Number') }}</label>
					<input autocomplete='off' class='form-control card-number' size='20' type='text' required="required">
				</div>
				<div class='col form-group cvc required'>
					<label class='control-label'>{{ __('CVC') }}</label>
					<input autocomplete='off' class='form-control card-cvc' placeholder='Напр. 888' size='4' type='text' required="required">
				</div>
				<div class='col form-group expiration required'>
					<label class='control-label'>{{ __('Expiration') }}</label>
					<input class='form-control card-expiry-month' placeholder='Напр. 02' size='2' type='text' required="required">
				</div>
				<div class='col form-group expiration required'>
					<label class='control-label'> </label>
					<input class='form-control card-expiry-year' placeholder='Напр. 2024' size='4' type='text' required="required">
				</div>
			
			<div class='col-12 form-group text-center'>
				<input type="submit" class="btn submit btn-success form-control btn-block" value="{{ __('Submit Payment') }} - {{ $price . Options::get_option( 'currency_symbol' ) . ' на ' . $customPlan }}">
				<br>
				<p class="text-center"><em>Вашите данни за плащане са криптирани и защитени</em></p>
			</div>
			
			</div><!-- /.row -->
			</form>
			<div class="payment-methods__cards text-center">
					<img src="/images/visa-card.svg"/>
					<img src="/images/mastercard-card.svg"/>
					<img src="/images/maestro-card.svg"/>
				</div>
			</div>
		</div>
		<!-- .col-* -->
	</div>
	<!-- ./container add-paddings -->
</div>
<!-- ./container-fluid & white -->@endsection