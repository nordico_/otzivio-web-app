<?php

namespace App\Imports;

use App\Invite;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;




class CustomersImport implements ToModel
{
    use Importable;

    public function model(array $row)
    {
        return new Invite([
            'email' => $row[0],
            'name' => $row[1],
            'reference_number' => $row[2],
        ]);
    }
}
