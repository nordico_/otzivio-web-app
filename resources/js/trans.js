import bg from "../../resources/lang/bg.json";
import en from "../../resources/lang/en.json";

let activeLang = document.getElementsByTagName("html")[0].getAttribute("lang");

export function trans(word){
    if(activeLang!='en' && bg[word]) return bg[word];
    else if(en[word]) return en[word];
    return word;
}

